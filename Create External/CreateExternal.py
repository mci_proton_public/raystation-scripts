from connect import *

__version__ = '0.0.1'

def CreateExternal():
    patient = get_current("Patient")
    case = get_current("Case")
    examination = get_current('Examination')

# Script looks for an existing External contour
    external = ''
    for rois in case.PatientModel.RegionsOfInterest:
        if rois.Type == 'External':
            external = rois.Name

# Creates a contour at -700 HU and above to catch most of patient and immobilization
    with CompositeAction('Gray level threshold (-700HU, Image set: TPCT)'):
      case.PatientModel.CreateRoi(Name=r"-700HU", Color="Red", Type="Organ", TissueName=None, RbeCellTypeName=None, RoiMaterial=None) #, StandardStructureNameId="Unknown")
      case.PatientModel.StructureSets[examination.Name].RoiGeometries[r"-700HU"].OfRoi.GrayLevelThreshold(Examination=examination, LowThreshold=-700, HighThreshold=3071, PetUnit=r"", CbctUnit=None, BoundingBox=None)

# Creates a contour at -950 HU and above to catch the areas missed by -700 HU contour
    with CompositeAction('Gray level threshold (-950HU, Image set: TPCT)'):
      case.PatientModel.CreateRoi(Name=r"-950HU", Color="Yellow", Type="Organ", TissueName=None, RbeCellTypeName=None, RoiMaterial=None) #, StandardStructureNameId="Unknown")
      case.PatientModel.StructureSets[examination.Name].RoiGeometries[r"-950HU"].OfRoi.GrayLevelThreshold(Examination=examination, LowThreshold=-950, HighThreshold=3071, PetUnit=r"", CbctUnit=None, BoundingBox=None)

# Creates an empty ROI to allow the user to fill in the areas missed by the -950 HU. This contour can be a large brush (~3cm) and is used primarily to connect areas that will be missed by the 3cm expansion
    case.PatientModel.CreateRoi(Name=r"Missing", Color="0, 255, 64", Type="Organ", TissueName=None, RbeCellTypeName=None, RoiMaterial=None) #, StandardStructureNameId="Unknown")

# Turns off the -950 Hu contour, so the user can edit the Missing contour
    patient.SetRoiVisibility(RoiName = '-950HU', IsVisible = False)

    await_user_input('Use "Missing" contour and add in areas of missing mask/mold care using a large brush and interpolate. Then continue.')

    #case.PatientModel.RegionsOfInterest['Missing'].InterpolateContours()

# Intersects Missing contour and -950 contour, to only include the missing areas within the immobilization
    with CompositeAction('ROI Algebra (Overlap, Image set: TPCT)'):
      case.PatientModel.CreateRoi(Name=r"Overlap", Color="255, 0, 128", Type="Organ", TissueName=None, RbeCellTypeName=None, RoiMaterial=None) #, StandardStructureNameId="Unknown")
      case.PatientModel.RegionsOfInterest[r"Overlap"].CreateAlgebraGeometry(
          Examination=examination, Algorithm="Auto",
          ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [r"Missing"], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
          ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [r"-950HU"], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
          ResultOperation="Intersection",
          ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })

# If an external contour already exists, it will merge -700 HU and the previously created Overlap contour, to capture patient and immobilization
    if external == '':
        with CompositeAction('ROI Algebra (BODY_10B, Image set: TPCT)'):
          case.PatientModel.CreateRoi(Name=r"BODY_10B", Color="Yellow", Type="Organ", TissueName=None, RbeCellTypeName=None, RoiMaterial=None) #, StandardStructureNameId="Unknown")
          case.PatientModel.RegionsOfInterest[r"BODY_10B"].CreateAlgebraGeometry(
              Examination=examination, Algorithm="Auto",
              ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [r"-700HU", r"Overlap"], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ResultOperation="None",
              ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })

 # Hides contours created by script, except External, to allow the user to edit
        case.PatientModel.RegionsOfInterest['BODY_10B'].SetAsExternal()
        patient.SetRoiVisibility(RoiName = '-700HU', IsVisible = False)
        patient.SetRoiVisibility(RoiName = 'Overlap', IsVisible = False)
        patient.SetRoiVisibility(RoiName = 'Missing', IsVisible = False)

# The user should use the Erase feature to remove artifact streaking and bumps from BBs
        await_user_input('Review the BODY contour and edit out BBs, artifacts, etc. Then continue.')

        with CompositeAction('ROI Algebra (BODY_10B, Image set: TPCT)'):
          case.PatientModel.RegionsOfInterest[r"BODY_10B"].CreateAlgebraGeometry(
              Examination=examination, Algorithm="Auto",
              ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [r"BODY_10B"], 'MarginSettings': { 'Type': "Expand", 'Superior': 3, 'Inferior': 3, 'Anterior': 3, 'Posterior': 3, 'Right': 3, 'Left': 3 } },
              ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ResultOperation="None", ResultMarginSettings={ 'Type': "Contract", 'Superior': 2.9, 'Inferior': 2.9, 'Anterior': 2.9, 'Posterior': 2.9, 'Right': 2.9, 'Left': 2.9 })

# Simplifies the External contour to keep it inside the CT data set and clean up ditzels
        case.PatientModel.StructureSets[examination.Name].SimplifyContours(RoiNames=[r"BODY_10B"], RemoveHoles3D=True, RemoveSmallContours=True, AreaThreshold=1, ReduceMaxNumberOfPointsInContours=False, MaxNumberOfPoints=None, CreateCopyOfRoi=False, ResolveOverlappingContours=False)

# Performs previous steps, but creates a new BODY contour
    else:
        with CompositeAction('ROI Algebra (External_10B, Image set: )'):
          case.PatientModel.RegionsOfInterest[external].CreateAlgebraGeometry(
              Examination=examination, Algorithm="Auto",
              ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [r"-700HU", r"Overlap"], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ResultOperation="None",
              ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })

        patient.SetRoiVisibility(RoiName = external, IsVisible = True)
        patient.SetRoiVisibility(RoiName = '-700HU', IsVisible = False)
        patient.SetRoiVisibility(RoiName = 'Overlap', IsVisible = False)
        patient.SetRoiVisibility(RoiName = 'Missing', IsVisible = False)

        await_user_input('Review the BODY contour and edit out BBs, artifacts, etc')

        with CompositeAction('ROI Algebra (External_10B, Image set: TPCT)'):
          case.PatientModel.RegionsOfInterest[external].CreateAlgebraGeometry(
              Examination=examination, Algorithm="Auto",
              ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [external], 'MarginSettings': { 'Type': "Expand", 'Superior': 3, 'Inferior': 3, 'Anterior': 3, 'Posterior': 3, 'Right': 3, 'Left': 3 } },
              ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
              ResultOperation="None", ResultMarginSettings={ 'Type': "Contract", 'Superior': 2.9, 'Inferior': 2.9, 'Anterior': 2.9, 'Posterior': 2.9, 'Right': 2.9, 'Left': 2.9 })

        case.PatientModel.StructureSets[examination.Name].SimplifyContours(RoiNames=[external], RemoveHoles3D=True, RemoveSmallContours=True, AreaThreshold=1, ReduceMaxNumberOfPointsInContours=False, MaxNumberOfPoints=None, CreateCopyOfRoi=False, ResolveOverlappingContours=False)

# Deletes contours created by script, except BODY/External
    case.PatientModel.RegionsOfInterest['-700HU'].DeleteRoi()
    case.PatientModel.RegionsOfInterest['-950HU'].DeleteRoi()
    case.PatientModel.RegionsOfInterest['Overlap'].DeleteRoi()
    case.PatientModel.RegionsOfInterest['Missing'].DeleteRoi()

xx = CreateExternal()