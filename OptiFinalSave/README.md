# OptiSaveFinalSave_Protons.py
## Purpose
The purpose of the OptiSaveFinalSave_Protons.py script is to automate the optimization and saving process for proton therapy plans in RayStation. It is specifically designed to enhance the workflow efficiency for physicists and dosimetrists by streamlining the final steps of plan optimization and ensuring that the plan is accurately saved, especially for plans utilizing the IonMonteCarlo dose calculation algorithm.
## Workflow
* **Initialization**: The script begins by establishing a connection with the current RayStation session and retrieving the currently selected patient, plan, and beamset.
* **Run Optimization**: It then proceeds to run the optimization for the selected beamset.
* **Initial Save**: Following the optimization, the patient data is immediately saved to preserve the changes made during the optimization process.
* **Dose Calculation Check**: The script checks if the final dose calculation algorithm used by the beamset is IonMonteCarlo.
	* If yes, it recomputes the dose using the IonMonteCarlo algorithm without forcing a recompute, ensuring that the most accurate dose calculation is used while avoiding unnecessary computation time.
	* If no, the script does nothing further, as no special handling is required for other dose calculation algorithms.
* **Final Save**: The patient data is saved again to ensure that all changes are preserved.
## How to use
* Press play ;)
