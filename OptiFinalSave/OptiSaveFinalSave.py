from connect import get_current

__version__='0.0.1'

patient = get_current("Patient")
plan = get_current("Plan")
beamset = get_current("BeamSet")
finalDose = beamset.AccurateDoseAlgorithm.DoseAlgorithm
beamNum = beamset.Number
plan.PlanOptimizations[beamNum-1].RunOptimization()
patient.Save()

if finalDose == "IonMonteCarlo":
    beamset.ComputeDose(
                        ComputeBeamDoses=True,
                        DoseAlgorithm=finalDose,
                        ForceRecompute=False)
    patient.Save()
else:
    pass
