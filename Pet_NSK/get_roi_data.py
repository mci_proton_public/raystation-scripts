import re
import numpy as np
import tkinter as tk
from tkinter import ttk
from connect import get_current
import os

__version__ = '0.0.1'

plan = get_current("Plan")
case = get_current("Case")
patient = get_current("Patient")
examination = get_current("Examination")

def prompt_user_selection(options):
    # Create the main application window
    root = tk.Tk()
    
    options = [" "] + options



    

    # Create a label to display the primary value
    label = tk.Label(root, text="Select Primary")
    label.pack(pady=10)

    # Create a dropdown menu (combobox) with options
    selected_primary = tk.StringVar()
    combobox = ttk.Combobox(root, textvariable=selected_primary)
    combobox['values'] = options
    combobox.current(0)  # Set the default option
    combobox.pack(pady=10)

    # Create a label to display the primary value
    label = tk.Label(root, text="Select Node")
    label.pack(pady=10)

    # Create a dropdown menu (combobox) with options
    selected_node = tk.StringVar()
    combobox_n = ttk.Combobox(root, textvariable=selected_node)
    combobox_n['values'] = options
    combobox_n.current(0)  # Set the default option
    combobox_n.pack(pady=10)


    # Create a submit button
    def on_submit():
        # print(f"Primary value: {selected_primary.get()}")
        # print(f"Node value: {selected_node.get()}")
        root.destroy()
        

    submit_button = tk.Button(root, text="Submit", command=on_submit)
    submit_button.pack(pady=10)

    # Run the window's main loop
    root.mainloop()
    return {selected_primary.get():"CTVp",selected_node.get():"CTVn"}

def get_roi_data(fp = r"\\ad\dfs\Shared Data\MCI Proton\Mauricio\NK\stats"):
    patient_name = patient.Name
    plan_name = plan.Name

    # Grab the structure set for CTTX
    structure_set_cttx = [x for x in case.PatientModel.StructureSets if 'CTTX' in x.OnExamination.Name][0]
    if examination.Name != structure_set_cttx.OnExamination.Name:
        # Make pop up window informing that the CTTX grabbed by the script is not the same as the one in the plan
        root = tk.Tk()
        root.title("Warning")
        label = tk.Label(root, text=f"The CTTX structure set grabbed by the script {structure_set_cttx.OnExamination.Name} is not the same as the one currently loaded {examination.Name}. Please verify. Will now continue to export")
        label.pack(pady=10)
        submit_button = tk.Button(root, text="Continue", command=root.destroy)
        submit_button.pack(pady=10)
        root.mainloop()
        

    #Grab the rois from the structure set
    roi_list = [x.OfRoi.Name for x in structure_set_cttx.RoiGeometries]
    pattern = r'(?P<ctv>CTV[n|p]?_?[0-9]*)_(?P<expansion>0\.5|1|2)_Enhance_(?P<enhance>1\.1|1\.5)'
    # search roi_list using the pattern
    rois = [roi for roi in roi_list if re.search(pattern, roi, re.IGNORECASE)]
    ctv_names = list(np.unique([re.search(pattern, roi, re.IGNORECASE).group('ctv') for roi in rois]))

    ctv_rename = prompt_user_selection(ctv_names)
    ctv_name = []
    ctv_expansion = []
    ctv_enhancement = []
    for roi in rois:
        match = re.search(pattern, roi, re.IGNORECASE)
        ctv_name.append(ctv_rename[match.group('ctv')])
        ctv_expansion.append(match.group('expansion'))
        ctv_enhancement.append(match.group('enhance'))
    

    # Get the roi volumes
    roi_volumes = [structure_set_cttx.RoiGeometries[x].GetRoiVolume() for x in rois]    # cm^3

    # Get total dose statistics per roi
    total_dose = plan.TreatmentCourse.TotalDose

    mean_dose = [total_dose.GetDoseStatistic(RoiName=roi, DoseType='Average') for roi in rois]    # Gy
    max_dose = [total_dose.GetDoseStatistic(RoiName=roi, DoseType='Max') for roi in rois]    # Gy

    with open(os.path.join(fp,patient_name.replace("^","_")+"_"+plan_name+".csv"),'w') as f:
        f.write("Patient,Plan,CTV,Expansion,Enhancement,Volume [cc],Mean Dose [cGy (RBE)],Max Dose [cGy (RBE)]\n")
        for ctv_name,ctv_exp,ctv_enh,vol,mean,max_d in zip(ctv_name,ctv_expansion,ctv_enhancement,roi_volumes,mean_dose,max_dose):
            f.write("{},{},{},{},{},{},{},{}\n".format(patient_name,plan_name,ctv_name,ctv_exp,ctv_enh,vol,mean,max_d))
    
    return True

if __name__ == '__main__':
    get_roi_data()