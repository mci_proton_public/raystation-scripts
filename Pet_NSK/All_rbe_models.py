from connect import *
__version__ = '0.0.2'

def main():
    beam_set = get_current('BeamSet')
    case = get_current('Case')
    exam = beam_set.GetPlanningExamination()
    
    assert beam_set.Modality == 'Protons'
    
    if hasattr(beam_set.FractionDose,'PhysicalDose'):
        fx_dose_Gy = beam_set.FractionDose.PhysicalDose.DoseValues.DoseData * 0.01  #Gy/fx 
        let = beam_set.FractionDose.PhysicalDose.DoseValues.PhysicalData.DoseAveragedLetData * 0.1#keV/um (LET values stored in MeV/cm in the RayStation domain model) 
    else:
        print('Physical dose by scaling RBE dose by 1/1.1')
        fx_dose_Gy = beam_set.FractionDose.DoseValues.DoseData * 0.01 /1.1  #Gy/fx 
        let = beam_set.FractionDose.DoseValues.PhysicalData.DoseAveragedLetData * 0.1#keV/um (LET values stored in MeV/cm in the RayStation domain model) 
    
    dose_grid = beam_set.FractionDose.InDoseGrid;
    


    #Constant alpha_beta_ratio assumed throughout the patient. 
    #(Advanced users can adapt the script to have different alpha_beta_ratios in different ROIs.)
    set_progress('User Input of alpha/beta ratio and RBE model',10)
    alpha_beta_ratio,models = user_options()
    progress = 10
    for model_name,model in models:
        progress += 20
        set_progress(f'Calculating RBE values for {model_name}',progress)
        rbe_dose_values = model(alpha_beta_ratio, fx_dose_Gy, let)*100 #cGy/fx
        dose_description = f'{model_name}_ab_{alpha_beta_ratio}'
        try:
            delete_evaluation_dose( dose_description, case, beam_set.DicomPlanLabel)
        except:
            pass
        set_progress(f'Adding RBE values for {model_name} to the dose grid',progress+5)
        case.AddCustomEvaluationDose(ExaminationName =  exam.Name,
                                    Name = dose_description,
                                    ReferencedBeamSet = beam_set,
                                    DoseGridNumberOfVoxels = dose_grid.NrVoxels,
                                    DoseGridCorner = dose_grid.Corner, 
                                    DoseGridVoxelSize = dose_grid.VoxelSize, 
                                    FractionDoseValues = rbe_dose_values,
                                    IsRbeDose = True)
    set_progress('Done',100)
    exit()
                                 

def mc_namara_rbe(alpha_beta_ratio, dose, let):
    #McNamara et al 2015 Phys. Med. Biol. 60 8399
    # Note missed 0.5 for last alpha_beta_ratio in equation 8 in the paper!  
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio**2 + 4*dose*alpha_beta_ratio*(0.999064+0.35605/alpha_beta_ratio*let) + 4*dose**2*(1.1012-0.0038703*np.sqrt(alpha_beta_ratio)*let)**2)-alpha_beta_ratio)
    return rbe_dose
    
def wedenberg_rbe(alpha_beta_ratio, dose, let):
    #Wedenberg M, Lind B K and Hårdemark B 2013 A model for the relative biological effectiveness of protons: the tissue specific parameter α/β of photons is a predictor for the sensitivity to LET changes Acta Oncol. 52 580–8
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio **2+ 4*dose*(alpha_beta_ratio + 0.434*let) + 4*dose**2) - alpha_beta_ratio)
    return rbe_dose
    
def carabe_rbe(alpha_beta_ratio, dose, let):
    #Carabe A, Moteabbed M, Depauw N, Schuemann J and Paganetti H 2012 Range uncertainty in proton therapy due to variable biological effectiveness Phys. Med. Biol. 57 1159–72
    rbe_max=0.843+0.154*2.686/alpha_beta_ratio*let
    rbe_min=1.09+0.006*2.686/alpha_beta_ratio*let
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio**2 + 4*dose*alpha_beta_ratio*rbe_max + 4*rbe_min**2*dose**2) - alpha_beta_ratio)
    return rbe_dose

def linear_let_RBE(k, dose, let):
    # Simple model that is often used when creating rbe model based on outcome results (lesions etc)
    rbe_dose = dose * ( 1.0 + k*let)
    return rbe_dose

def delete_evaluation_dose( name, case, beamSetName): 
    examination = get_current("Examination")

    if not case.TreatmentDelivery.FractionEvaluations:
        return
    doe = next(x for x in case.TreatmentDelivery.FractionEvaluations[0].DoseOnExaminations if x.OnExamination.Name == examination.Name)
    try:
        de = next(de for de in doe.DoseEvaluations if de.CustomDoseProperties.Description == name and de.ForBeamSet.DicomPlanLabel == beamSetName )
    except:
        return
    de.DeleteEvaluationDose()

def user_options():
    # Make a user interface for the user to select the RBE model and alpha/beta ratio
    import tkinter as tk
    from tkinter import simpledialog
    root = tk.Tk()
    root.withdraw()
    alpha_beta_ratio = tk.DoubleVar()
    rbe_model = tk.StringVar()

    model_dict = {"McNamara": [("McNamara",mc_namara_rbe)], "Wedenberg": [("Wedenberg",wedenberg_rbe)], "Carabe": [("Carabe",carabe_rbe)], "All": [("Carabe",carabe_rbe),("McNamara",mc_namara_rbe), ("Wedenberg",wedenberg_rbe)]}
    

    dialog = tk.Toplevel(root)
    dialog.title("User Options")

    tk.Label(dialog, text="Enter alpha/beta ratio (Gy):").pack(pady=5)
    tk.Entry(dialog, textvariable=alpha_beta_ratio).pack(pady=5)

    tk.Label(dialog, text="Select RBE model:").pack(pady=5)
    tk.OptionMenu(dialog, rbe_model, "McNamara", "Wedenberg", "Carabe", "All").pack(pady=5)

    # Ask if the user wants to delete the previous RBE evaluations
    def on_delete():
        beam_set = get_current('BeamSet')
        case = get_current('Case')
        exam = beam_set.GetPlanningExamination()

        delete = simpledialog.askstring("Delete previous 3.76 RBE evaluations", "Do you want to delete the previous 3.76_RBE evaluations? (yes/no)")
        if delete.lower() == 'yes':
            delete_evaluation_dose("Carabe_ab_3.76", case, beam_set.DicomPlanLabel)
            delete_evaluation_dose("McNamara_ab_3.76", case, beam_set.DicomPlanLabel)
            delete_evaluation_dose("Wedenberg_ab_3.76", case, beam_set.DicomPlanLabel)

    def on_submit():
        on_delete()
        dialog.destroy()

    tk.Button(dialog, text="Submit", command=on_submit).pack(pady=20)

    root.wait_window(dialog)
    alpha_beta_ratio = alpha_beta_ratio.get()
    rbe_model = rbe_model.get()
    root.destroy()
    return float(alpha_beta_ratio), model_dict[rbe_model]

if __name__ == '__main__':
    main()



