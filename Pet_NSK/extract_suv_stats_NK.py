import numpy as np
from connect import set_progress, get_current
import os
import re

__version__ = '0.0.9'
__author__ = 'Mauricio Acosta'

def main():
    set_progress("Identifing corresponding CT/PET scans",1)

    case = get_current('Case')
    plan = get_current('Plan')
    patient = get_current('Patient')

    # Find the Pet Scan
    pet_exams = [x.Name for x in case.Examinations if x.EquipmentInfo.Modality=='Pet']
    ss = case.PatientModel.StructureSets
    pet_scan = None
    for pet in pet_exams:
        ssi = ss[pet]
        try:
            ssi.RoiGeometries[f'Pet_Enhance_1.1_{pet}']
            pet_scan = pet
            break
        except:
            continue

    set_progress(f"Found PET: {pet_scan}",2)
    # Get planning examination
    planning_Exam = plan.BeamSets[0].GetPlanningExamination().Name # Should be this one
    ssi = ss[planning_Exam]
    try:
        ssi.RoiGeometries[f'Pet_Enhance_1.1_{pet_scan}']
        planning_scan = planning_Exam
    except:
        print("Error. Planning examination doesn't match")
        exit
    set_progress(f"Found CT: {planning_scan}. Extracting CT/PET/SUV data",3)


    # Get pixel Data. The CT and PET are registered
    imageStack_ct = case.Examinations[planning_scan].Series[0].ImageStack
    imageStack_pet = case.Examinations[pet_scan].Series[0].ImageStack
    pixel_size = imageStack_ct.NrPixels



    ct_pixelData = imageStack_ct.PixelData.view(np.uint16).reshape(-1,pixel_size['y'],pixel_size['x'])
    pet_pixelData = imageStack_pet.PixelData.view(np.uint16).reshape(-1,pixel_size['y'],pixel_size['x'])
    conversion_suv = imageStack_pet.PetConversionParameters.SuvBwScaleFactors
    suv_pixelData = pet_pixelData * conversion_suv[:,np.newaxis,np.newaxis]
    suv_pixelData = suv_pixelData.ravel()
    set_progress("Extracted CT, PET, and SUV data. Identify relevant ROIs",4)




    structureSet_planning = case.PatientModel.StructureSets[planning_scan]

    corner = imageStack_pet.Corner
    nrVoxels = imageStack_pet.NrPixels
    nrVoxels['z'] = pet_pixelData.shape[0]
    voxelSize = imageStack_pet.PixelSize
    voxelSize['z'] = imageStack_ct.SlicePositions[1] - imageStack_ct.SlicePositions[0]

    rois = [roi.OfRoi.Name for roi in structureSet_planning.RoiGeometries if 'ctv' in roi.OfRoi.Name.lower() and ('enhance' in roi.OfRoi.Name.lower() or 'expansion' in roi.OfRoi.Name.lower())]

    suv_data = []

    def extract_expansion_enhancement_ctv(roi_name):
        ''' This will extract the enhancement and expansion from the roi name'''
        if 'expansion' in roi_name.lower():
            pattern = r'(?P<CTV>ctv.*)_(?P<Expansion>\d+(\.\d+)?)cm_Expansion'
        else:
            pattern = r'(?P<CTV>ctv.*)_(?P<Expansion>\d+(\.\d+)?)_(non-)?enhance_(?P<Enhancement>1\.1|1\.5)_(?P<PET>.*)'
        match = re.match(pattern,roi_name,re.IGNORECASE)
        result = match.groupdict()
        if 'Enhancement' not in result:
            result['Enhancement'] = None
        if 'PET' not in result:
            result['PET'] = None
        
        #identify if its a non-enhanced section
        if 'non-enhance' in roi_name.lower():
            result['Enhanced'] = False
        elif 'enhance' in roi_name.lower():
            result['Enhanced'] = True
        else:
            result['Enhanced'] = None

        # Format CTV to exlude any digits in between CTV and (n|p)
        result['CTV'] = re.sub(r'ctv(\d+)','CTV',result['CTV'],flags=re.IGNORECASE)
        result['CTV'] = result['CTV'].split('_')[0]

        return result
        

    for i,roi in enumerate(rois):
        progress_val = 5 + (i/len(rois))*90
        set_progress(f"Processing {roi}",progress_val)
        roi_ = structureSet_planning.RoiGeometries[roi]
        if not roi_.HasContours() or roi_.GetRoiGeometryAsVoxels(Corner = corner, VoxelSize = voxelSize, NrVoxels = nrVoxels).ravel().sum()==0:
            mean_suv = None
            max_suv = None
            try:
                roi_volume = roi_.GetRoiVolume()
            except Exception as e:
                roi_volume = 0
                
            roi_info = extract_expansion_enhancement_ctv(roi)
            suv_data.append({'Patient':patient.Name, 'Plan':plan.Name,'ROI':roi,
                             'Mean SUVbw [g/mL]':mean_suv, 'Max SUVbw [g/mL]':max_suv,
                             'ROI Volume [cc]':roi_volume, **roi_info
                             })
        else:
            roiAsVoxels = roi_.GetRoiGeometryAsVoxels(Corner = corner, VoxelSize = voxelSize, NrVoxels = nrVoxels)
            mean_suv = np.average(suv_pixelData, weights = roiAsVoxels)
            roi_suv_values = suv_pixelData[roiAsVoxels != 0]
            max_suv = roi_suv_values.max()
            try:
                roi_volume = roi_.GetRoiVolume()
            except Exception as e:
                print(f"Error getting roi Volume for {roi}\n Error: {e}")
                roi_volume = 0
            
            # Extract enhancement and expansion
            roi_info = extract_expansion_enhancement_ctv(roi)
            
            
            


            suv_data.append({'Patient':patient.Name, 'Plan':plan.Name,'ROI':roi,
                            'Mean SUVbw [g/mL]':mean_suv, 'Max SUVbw [g/mL]':max_suv,
                            'ROI Volume [cc]':roi_volume, **roi_info
                            })


    set_progress("Saving to CSV",98)
    fol = r'\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\NK PET\stats\SUV'
    fp = os.path.join(fol,f'{patient.Name}_{plan.Name}_SUV_stats.csv')
    with open(fp,'w') as f:
        f.write('Patient,Plan,ROI,Mean SUVbw [g/mL],Max SUVbw [g/mL],ROI Volume [cc],CTV,Expansion,Enhancement,Enhanced,PET\n')
        for row in suv_data:
            f.write(f"{row['Patient']},{row['Plan']},{row['ROI']},{row['Mean SUVbw [g/mL]']},{row['Max SUVbw [g/mL]']},{row['ROI Volume [cc]']},{row['CTV']},{row['Expansion']},{row['Enhancement']},{row['Enhanced']},{row['PET']}\n")
    exit

if __name__ == '__main__':
    main()