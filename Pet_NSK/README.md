# README

## Purpose

The purpose of this script is to convert Standardized Uptake Value (SUV) in g/mL to Becquerel per milliliter (Bq/mL) and perform an analysis on PET scans. The script includes functionalities to handle patient information, dose information, and create contours based on the PET scan data.

## Workflow

1. **User Inputs**:
    - The user provides the following inputs:
        - Selected option from radio buttons (e.g., unit of measurement).
        - Threshold value for analysis.
        - Selected PET scans from checkboxes.

2. **Script Execution**:
    - The script performs the following steps when executed:
        1. **Initialize Environment**:
            - Initializes the environment and hides unnecessary windows.
        2. **Retrieve Examination Data**:
            - Retrieves the planning examination and PET scan data based on user inputs.
        3. **Process PET Scans**:
            - Processes each selected PET scan:
                - Converts SUV to Bq/mL if necessary.
                - Calculates the maximum value for the examination series.
        4. **Create Contours**:
            - Creates contours based on the processed data and user-defined thresholds.
            - Generates new regions of interest (ROIs) and performs algebraic operations to create enhanced and non-enhanced regions.
        5. **Completion**:
            - Updates progress and displays a completion message.

3. **Output**:
    - The script generates contours and ROIs based on the PET scan data and user inputs, providing a detailed analysis of the PET scans.