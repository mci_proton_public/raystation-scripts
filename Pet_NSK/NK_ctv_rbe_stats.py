from connect import get_current

import os

import re

__version__ = '0.0.7'

def get_doses_on_examination():

    case = get_current('Case')

    for doe in case.TreatmentDelivery.FractionEvaluations[0].DoseOnExaminations:

        if doe.OnExamination.Name.strip()[:4]=='CTTX':

            # print("Found Dose on Examination CTTX = ", doe.OnExamination.Name)

            return doe

    print("Did not find Dose on Examination CTTX")

    return None



def get_dose_evaluation(doe,model_name='carabe'):

    for de in doe.DoseEvaluations:

        if hasattr(de,'CustomDoseProperties'):

            cdp = de.CustomDoseProperties

            if hasattr(cdp,'Description'):

                desc = cdp.Description

                if model_name in desc.lower():

                    print("Found Dose Evaluation on model = ", desc)

                    return de



def get_dose_statistics_rbe_models(dose_evaluation,roi_name,num_fractions=1):

    ''' This function will return max,min,avg dose, and max_1_percent for given dose_evaluation'''

    # Check if dose evaluation is None and return None if it is. This means that the model evaluation was not found
    if not dose_evaluation:
        return {'min_dose':None, 'max_dose':None, 'avg_dose':None, 'max_1_percent_dose':None}


    min_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                DoseType='Min')

    max_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                DoseType='Max')

    avg_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                DoseType='Average')

    max_1_percent_dose_per_fraction = dose_evaluation.GetDoseAtRelativeVolumes(RoiName = roi_name,

                                                                               RelativeVolumes=[.01])[0]

    result = {'min_dose':min_dose_per_fraction*num_fractions/100,

              'max_dose':max_dose_per_fraction*num_fractions/100,

                'avg_dose':avg_dose_per_fraction*num_fractions/100,

                'max_1_percent_dose':max_1_percent_dose_per_fraction*num_fractions/100}

    return result





def get_dose_stats(rois,rbe_model_name):

    num_fractions = int(get_current("BeamSet").FractionationPattern.NumberOfFractions)

    doe = get_doses_on_examination()

    de = get_dose_evaluation(doe,rbe_model_name)

    dose_stats = {}

    for roi in rois:

        dose_stats[roi] = get_dose_statistics_rbe_models(de,roi,num_fractions)

    return dose_stats


def get_reg_stats(rois):
    beam_set = get_current("BeamSet")

    num_fractions = int(beam_set.FractionationPattern.NumberOfFractions)

    dose_evaluation = beam_set.FractionDose

    dose_stats = {}

    for roi_name in rois:

        min_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                    DoseType='Min')

        max_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                    DoseType='Max')

        avg_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,

                                                    DoseType='Average')

        max_1_percent_dose_per_fraction = dose_evaluation.GetDoseAtRelativeVolumes(RoiName = roi_name,

                                                                                   RelativeVolumes=[.01])[0]

        dose_stats[roi_name] = {'min_dose':min_dose_per_fraction*num_fractions/100,

                  'max_dose':max_dose_per_fraction*num_fractions/100,

                    'avg_dose':avg_dose_per_fraction*num_fractions/100,

                    'max_1_percent_dose':max_1_percent_dose_per_fraction*num_fractions/100}

    return dose_stats


def write_to_csv(fp=r'\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\NK PET\stats',export_file_name = None):

    rois = [x.Name for x in get_current('Case').PatientModel.RegionsOfInterest if x.Name[:4].lower() in ['ctvp', 'ctvn'] and 'Enhance' in x.Name]

    # Add the rois that correspond to the entire ctv with expansion

    rois += [x.Name for x in get_current('Case').PatientModel.RegionsOfInterest if x.Name[-12:].lower()=='cm_expansion']

    
    

    roi_info = {}

    for roi in rois:
        data = {}
        # Extract info from each roi
        pattern = r"^(?P<roiName>.*)_(?P<expansionValue>\d+(\.\d+)?)_(?P<enhanceType>Enhance|NON-Enhance)_(?P<enhancementValue>\d+(\.\d+)?)_(?P<imaging>.*)$"
        match = re.match(pattern, roi)
        if match:
            data['roiName'] = match.group('roiName')
            data['expansionValue'] = match.group('expansionValue')
            data['enhanceType'] = match.group('enhanceType')
            data['enhancementValue'] = match.group('enhancementValue')
            data['imaging'] = match.group('imaging')
        else:
            # Could be the expansion roi
            pattern = r"^(?P<roiName>.*)_(?P<expansion>\d+(\.\d+)?)cm_Expansion$"
            match = re.match(pattern, roi)
            if match:
                data['roiName'] = match.group('roiName')
                data['expansionValue'] = match.group('expansion')
                data['enhanceType'] = 'None'
                data['enhancementValue'] = 'None'
                data['imaging'] = 'None'
            else:
                data['roiName'] = 'None'
                data['expansionValue'] = 'None'
                data['enhanceType'] = 'None'
                data['enhancementValue'] = 'None'
                data['imaging'] = 'None'
        roi_info[roi] = data


    patient_name = get_current('Patient').Name

    case_name = get_current('Case').CaseName

    plan_name = get_current('Plan').Name

    beam_set_name = get_current('BeamSet').DicomPlanLabel

    structure_set_cttx = [x for x in get_current('Case').PatientModel.StructureSets if x.OnExamination.Name.strip()[:4]=='CTTX'][0]

    structure_set_examination_name = structure_set_cttx.OnExamination.Name
    
    examination_name = get_current('Examination').Name

    if structure_set_cttx.OnExamination.Name != examination_name:
        print(f"Structure set ({structure_set_cttx.OnExamination.Name}) and examination ({examination_name}) do not match")

    roi_volumes = {}
    for roi in rois:
        try:
            roi_volumes[roi] = structure_set_cttx.RoiGeometries[roi].GetRoiVolume()
        except:
            roi_volumes[roi] = None

    dose_stats_carabe = get_dose_stats(rois,'carabe')
    print('Carabe stats:',dose_stats_carabe)

    dose_stats_mcnamara = get_dose_stats(rois,'mcnamara')

    dose_stats_wedenberg = get_dose_stats(rois,'wedenberg')

    dose_stats_reg = get_reg_stats(rois)

    if not export_file_name:
        export_file_name = patient_name+'_' + beam_set_name +'.csv'

    with open(os.path.join(fp,export_file_name),'w') as f:

        f.write('Patient,Case,Plan,BeamSet,Examination,StructureSetExamination,ROI,ROI_Name,Expansion_Value,Enhance_Type,Enhancement_Value,Imaging,ROI_volume [cc],RBE_Model,Min Dose [Gy],Max Dose [Gy],Avg Dose [Gy],Max 1% Dose [Gy]\n')

        for roi in dose_stats_carabe:
            f.write(','.join([patient_name, case_name, plan_name, beam_set_name, examination_name, structure_set_examination_name,roi, roi_info[roi]['roiName'], roi_info[roi]['expansionValue'], roi_info[roi]['enhanceType'], roi_info[roi]['enhancementValue'], roi_info[roi]['imaging'], str(roi_volumes[roi]), 'Carabe', ','.join([str(x) for x in dose_stats_carabe[roi].values()])]) + '\n')
        
        for roi in dose_stats_mcnamara:
            f.write(','.join([patient_name, case_name, plan_name, beam_set_name, examination_name, structure_set_examination_name,roi, roi_info[roi]['roiName'], roi_info[roi]['expansionValue'], roi_info[roi]['enhanceType'], roi_info[roi]['enhancementValue'], roi_info[roi]['imaging'], str(roi_volumes[roi]), 'McNamara', ','.join([str(x) for x in dose_stats_mcnamara[roi].values()])]) + '\n')

        for roi in dose_stats_wedenberg:
            f.write(','.join([patient_name, case_name, plan_name, beam_set_name, examination_name, structure_set_examination_name,roi, roi_info[roi]['roiName'], roi_info[roi]['expansionValue'], roi_info[roi]['enhanceType'], roi_info[roi]['enhancementValue'], roi_info[roi]['imaging'], str(roi_volumes[roi]), 'Wedenberg', ','.join([str(x) for x in dose_stats_wedenberg[roi].values()])]) + '\n')

        for roi in dose_stats_reg:
            f.write(','.join([patient_name, case_name, plan_name, beam_set_name, examination_name, structure_set_examination_name,roi, roi_info[roi]['roiName'], roi_info[roi]['expansionValue'], roi_info[roi]['enhanceType'], roi_info[roi]['enhancementValue'], roi_info[roi]['imaging'], str(roi_volumes[roi]), 'Physical_Dose_x_1.1', ','.join([str(x) for x in dose_stats_reg[roi].values()])]) + '\n')




    print('Wrote stats to',os.path.join(fp,export_file_name))

    return


if __name__ == '__main__':
    write_to_csv()