from connect import *

def main():
    beam_set = get_current('BeamSet')
    case = get_current('Case')
    exam = beam_set.GetPlanningExamination()
    
    assert beam_set.Modality == 'Protons'
    
    if hasattr(beam_set.FractionDose,'PhysicalDose'):
        fx_dose_Gy = beam_set.FractionDose.PhysicalDose.DoseValues.DoseData * 0.01  #Gy/fx 
        let = beam_set.FractionDose.PhysicalDose.DoseValues.PhysicalData.DoseAveragedLetData * 0.1#keV/um (LET values stored in MeV/cm in the RayStation domain model) 
    else:
        print('Physcisl dose by scaling RBE dose by 1/1.1')
        fx_dose_Gy = beam_set.FractionDose.DoseValues.DoseData * 0.01 /1.1  #Gy/fx 
        let = beam_set.FractionDose.DoseValues.PhysicalData.DoseAveragedLetData * 0.1#keV/um (LET values stored in MeV/cm in the RayStation domain model) 
    
    dose_grid = beam_set.FractionDose.InDoseGrid;
     
    #Constant alpha_beta_ratio assumed throughout the patient. 
    #(Advanced users can adapt the script to have different alpha_beta_ratios in different ROIs.)
    alpha_beta_ratio = 3.76 #Gy

    rbe_dose_values = carabe_rbe(alpha_beta_ratio, fx_dose_Gy, let)*100 #cGy/fx
    
    dose_description = 'Carabe_ab_3.76'
    delete_evaluation_dose( dose_description, case, beam_set.DicomPlanLabel)

    case.AddCustomEvaluationDose(ExaminationName =  exam.Name,
                                 Name = dose_description,
                                 ReferencedBeamSet = beam_set,
                                 DoseGridNumberOfVoxels = dose_grid.NrVoxels,
                                 DoseGridCorner = dose_grid.Corner, 
                                 DoseGridVoxelSize = dose_grid.VoxelSize, 
                                 FractionDoseValues = rbe_dose_values,
                                 IsRbeDose = True)
                                 

def mc_namara_rbe(alpha_beta_ratio, dose, let):
    #McNamara et al 2015 Phys. Med. Biol. 60 8399
    # Note missed 0.5 for last alpha_beta_ratio in equation 8 in the paper!  
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio**2 + 4*dose*alpha_beta_ratio*(0.999064+0.35605/alpha_beta_ratio*let) + 4*dose**2*(1.1012-0.0038703*np.sqrt(alpha_beta_ratio)*let)**2)-alpha_beta_ratio)
    return rbe_dose
    
def wedenberg_rbe(alpha_beta_ratio, dose, let):
    #Wedenberg M, Lind B K and Hårdemark B 2013 A model for the relative biological effectiveness of protons: the tissue specific parameter α/β of photons is a predictor for the sensitivity to LET changes Acta Oncol. 52 580–8
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio **2+ 4*dose*(alpha_beta_ratio + 0.434*let) + 4*dose**2) - alpha_beta_ratio)
    return rbe_dose
    
def carabe_rbe(alpha_beta_ratio, dose, let):
    #Carabe A, Moteabbed M, Depauw N, Schuemann J and Paganetti H 2012 Range uncertainty in proton therapy due to variable biological effectiveness Phys. Med. Biol. 57 1159–72
    rbe_max=0.843+0.154*2.686/alpha_beta_ratio*let
    rbe_min=1.09+0.006*2.686/alpha_beta_ratio*let
    rbe_dose = 0.5*(np.sqrt(alpha_beta_ratio**2 + 4*dose*alpha_beta_ratio*rbe_max + 4*rbe_min**2*dose**2) - alpha_beta_ratio)
    return rbe_dose

def linear_let_RBE(k, dose, let):
    # Simple model that is often used when creating rbe model based on outcome results (lesions etc)
    rbe_dose = dose * ( 1.0 + k*let)
    return rbe_dose

def delete_evaluation_dose( name, case, beamSetName): 

    if not case.TreatmentDelivery.FractionEvaluations:
        return
    
    try:
        de = next(de for de in case.TreatmentDelivery.FractionEvaluations[0].DoseOnExaminations[0].DoseEvaluations if de.CustomDoseProperties.Description == name and de.ForBeamSet.DicomPlanLabel == beamSetName )
    except:
        return
    de.DeleteEvaluationDose()

main()



