from tkinter import *
from tkinter import messagebox
from connect import *
import traceback
from tkinter import PhotoImage
import logging
import os
import tkinter as tk


__version__ = '0.0.18'


# Convert SUV (g/mL) to Bq/mL
def suv_to_bqml(suv, weight, height, sex, dose):
    ''' This function converts an SUV (g/mL) value to Bq/mL. This assumes that the SUV value is in g/mL, weight in kg, height in cm, and dose in mCi.'''
    dose = dose * 37 * 10 ** 3 # Convert mCi to kBq
    if sex == "Male":
        lbm = 1.1 * weight - 120 * (weight / height) ** 2
    else:
        lbm = 1.07 * weight - 148 * (weight / height) ** 2
    
    return suv*dose/lbm

def actual_activity(initial_activity,time_passed=60,half_life = 110):
    '''This assumes time and half life is in minutes'''
    num_half_lives = time_passed/half_life
    final_activity = initial_activity*(0.5**num_half_lives)
    return final_activity


# Create a function to handle the button click
def start_analysis():
    Root.withdraw()
    
    try:
        with CompositeAction("Execute PET AUTO Function"):
            
            
            examination_planning = beam_set.GetPlanningExamination()
            # Get the selected option from the radio buttons
            selected_option = var.get()
            
            # Get the threshold value
            entered_value = entry.get()

            

            


            # Get the selected PET scans from the checkboxes
            selected_pet_scans = [pet_var for pet_var in pet_vars if pet_vars[pet_var].get()==1]

            # Get the selected CTV ROIs from the checkboxes
            selected_ctv_rois = [ctv_var for ctv_var in ctv_vars if ctv_vars[ctv_var].get()==1]
            
            # Log the plan, planning examination, selected option, entered value, and selected PET scans
            logger.info(f"Plan: {beam_set.BeamSetIdentifier().split(':')[0]}")
            logger.info(f"Planning Examination: {examination_planning.Name}")
            logger.info(f"Selected Option: {selected_option}")
            logger.info(f"Entered Value: {entered_value}")
            logger.info(f"Selected PET Scans: {selected_pet_scans}")

            # Check that the entered value is a number
            try:
                entered_value = float(entered_value)
            except ValueError:
                raise Exception("Entered value must be a number")
            
            for id_ex,examination_name in enumerate(selected_pet_scans):
                set_progress(f"Drawing contours on {examination_name}",((id_ex+1)/(len(selected_pet_scans) + 1))*100)
                examination = case.Examinations[examination_name]
                # messagebox.showinfo("selected_pet_scans",examination_name)
                
                #conversion_factor_activity_concentration = examination.Series[0].ImageStack.PetConversionParameters.ActivityConcentrationScaleFactors[0]
                #unit = examination.Series[0].ImageStack.PetConversionParameters.Unit
                unit = 'g/ml' if var.get() == "Liver SUV" else "Bq/ml"
                # Check that if using SUV that there is a conversion factor
                if unit == 'g/ml':
                    if np.any(examination.Series[0].ImageStack.PetConversionParameters.SuvBwScaleFactors == None):
                        if examination.Series[0].ImageStack.PetConversionParameters.Unit == 'g/ml':
                            conversion_factor = max(examination.Series[0].ImageStack.PetConversionParameters.RescaleSlopes) #[0] # We assume that each slice has the same factor
                        else:
                            raise Exception("Scan has no SUVBwScaleFactors")
                    else:
                        conversion_factor = max(examination.Series[0].ImageStack.PetConversionParameters.SuvBwScaleFactors) #[0] # We assume that each slice has the same factor
                
                elif unit == 'Bq/ml':
                    if np.any(examination.Series[0].ImageStack.PetConversionParameters.ActivityConcentrationScaleFactors == None):
                        if examination.Series[0].ImageStack.PetConversionParameters.Unit == 'Bq/ml':
                            conversion_factor = max(examination.Series[0].ImageStack.PetConversionParameters.RescaleSlopes) #[0] # We assume that each slice has the same factor
                        else:
                            raise Exception("Scan has no ActivityConcentrationScaleFactors")
                    else:
                        conversion_factor = max(examination.Series[0].ImageStack.PetConversionParameters.ActivityConcentrationScaleFactors) #[0] # We assume that each slice has the same factor
                
                    # Get patient information and dose information
                    weight = float(weight_entry.get())
                    height = float(height_entry.get())
                    sex = var_sex.get()
                    dose = float(dose_entry.get())
                    time = float(time_entry.get())
                    dose = actual_activity(dose,time)

                    # Convert SUV to Bq/mL
                    entered_value = suv_to_bqml(entered_value, weight, height,sex,dose)



                    
                else:
                    raise Exception(f"Invalid unit type [{unit = }]")

                max_val = examination.Series[0].ImageStack.MaxStoredValue * conversion_factor
                for id_en,enhance in enumerate([1.1,1.5]):
                    set_progress(f"Creating PET_Enhance_{enhance} contours on {examination_name}",((id_ex+1)/(len(selected_pet_scans) + 1) + (id_en)/3)*100)
                    # Create PET_Enhance_1.1 contour
                    name1 = f"Pet_Enhance_{enhance}" + f"_{examination_name}"
                    threshold1 = enhance * float(entered_value)
                    case.PatientModel.CreateRoi(Name=name1, Color="Blue", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)
                    case.PatientModel.RegionsOfInterest[name1].GrayLevelThreshold(Examination=examination, LowThreshold=threshold1, HighThreshold=max_val, PetUnit=unit, CbctUnit=None, BoundingBox=None)

                    #messagebox.showinfo("Examinations",f"Source = {examination.Name}\nTarget = {examination_planning.Name}")
                    # Copy over to CTTX
                    case.PatientModel.CopyRoiGeometries(SourceExamination=examination,
                                                        TargetExaminationNames=[examination_planning.Name],
                                                        RoiNames=[name1],
                                                        ImageRegistrationNames=[],
                                                        TargetExaminationNamesToSkipAddedReg=[examination_planning.Name])

                    
                    plan = get_current("Plan")
                    structure_set = plan.GetTotalDoseStructureSet()
                    for ctv_roi in selected_ctv_rois:
                    #for X in ['p','n']:
                        #rois = [r.OfRoi.Name for r in structure_set.RoiGeometries if f'CTV{X}_' in r.OfRoi.Name]
                        #ctv_roi = sorted(rois)[0]
                        #print(ctv_roi)
                        for expansion in [0,.5,1,2]:
                            name_new = ctv_roi.split('_')[0]+f'_{expansion}_Enhance_{enhance}'  + f"_{examination_name}"
                            case.PatientModel.CreateRoi(Name=name_new, Color="Blue", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)
                            case.PatientModel.RegionsOfInterest[name_new].CreateAlgebraGeometry(
                                Examination=examination_planning,
                                Algorithm="Auto",
                                ExpressionA={
                                    'Operation': "Union",
                                    'SourceRoiNames': [f"{ctv_roi}"],
                                    'MarginSettings': {
                                        'Type': "Expand",
                                        'Superior': expansion,
                                        'Inferior': expansion,
                                        'Anterior': expansion,
                                        'Posterior': expansion,
                                        'Right': expansion,
                                        'Left': expansion}},
                                ExpressionB={
                                    'Operation': "Union",
                                    'SourceRoiNames': [name1],
                                    'MarginSettings': {
                                        'Type': "Expand",
                                        'Superior': 0,
                                        'Inferior': 0,
                                        'Anterior': 0,
                                        'Posterior': 0,
                                        'Right': 0,
                                        'Left': 0}},
                                ResultOperation="Intersection",
                                ResultMarginSettings={
                                    'Type': "Expand",
                                    'Superior': 0,
                                    'Inferior': 0,
                                    'Anterior': 0,
                                    'Posterior': 0,
                                    'Right': 0,
                                    'Left': 0})
                            name_leftover = ctv_roi.split('_')[0]+f'_{expansion}_NON-Enhance_{enhance}'  + f"_{examination_name}"
                            case.PatientModel.CreateRoi(Name=name_leftover, Color="Red", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)
                            case.PatientModel.RegionsOfInterest[name_leftover].CreateAlgebraGeometry(
                                Examination=examination_planning,
                                Algorithm="Auto",
                                ExpressionA={
                                    'Operation': "Union",
                                    'SourceRoiNames': [f"{ctv_roi}"],
                                    'MarginSettings': {
                                        'Type': "Expand",
                                        'Superior': expansion,
                                        'Inferior': expansion,
                                        'Anterior': expansion,
                                        'Posterior': expansion,
                                        'Right': expansion,
                                        'Left': expansion}},
                                ExpressionB={
                                    'Operation': "Union",
                                    'SourceRoiNames': [name_new],
                                    'MarginSettings': {
                                        'Type': "Expand",
                                        'Superior': 0,
                                        'Inferior': 0,
                                        'Anterior': 0,
                                        'Posterior': 0,
                                        'Right': 0,
                                        'Left': 0}},
                                ResultOperation="Subtraction",
                                ResultMarginSettings={
                                    'Type': "Expand",
                                    'Superior': 0,
                                    'Inferior': 0,
                                    'Anterior': 0,
                                    'Posterior': 0,
                                    'Right': 0,
                                    'Left': 0})

                            # Make expansion on ctv only once
                            if enhance==1.1:
                                name_expansion = ctv_roi.split('_')[0] + f'_{expansion}cm_Expansion'
                                case.PatientModel.CreateRoi(Name=name_expansion, Color="Blue", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)
                                case.PatientModel.RegionsOfInterest[name_expansion].CreateAlgebraGeometry(
                                    Examination=examination_planning,
                                    Algorithm="Auto",
                                    ExpressionA={
                                        'Operation': "Union",
                                        'SourceRoiNames': [f"{ctv_roi}"],
                                        'MarginSettings': {
                                            'Type': "Expand",
                                            'Superior': expansion,
                                            'Inferior': expansion,
                                            'Anterior': expansion,
                                            'Posterior': expansion,
                                            'Right': expansion,
                                            'Left': expansion}})
                            
                    # case.PatientModel.RegionsOfInterest[name1].DeleteRoi()


            # Display a message box with the selected options
            set_progress("Done!",100)
            messagebox.showinfo("Complete", f"Done!")
            root.destroy()
    except Exception as e:
        logger.error("Traceback: "+ traceback.format_exc().replace('\n', 'NEWLINE'))
        messagebox.showinfo("Error",traceback.format_exc())
        root.destroy()



# Create a logger for the current module
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    log_fp = r'\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\PET Auto Tool\log.txt'
    logging.basicConfig(filename=log_fp, level=logging.DEBUG, format='%(asctime)s:::%(levelname)s:::%(name)s:::%(message)s')
    # Log the current user to log file using the logger instance
    logger.info(f"User: {os.getlogin()}")
    # Get the current case
    try:
        case = get_current("Case")
    except:
        messagebox.showerror("Error","No case is loaded. Please ensure a case and beamset is loaded")
        exit()

    try:
        beam_set = get_current("BeamSet")
    except:
        messagebox.showerror("Error","No beam set is loaded. PLease ensure a beamset is loaded")
        exit()
    
    # Log the patient
    logger.info(f"PatientID: {get_current('Patient').PatientID}")
    # Create the main window
    Root = Tk()
    Root.title("PET Auto function")
    Root.iconbitmap(r"\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\PET Auto Tool\bap_icon.ico")
    Root.configure(background='white')
    Root.geometry("600x800")

    # Set the maximum height for the window
    max_height = 800
    
    # Create a canvas and a scrollbar
    canvas = tk.Canvas(Root, height=max_height, background='white')
    scrollbar = tk.Scrollbar(Root, orient="vertical", command=canvas.yview)
    canvas.configure(yscrollcommand=scrollbar.set)
    
    # Create a frame to hold the content
    root = tk.Frame(canvas, background='white')
    
    # Add the frame to the canvas
    canvas.create_window((0, 0), window=root, anchor='nw')
    
    # Pack the canvas and scrollbar
    canvas.pack(side="left", fill="both", expand=True)
    scrollbar.pack(side="right", fill="y")

    # Update the canvas scroll region
    def on_frame_configure(event):
        canvas.configure(scrollregion=canvas.bbox("all"))

    root.bind("<Configure>", on_frame_configure)
    
    # Update the canvas scroll region
    root.update_idletasks()
    canvas.config(scrollregion=canvas.bbox("all"))

    image = PhotoImage(file=r"\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\PET Auto Tool\logo_white.png")

    image_label = Label(root, image=image, background='white')
    image_label.pack()
    # Create a label for the radio buttons
    label = Label(root, text="Select the units of the PET(s):",background='white')
    label.pack(pady=20)

    # Create a variable to store the selected option
    var = StringVar()

    # Create radio buttons (side by side)
    radio_frame = Frame(root,background='white')
    radio_button1 = Radiobutton(radio_frame, text="SUV value [g/mL]", variable=var, value="Liver SUV",background='white')
    radio_button2 = Radiobutton(radio_frame, text="Counts value [Bq/mL]", variable=var, value="Counts",background='white')
    radio_button1.pack(side=LEFT)
    radio_button2.pack(side=LEFT)
    radio_frame.pack()

    options_frame = Frame(root,background='white')


    # Create an input box

    entry_label = Label(root, text="Enter the mean Liver SUV (g/mL) threshold:",background='white')
    entry_label.pack(pady=10)
    entry = Entry(root)
    entry.pack()

    # Create the main options frame
    options_frame = Frame(root, background='white', bd=1, relief='solid')

    # Add label for "Patient and Dose information"
    patient_info_label = Label(options_frame, text="Patient and Dose information", background='white', font=("Arial", 14))
    patient_info_label.pack(side=TOP, pady=5)

    # Create a frame for Weight entry
    weight_frame = Frame(options_frame, background='white')
    weight_label = Label(weight_frame, text="Weight (kg):", background='white')
    weight_label.pack(side=LEFT)
    weight_entry = Entry(weight_frame)
    weight_entry.pack(side=LEFT)
    weight_frame.pack(side=TOP, pady=5)

    # Create a frame for Height entry
    height_frame = Frame(options_frame, background='white')
    height_label = Label(height_frame, text="Height (cm):", background='white')
    height_label.pack(side=LEFT)
    height_entry = Entry(height_frame)
    height_entry.pack(side=LEFT)
    height_frame.pack(side=TOP, pady=5)

    # Create a frame for Sex radiobuttons
    sex_frame = Frame(options_frame, background='white')
    var_sex = StringVar()
    var_sex.set(None)
    # Make Label for This frame
    sex_label = Label(sex_frame, text="Sex", background='white', font=("Arial", 14))
    sex_label.pack()
    radio_button_sex_M = Radiobutton(sex_frame, text="Male", variable=var_sex, value="Male", background='white')
    radio_button_sex_F = Radiobutton(sex_frame, text="Female", variable=var_sex, value="Female", background='white')
    radio_button_sex_M.pack(side=LEFT)
    radio_button_sex_F.pack(side=LEFT)
    sex_frame.pack(side=TOP, pady=5)

    # Create a frame for Dose entry
    dose_frame = Frame(options_frame, background='white')
    dose_label = Label(dose_frame, text="Dose (mCi):", background='white')
    dose_label.pack(side=LEFT)
    dose_entry = Entry(dose_frame)
    dose_entry.pack(side=LEFT)
    dose_frame.pack(side=TOP, pady=5)

    # Create a frame for time to uptake
    time_frame = Frame(options_frame, background='white')
    time_label = Label(time_frame, text="Time to uptake (min):", background='white')
    time_label.pack(side=LEFT)
    time_entry = Entry(time_frame)
    time_entry.delete(0, END)
    time_entry.insert(0, "60")
    time_entry.pack(side=LEFT)
    time_frame.pack(side=TOP, pady=5)


    # Pack the main options frame
    options_frame.pack(pady=10)

    # Disable all the widgets in the options frame if var value is "Liver SUV"
    def disable_widgets():
        weight_entry.config(state='disabled')
        height_entry.config(state='disabled')
        radio_button_sex_M.config(state='disabled')
        radio_button_sex_F.config(state='disabled')
        dose_entry.config(state='disabled')
        time_entry.config(state='disabled')

    def toggle_widgets(*args):
        if var.get() == "Liver SUV":
            disable_widgets()
        else:
            weight_entry.config(state='normal')
            height_entry.config(state='normal')
            radio_button_sex_M.config(state='normal')
            radio_button_sex_F.config(state='normal')
            dose_entry.config(state='normal')
            time_entry.config(state='normal')

    var.trace_add('write', toggle_widgets)
    


    # Create a frame for the pet scans and the CTV rois to select
    pet_roi_frame = Frame(root, background='white')

    #Pet selection
    pet_frame = Frame(pet_roi_frame, background='white',bd=1, relief='solid')
    
    # Create a label for the checkboxes
    pet_label = Label(pet_frame, text="Please select the PET scan(s) you want to draw on:",background='white')
    pet_label.pack(pady=10)

    # List of PET scans
    pet_examinations = [x.Name for x in case.Examinations if 'pet' in x.EquipmentInfo.Modality.lower()]

    # Create variables to store the selected PET scans
    pet_vars = {}

    # Create checkboxes for each PET scan
    for pet_exam in pet_examinations:
        pet_var = IntVar()
        pet_checkbox = Checkbutton(pet_frame, text=pet_exam, variable=pet_var, background='white')
        pet_checkbox.pack()
        pet_vars[pet_exam] = pet_var

    # Set the initial state of the radio buttons to None
    var.set(None)

    # Pack the pet frame
    pet_frame.pack(side=LEFT,anchor='n', padx = (0,20))


    #ctv ROI selection
    roi_frame = Frame(pet_roi_frame, background='white',bd=1, relief='solid')
    roi_label = Label(roi_frame, text="Please select the CTV ROIs you want to intersect with:",background='white')
    roi_label.pack(pady=10)
    ctv_rois = [r.OfRoi.Name for r in get_current("Plan").GetTotalDoseStructureSet().RoiGeometries if r.OfRoi.Name[:3] == 'CTV']
    ctv_vars = {}
    for ctv_roi in ctv_rois:
        ctv_var = IntVar()
        ctv_checkbox = Checkbutton(roi_frame, text=ctv_roi, variable=ctv_var, background='white')
        ctv_checkbox.pack()
        ctv_vars[ctv_roi] = ctv_var
    
    roi_frame.pack(side=RIGHT)

    # Pack the pet_roi_frame
    pet_roi_frame.pack(pady=10)

    # Create a button to start the analysis
    go_button = Button(root, text="Run", width=10, command=start_analysis)
    go_button.pack(pady=20)


    user_info = f"Mauricio Acosta - mauricioac@baptisthealth.net - Version {__version__}"
    user_info_label = Label(root, text=user_info, bg="#2EA84A")
    user_info_label.pack(pady=(40,0),fill='both')

    # Run the main loop
    Root.mainloop()
