from connect import get_current
import os

def get_doses_on_examination():
    case = get_current('Case')
    for doe in case.TreatmentDelivery.FractionEvaluations[0].DoseOnExaminations:
        if doe.OnExamination.Name.strip()[:4]=='CTTX':
            print("Found Dose Evaluation on CTTX = ", doe.OnExamination.Name)
            return doe
    print("Did not find Dose Evaluation on CTTX")
    return None

def get_dose_evaluation(doe,model_name='carabe'):
    for de in doe.DoseEvaluations:
        if hasattr(de,'CustomDoseProperties'):
            cdp = de.CustomDoseProperties
            if hasattr(cdp,'Description'):
                desc = cdp.Description
                if model_name in desc.lower():
                    print("Found Dose Evaluation on model = ", desc)
                    return de

def get_dose_statistics_rbe_models(dose_evaluation,roi_name,num_fractions=1):
    ''' This function will return max,min,avg dose, and max_1_percent for given dose_evaluation'''
    # Get min [cGy]
    min_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,
                                                DoseType='Min')
    max_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,
                                                DoseType='Max')
    avg_dose_per_fraction = dose_evaluation.GetDoseStatistic(RoiName = roi_name,
                                                DoseType='Average')
    max_1_percent_dose_per_fraction = dose_evaluation.GetDoseAtRelativeVolumes(RoiName = roi_name,
                                                                               RelativeVolumes=[.01])[0]
    result = {'min_dose':min_dose_per_fraction*num_fractions/100,
              'max_dose':max_dose_per_fraction*num_fractions/100,
                'avg_dose':avg_dose_per_fraction*num_fractions/100,
                'max_1_percent_dose':max_1_percent_dose_per_fraction*num_fractions/100}
    return result


def get_dose_stats(rois,rbe_model_name):
    num_fractions = int(get_current("BeamSet").FractionationPattern.NumberOfFractions)
    doe = get_doses_on_examination()
    de = get_dose_evaluation(doe,rbe_model_name)
    dose_stats = {}
    for roi in rois:
        dose_stats[roi] = get_dose_statistics_rbe_models(de,roi,num_fractions)
    return dose_stats

def write_to_csv(fp=r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\NK\stats',name = 'stats.csv'):
    rois = [x.Name for x in get_current('Case').PatientModel.RegionsOfInterest if x.Name[:4].lower() in ['ctvp', 'ctvn'] and 'Enhance' in x.Name]
    patient = get_current('Patient').Name
    plan = get_current('Plan').Name
    dose_stats_carabe = get_dose_stats(rois,'carabe')
    dose_stats_mcnamara = get_dose_stats(rois,'mcnamara')
    dose_stats_wedenberg = get_dose_stats(rois,'wedenberg')
    with open(os.path.join(fp,name),'w') as f:
        f.write('Patient,Plan,ROI,RBE_Model,Min Dose [Gy],Max Dose [Gy],Avg Dose [Gy],Max 1% Dose [Gy]\n')
        for roi in dose_stats_carabe:
            f.write(patient+','+plan+','+roi+','+'Carabe'+','+','.join([str(x) for x in dose_stats_carabe[roi].values()])+'\n')
        for roi in dose_stats_mcnamara:
            f.write(patient+','+plan+','+roi+','+'McNamara'+','+','.join([str(x) for x in dose_stats_mcnamara[roi].values()])+'\n')
        for roi in dose_stats_wedenberg:
            f.write(patient+','+plan+','+roi+','+'Wedenberg'+','+','.join([str(x) for x in dose_stats_wedenberg[roi].values()])+'\n')

    print('Wrote stats to',os.path.join(fp,name))
    return

write_to_csv()
