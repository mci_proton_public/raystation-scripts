from connect import get_current, set_progress
import numpy as np
import re
import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox,MessageBoxButtons, MessageBoxIcon
import subprocess
import sys


__version__ = "0.0.3"
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

try:
    import pandas as pd

except:
    set_progress("Installing Missing Libraries",50)
    install('pandas')
    MessageBox.Show("Missing Libraries installed. Please re-run script")
    exit()


def get_letd(roi):
    ''' This function will return the LETd values for a given roi for the current plan.

    If there are multiple phases, then the dose-weighted letd is returned.
    '''
    plan = get_current("Plan")
    # Get each beamset in plan
    beamsets = plan.BeamSets
    # For our purpose, the structure set and dosegrid is the same across each beamset
    # use the first one to get shared data
    bs_0 = beamsets[0]
    dg = bs_0.GetDoseGrid()
    ss = bs_0.GetStructureSet()
    roi_data = ss.RoiGeometries[roi]
    # Get the ROI mask
    roi_mask = roi_data.GetRoiGeometryAsVoxels(Corner = dg.Corner, VoxelSize = dg.VoxelSize, NrVoxels = dg.NrVoxels)

    roi_idx = [i for i,x in enumerate(np.ravel(roi_mask)) if x != 0]

    num_dose_weighted = np.array([0.0]*len(roi_idx))
    den_dose_weighted = np.array([0.0]*len(roi_idx))

    if len(beamsets) == 1:
        letd = np.ravel(bs_0.FractionDose.DoseValues.PhysicalData.DoseAveragedLetData/10)[roi_idx]
        return letd
    #else
    for bs in beamsets:
        # Get dose data
        dose_data = bs.FractionDose.DoseValues.DoseData
        # Number of fractions
        n_fractions = bs.FractionationPattern.NumberOfFractions

        # Get total dose
        dose_data *= n_fractions

        dose_data = np.ravel(dose_data)[roi_idx]
        # Get letd data
        letd = np.ravel(bs.FractionDose.DoseValues.PhysicalData.DoseAveragedLetData/10)[roi_idx]
        
        letd_time_dose = dose_data*letd
        num_dose_weighted += letd_time_dose
        den_dose_weighted += dose_data
    composite_letd = num_dose_weighted/den_dose_weighted
    return composite_letd

def get_letd_stats_roi(roi):
    letd_in_roi = get_letd(roi)
    # Make dictionary with max, min, mean, std
    letd_stats = {}
    letd_stats["max"] = np.max(letd_in_roi)
    letd_stats["min"] = np.min(letd_in_roi)
    letd_stats["mean"] = np.mean(letd_in_roi)
    #letd_stats["std"] = np.std(letd_in_roi, ddof = 1) # Doesn't make sense since distribution isn't approx normal
    letd_stats["sum"] = np.sum(letd_in_roi)
    #letd_stats["data"] = letd_in_roi
    return letd_stats

def get_all_letd_stats():
    rois = [x.OfRoi.Name for x in get_current("Plan").BeamSets[0].GetStructureSet().RoiGeometries if re.match(r'^CTV[n|p]_(\d+_)?[0|0.5|1|2]_', x.OfRoi.Name)]
    data = []
    for roi_idx,roi in enumerate(rois):
        set_progress(f"Getting LETd stats for {roi}", roi_idx/len(rois)*100)
        try:
            stats = get_letd_stats_roi(roi)
        except Exception as e:
            print(f"ERROR :\n",{e})
            stats = {}
            stats["max"] = None
            stats["min"] = None
            stats["mean"] = None
            stats["sum"] = None

        stats["roi"] = roi
        data.append(stats)

    df = pd.DataFrame(data)
    df['Patient'] = get_current("Patient").Name
    df['PID'] = get_current("Patient").PatientID
    df['Plan'] = get_current("Plan").Name
    return df

if __name__ == "__main__":
    patient = get_current("Patient").Name
    plan = get_current("Plan").Name
    output_fp = r"\\ad\dfs\Shared Data\MCI Radiation Oncology\MauricioAcosta\NK PET\stats\LETd"
    fname = f"{patient}_{plan}_letd_stats.xlsx"
    output_fp = os.path.join(output_fp, fname)
    df = get_all_letd_stats()
    df.to_excel(output_fp, index = False)