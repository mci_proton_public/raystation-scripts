def get_slice(slice_num,reshaped_pixel_data):
    ''' Returns a single slice from the reshaped pixel data by merging consecutive slices given that the data is 16 bit but stored as 2 8 bit images for each slice.'''
    
    first_slice = reshaped_pixel_data[int((slice_num-1)*2)]
    second_slice = reshaped_pixel_data[int(slice_num *2-1)]
    a = first_slice.flatten()
    b = second_slice.flatten()
    c = np.reshape([((i << 8) | j) >> 1 for i, j in zip(a, b)], np.shape(first_slice))
    return c

def reshape_data(pixel_data):
    '''Reshapes the pixel data to the correct dimensions'''
    reshaped_pixel_data = np.reshape(pixel_data, (int(len(pixel_data)/(512**2)), 512, 512))
    return reshaped_pixel_data

def get_suv_slice(slice_num,pixel_data,suv_data):
    rs = reshape_data(pixel_data)
    slice = get_slice(slice_num,rs)
    return slice*suv_data[slice_num-1]

