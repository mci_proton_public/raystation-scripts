from connect import get_current, set_progress
import numpy as np
import os
import re

import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox,MessageBoxButtons, MessageBoxIcon
import subprocess
import sys

import pickle
import datetime



__version__ = "0.1.5"

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

try:
    import pandas as pd

except:
    set_progress("Installing Missing Libraries",50)
    install('pandas')
    MessageBox.Show("Missing Libraries installed. Please re-run script")
    exit()

def get_doses_on_examination():
    case = get_current('Case')
    for doe in case.TreatmentDelivery.FractionEvaluations[0].DoseOnExaminations:
        if doe.OnExamination.Name.strip()[:4]=='CTTX':
            return doe
    print("Did not find Dose on Examination CTTX")
    return None

def get_dose_evaluations(doe,model_name='carabe'):
    dose_evaluations = []
    beamsets = [x.BeamSetIdentifier() for x in get_current('Plan').BeamSets]
    for de in doe.DoseEvaluations:
        if de.ForBeamSet.BeamSetIdentifier() in beamsets:
            if hasattr(de,'CustomDoseProperties'):
                cdp = de.CustomDoseProperties
                if hasattr(cdp,'Description'):
                    desc = cdp.Description
                    if model_name.lower() in desc.lower():
                        print("Found Dose Evaluation on model = ", desc)
                        dose_evaluations.append(de)
                        beamsets.remove(de.ForBeamSet.BeamSetIdentifier())
                        if len(beamsets) == 0:
                            return dose_evaluations
    return "Error: Not all beamsets have a dose evaluation"
                
def get_dose_stats_multiphase(dose_evaluations,rois,output_fol = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\dose_let_stats\dose_let_distributions'):
    ''' This method will return the dose statistics for the rois by summing the dose from each dose_evaluation'''
    plan = get_current("Plan")
    
    # We have to manually add the total dose from each beamset since the rbe models are ran on each beamset
    total_dose = np.zeros_like(np.ravel(dose_evaluations[0].DoseValues.DoseData))

    for de in dose_evaluations:
        # Get the number of fractions that corresponds to the beamset
        numberFractions = de.ForBeamSet.FractionationPattern.NumberOfFractions
        # Get the dose data
        dose_data = np.ravel(de.DoseValues.DoseData)/100
        total_dose += dose_data*numberFractions
    roi_stats = []
    for roi in rois:
        roi_dosegrid = dose_evaluations[0].GetDoseGridRoi(RoiName = roi)
        roi_idx = roi_dosegrid.RoiVolumeDistribution.VoxelIndices
        roi_weights = roi_dosegrid.RoiVolumeDistribution.RelativeVolumes
        roi_dose = total_dose[roi_idx]
        
        dfi = pd.DataFrame()
        dfi['dose'] = roi_dose
        dfi['weights'] = roi_weights
        dfi['index'] = roi_idx
        dfi.sort_values('dose',inplace=True, ascending=True)
        dfi['cum_weights'] = dfi['weights'].cumsum()

        records = dfi.to_records(index=False)

        fname = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f.pkl")
        export_path = os.path.join(output_fol,fname)
        # pickle the dfi
        with open(export_path, 'wb') as f:
            pickle.dump(records, f)
        
        min_dose = np.min(roi_dose)
        max_dose = np.max(roi_dose)
        avg_dose = np.average(roi_dose, weights=roi_weights)
        max_1_percent_dose = dfi.loc[dfi['cum_weights']>=0.99]['dose'].iloc[0]

        roi_stats.append({"ROI":roi,'Min':min_dose,'Max':max_dose,'Avg':avg_dose,'Max 1%':max_1_percent_dose,'Values':export_path})

    return roi_stats

def get_reg_stats(rois, output_fol = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\dose_let_stats\dose_let_distributions'):
    ''' Gets the regular total dose dose statistics for the rois in the list rois'''
    plan = get_current("Plan")
    dose_evaluation = plan.TreatmentCourse.TotalDose
    total_dose = np.ravel(dose_evaluation.DoseValues.DoseData)/100

    dose_stats = []
    for roi_name in rois:
        roi_dose_grid = dose_evaluation.GetDoseGridRoi(RoiName = roi_name)
        roi_idx = roi_dose_grid.RoiVolumeDistribution.VoxelIndices
        roi_dose = total_dose[roi_idx]
        roi_dose_weights = roi_dose_grid.RoiVolumeDistribution.RelativeVolumes

        dfi = pd.DataFrame()
        dfi['dose'] = roi_dose
        dfi['weights'] = roi_dose_weights
        dfi['index'] = roi_idx
        dfi.sort_values('dose',inplace=True, ascending=True)
        dfi['cum_weights'] = dfi['weights'].cumsum()

        records = dfi.to_records(index=False)

        fname = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f.pkl")
        export_path = os.path.join(output_fol,fname)
        # pickle the dfi
        with open(export_path, 'wb') as f:
            pickle.dump(records, f) 
        


        min_dose = np.min(roi_dose)
        max_dose = np.max(roi_dose)
        avg_dose = np.average(roi_dose, weights=roi_dose_weights)
        max_1_percent_dose = dfi.loc[dfi['cum_weights']>=0.99]['dose'].iloc[0]

        dose_stats.append({'ROI':roi_name,'Min':min_dose,'Max':max_dose,'Avg':avg_dose,'Max 1%':max_1_percent_dose,'Values':export_path})
        #dose_stats[roi_name] = {'min_dose':min_dose_per_fraction*num_fractions/100,'max_dose':max_dose_per_fraction*num_fractions/100,'avg_dose':avg_dose_per_fraction*num_fractions/100,'max_1_percent_dose':max_1_percent_dose_per_fraction*num_fractions/100}
    return dose_stats

# Make a form where user can select rois
def get_rois():
    case = get_current('Case')
    rois = []
    for roi in case.PatientModel.RegionsOfInterest:
        rois.append(roi.Name)
    return sorted(rois)

# make a tkinter form where user can select rois
def get_rois_tk():
    import tkinter as tk
    from tkinter import ttk

    root = tk.Tk()
    root.title("Select ROIs")

    main_frame = ttk.Frame(root)
    main_frame.pack(fill=tk.BOTH, expand=1)

    canvas = tk.Canvas(main_frame)
    canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    scrollbar = ttk.Scrollbar(main_frame, orient=tk.VERTICAL, command=canvas.yview)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    scrollable_frame = ttk.Frame(canvas)
    scrollable_frame.bind(
        "<Configure>",
        lambda e: canvas.configure(
            scrollregion=canvas.bbox("all")
        )
    )

    canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")
    canvas.configure(yscrollcommand=scrollbar.set)

    rois = get_rois()
    rois_selected = []

    def on_select(event):
        rois_selected.append(event.widget.cget("text"))

    for roi in rois:
        cb = ttk.Checkbutton(scrollable_frame, text=roi)
        cb.bind("<Button-1>", on_select)
        cb.pack(anchor='w')


    # Add button to submit rois
    def on_submit():
        root.destroy()

    submit_button = ttk.Button(root, text="Submit", command=on_submit)
    submit_button.pack()

    root.mainloop()
    return rois_selected

###########

def get_letd_for_rois(rois,output_fol = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\dose_let_stats\dose_let_distributions'):
    '''This function will return the dose weighted letd across all beamsets'''
    plan = get_current("Plan")
    beamsets = plan.BeamSets
    letd_stats = []
    for roi in rois:
        roi_idx = beamsets[0].FractionDose.GetDoseGridRoi(RoiName = roi).RoiVolumeDistribution.VoxelIndices # Assume its the same for all beamsets
        roi_weights = beamsets[0].FractionDose.GetDoseGridRoi(RoiName = roi).RoiVolumeDistribution.RelativeVolumes # Assume its the same for all beamsets

        num_dose_weighted = np.zeros_like(roi_idx, dtype=float)
        den_dose_weighted = np.zeros_like(roi_idx, dtype=float)
        if len(beamsets) == 1:
            letd = np.ravel(beamsets[0].FractionDose.DoseValues.PhysicalData.DoseAveragedLetData/10)[roi_idx]
            dfi = pd.DataFrame()
            dfi['letd'] = letd
            dfi['weights'] = roi_weights
            dfi['index'] = roi_idx
            dfi.sort_values('letd',inplace=True, ascending=True)
            dfi['cum_weights'] = dfi['weights'].cumsum()
            max1 = dfi.loc[dfi['cum_weights']>=0.99]['letd'].iloc[0]

            records = dfi.to_records(index=False)
            fname = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f.pkl")
            export_path = os.path.join(output_fol,fname)
            # pickle the dfi
            with open(export_path, 'wb') as f:
                pickle.dump(records, f)

            result = {"ROI":roi,"Model":"LETd [keV/um]","Min":np.min(letd),"Max":np.max(letd),"Avg":np.average(letd,weights=roi_weights),"Max 1%":max1,"Values":export_path}
            letd_stats.append(result)
            continue
        else:
            for bs in beamsets:
                # Get dose data
                dose_data = bs.FractionDose.DoseValues.DoseData
                # Number of fractions
                n_fractions = bs.FractionationPattern.NumberOfFractions

                # Get total dose
                dose_data *= n_fractions

                roi_dose = np.ravel(dose_data)[roi_idx]
                # Get letd data
                roi_letd = np.ravel(bs.FractionDose.DoseValues.PhysicalData.DoseAveragedLetData/10)[roi_idx]
                
                
                letd_time_dose = roi_dose*roi_letd
                num_dose_weighted += letd_time_dose
                den_dose_weighted += roi_dose
            composite_letd = num_dose_weighted/den_dose_weighted
            dfi = pd.DataFrame()
            dfi['letd'] = composite_letd
            dfi['weights'] = roi_weights
            dfi['index'] = roi_idx
            dfi.sort_values('letd',inplace=True, ascending=True)
            dfi['cum_weights'] = dfi['weights'].cumsum()
            max1 = dfi.loc[dfi['cum_weights']>=0.99]['letd'].iloc[0]

            records = dfi.to_records(index=False)
            fname = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f.pkl")
            export_path = os.path.join(output_fol,fname)
            # pickle the dfi
            with open(export_path, 'wb') as f:
                pickle.dump(records, f)

            result = {"ROI":roi,"Model":"LETd [keV/um]","Min":np.min(composite_letd),"Max":np.max(composite_letd),"Avg":np.average(composite_letd,weights=roi_weights),"Max 1%":max1,"Values":export_path}
            letd_stats.append(result)
    return letd_stats

#######

def main():
    rois = get_rois_tk()
    if rois == []:
        rois = ["Brain", "BrainStem", "Chiasm", "Cochlea_L", "Cochlea_R", "Eye_L", "Eye_R", "Lens_L", "Lens_R", "OpticNerve_L", "OpticNerve_R", "PharynxConst", "Retina_L", "Retina_R", "SpinalCord"]

    # Get stats for regular dose distribution
    df = pd.DataFrame(columns = ["Patient","Plan","Model","ROI","Min","Max","Avg","Max 1%","Values"])

    
    status = 20
    set_progress("Getting RBE_1.1 stats",status)
    model = "RBE_1.1 [Gy]"
    roi_stats = get_reg_stats(rois)

    dfi = pd.DataFrame(roi_stats)
    dfi['Model'] = model
    df = pd.concat([df,dfi],ignore_index=True)
    


    # Get dose evaluations on CTTX
    doe = get_doses_on_examination()
    # Get RBE Model stats
    for model in ['Carabe','McNamara','Wedenberg']:
        status += 20
        set_progress(f"Getting {model} stats",status)
        model_des = get_dose_evaluations(doe,model)
        dose_stats = get_dose_stats_multiphase(model_des,rois)
        dfi = pd.DataFrame(dose_stats)
        dfi['Model'] = model + " [Gy]"
        
        df = pd.concat([df,dfi],ignore_index=True)
        

    set_progress("Getting LETd stats",90)
    
    letd_stats = get_letd_for_rois(rois)
    dfi = pd.DataFrame(letd_stats)
    dfi['Model'] = "LETd [keV/um]"
    df = pd.concat([df,dfi],ignore_index=True)

    patient = get_current("Patient").Name
    plan = get_current("Plan").Name

    df['Patient'] = patient
    df['Plan'] = plan
    return df


if __name__ == "__main__":
    df = main()
    set_progress("Writing results to CSV",95)
    export_loc = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\dose_let_stats'
    fname = f"{get_current('Patient').Name}_{get_current('Plan').Name}_dose_stats.csv"
    df.to_csv(os.path.join(export_loc,fname),index=False)
    print("Wrote dose stats to dose_stats.csv")
    exit()