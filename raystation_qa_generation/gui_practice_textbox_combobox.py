# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 11:01:03 2023

@author: mau22560
"""

# WinForms example
# Creates a GUI that prompts the user to select an ROI
# The center of this ROI will be used as to create
# isocenter data for the current beam set
from connect import *
import clr
clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Drawing")
from System.Windows.Forms import Application, Form, Label, ComboBox, Button,TextBox,CheckedListBox,SelectionMode,CheckBox,Appearance
from System.Drawing import Point, Size
import os

# Define Forms class that will prompt the user to select an
# ROI for creating isocenter data for the current beam set
class SelectROIForm(Form):

    def __init__(self, plan):
        # Set the size of the form
        self.Size = Size(350, 300)
        # Set title of the form
        self.Text = 'QA Auto Creation/Export'
        # Add a label
        label = Label()
        label.Text = 'Select QA Setup'
        label.Location = Point(15, 15)
        label.AutoSize = True
        self.Controls.Add(label)
        
        #Add checkboxes for digiphant or SW
        #Add a checkbox
        self.checkbox_DP=CheckBox()
        self.checkbox_DP.Text="DigiPhant"
        #self.checkbox_DP.Appearance = Appearance.Button;
        self.checkbox_DP.Location=Point(15,40)
        self.checkbox_DP.AutoSize = True
        self.Controls.Add(self.checkbox_DP)
        
        self.checkbox_SW=CheckBox()
        self.checkbox_SW.Text="SolidWater"
        #self.checkbox_DP.Appearance = Appearance.Button;
        self.checkbox_SW.Location=Point(100,40)
        self.checkbox_SW.AutoSize = True
        self.Controls.Add(self.checkbox_SW)
        
        
        label2 = Label()
        label2.Text = 'Select approved BeamSet(s) for QA creation/export'
        label2.Location = Point(15, 70)
        label2.AutoSize = True
        self.Controls.Add(label2)
        '''
        # Add a ComboBox that will display the ROI:s to select
        # Define the items to show up in the combobox,
        # we only want to show ROI:s with geometries
        # defined in the structure set of the current plan
        structure_set = plan.GetStructureSet()
        roi_names = [rg.OfRoi.Name for rg in structure_set.RoiGeometries if rg.PrimaryShape != None]
        self.combobox = ComboBox()
        self.combobox.DataSource = roi_names
        self.combobox.Location = Point(15, 60)
        self.combobox.AutoSize = True
        self.Controls.Add(self.combobox)
        '''
        
        
        #Add a textbox
        #self.textbox=TextBox()
        #self.textbox.Location=Point(15,80)
        #self.textbox.AutoSize = True
        #self.Controls.Add(self.textbox)
        
        #Add a checkedlistbox
        self.checkedlistbox=CheckedListBox()
        self.checkedlistbox.Location=Point(15,100)
        self.checkedlistbox.AutoSize = True
        self.Controls.Add(self.checkedlistbox)
        
        #self.listbox.SelectionMode = SelectionMode.MultiExtended;
        self.checkedlistbox.BeginUpdate();
        for beam_set in plan.BeamSets:
            if beam_set.Review.ApprovalStatus == "Approved":
                beam_set_name=beam_set.BeamSetIdentifier().split(":")[-1]
                self.checkedlistbox.Items.Add(beam_set_name);
        self.checkedlistbox.EndUpdate();
        # Add button to press OK and close the form
        button = Button()
        button.Text = 'OK'
        button.AutoSize = True
        button.Location = Point(135, 220)
        button.Click += self.ok_button_clicked
        self.Controls.Add(button)

    def ok_button_clicked(self, sender, event):
        # Method invoked when the button is clicked
        # Save the selected ROI name
        #self.roi_name = self.combobox.SelectedValue
        #self.folderpath=self.textbox.Text
        self.plans_to_create_export=[x for x in self.checkedlistbox.CheckedItems]
        print(self.plans_to_create_export)
        
        # Close the form
        self.Close()

# Access current plan and show the form
plan = get_current('Plan')
# Create an instance of the form and run it
form = SelectROIForm(plan)
Application.Run(form)
"""
# Create isocenter data, use the center of the selected ROI as position
structure_set = plan.GetStructureSet()
roi_center = structure_set.RoiGeometries[form.roi_name].GetCenterOfRoi()
beam_set = get_current('BeamSet')
isocenter_data = beam_set.CreateDefaultIsocenterData(Position={'x':roi_center.x,
                                                               'y':roi_center.y,
                                                               'z':roi_center.z})

"""

























