from connect import *
import os


def main():
    
    patient = get_current("Patient")
    patient_name=patient.Name
    patient_ID=patient.PatientID
    case = get_current("Case")
    
    #mod_plans = ['03cm_00mm_1RP_RS', '03cm_00mm_5RP_RS', '03cm_05mm_5RP_RS', '03cm_08mm_1RP_RS', '03cm_08mm_5RP_RS', '03cm_10mm_3RP_RS', '02cm_05mm_1RP_RS', '02cm_05mm_5RP_RS', '03cm_15mm_1RP_RS', '03cm_15mm_3RP_RS', '05cm_00mm_5RP_RS', '02cm_08mm_3RP_RS', '02cm_08mm_5RP_RS', '02cm_15mm_3RP_RS', '02cm_00mm_3RP_RS']
    
    all_plans = [x for x in case.TreatmentPlans if '_Split' in x.Name] # and x.Name in [y+'_Split' for y in mod_plans]]
    num_plans = len(all_plans)
    
    for idx,plan in enumerate(all_plans):
        
        set_progress(f"{plan.Name} [{idx+1}/{num_plans}]",int(idx/num_plans *100))
        
        plan.SetCurrent()
           
        dosegrid={}
        dosegrid['x']=plan.GetTotalDoseGrid().VoxelSize.x
        dosegrid['y']=plan.GetTotalDoseGrid().VoxelSize.y
        dosegrid['z']=plan.GetTotalDoseGrid().VoxelSize.z
        #Create QA plan and export
        #Here we will create QA plan for all beamsets that have been approved
        print("-------------")
        print("QA Generation")
        print(" ")
            
        for beam_set in plan.BeamSets:
            beam_set_name=beam_set.BeamSetIdentifier().split(":")[-1]
            qa_name=beam_set_name

            beam_set.CreateQAPlan(PhantomName=r"PSQA_WaterPhantom", PhantomId=r"PSQA_WaterPhantom", QAPlanName=qa_name, IsoCenter={ 'x': 0, 'y': 0, 'z': 0 }, DoseGrid=dosegrid, GantryAngle=0, CollimatorAngle=None, ComputeDoseWhenPlanIsCreated=False,CouchRotationAngle=0)
            qa_plan = [x for x in plan.VerificationPlans if x.BeamSet.DicomPlanLabel == qa_name][0]
            if dosegrid['x'] == .3:
                corner = { 'x': -25.05, 'y': 0, 'z': -24.3 }
                voxelsize = dosegrid
                numVoxels = { 'x': 167, 'y': 167, 'z': 162 }
            elif dosegrid['x'] == .2:
                corner = { 'x': -25, 'y': 0, 'z': -24.3 }
                voxelsize = dosegrid
                numVoxels = { 'x': 250, 'y': 250, 'z': 243 }
            elif dosegrid['x'] == .1:
                corner = { 'x': -25.0, 'y': 0, 'z': -24.25 }
                voxelsize = dosegrid
                numVoxels = { 'x': 500, 'y': 500, 'z': 485 }
            qa_plan.UpdateVerificationPlanDoseGrid(
                Corner=corner, 
                VoxelSize=voxelsize, 
                NumberOfVoxels=numVoxels
                )
            qa_plan.BeamSet.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False, RunEntryValidation=True)
            patient.Save()  
                        

                
           
        set_progress(f"Exporting QA plans to 'Plans Pending' folder [{idx+1}/{num_plans}]",int(idx/num_plans *100))
            
        verificationPlan = list(plan.VerificationPlans)[-1]
                    
        
        vp_name=plan.Name+':'+verificationPlan.BeamSet.DicomPlanLabel
                    
        bs_name=vp_name.split(":")[-1]
        #make folders
        patient_fp=r'\\ad\dfs\Shared Data\MCI Proton\QA\Patient QA\Patient QA Data\Plans Pending QA\{}_{}'.format(patient_name,patient_ID)
        if not os.path.exists(patient_fp):
            os.mkdir(patient_fp)
            
        plan_fp=os.path.join(patient_fp,bs_name)
                            
        if not os.path.exists(plan_fp):
            os.mkdir(plan_fp)
                    
        qa_export_fp=plan_fp
                            
                    
        result = verificationPlan.ScriptableQADicomExport(ExportFolderPath = qa_export_fp,
                                                            QaPlanIdentity = "Patient",
                                                            ExportExamination = False,
                                                            ExportExaminationStructureSet = False,
                                                            ExportBeamSet = True,
                                                            ExportBeamSetDose = True,
                                                            ExportBeamSetBeamDose = True,
                                                            IgnorePreConditionWarnings = False)


if __name__ == '__main__':
    main()