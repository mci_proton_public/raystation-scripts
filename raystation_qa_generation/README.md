# QA_creation_export_cpython.py
## Purpose
The primary function of this script is to automate the creation and export of Quality Assurance (QA) plans for proton therapy treatments. The script facilitates the selection of approved beamsets and the specification of QA setups (DigiPhant or SolidWater), streamlining the QA plan generation process and ensuring efficient and accurate QA plan exports to designated folders for further analysis.
## Workflow
* **Initialization**: Upon execution, the script initializes the RayStation environment, fetching the current plan details and presenting a graphical user interface (GUI) for user interaction.
* **QA Setup Selection**: Users are prompted to select the QA setup type (DigiPhant or SolidWater) through checkboxes. This choice determines the phantom configuration for the QA plan.
* **BeamSet Selection**: The script lists all approved beamsets within the current plan. Users can select one or multiple beamsets for which QA plans will be generated and exported.
* **QA Plan Generation**: For each selected beamset, the script generates a QA plan based on the specified setup type. It automatically assigns a naming convention to each QA plan, incorporating the beamset name and setup type for easy identification.
* **Export Process**: The generated QA plans are exported to a predefined network location, organized into patient-specific folders. The script handles folder creation and organization, ensuring that QA plans are correctly categorized for each patient and beamset.
* **Completion**: Upon successful generation and export of QA plans, the script provides a visual indication of completion through the GUI. In case of errors or issues during the process, error messages are displayed, guiding the user to resolve any problems encountered.
## How to use
* Press play
* Select the QA setup(s)
* Select the BeamSet(s)
* Click “Generate/Export QA Plan(s)”
