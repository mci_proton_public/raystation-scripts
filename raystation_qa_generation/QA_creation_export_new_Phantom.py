from connect import *
import os
import json
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import threading

__version__ = '0.0.2'

class create_export_QA:

    def __init__(self):
        # Set the size of the form
        self.plan=get_current("Plan")
        self.root=tk.Tk()
        self.root.iconbitmap(r"\\ad\dfs\Shared Data\MCI Proton\Mauricio\script_folder_do_not_edit\qa.ico")
        self.root.title('QA Auto Creation/Export')
        self.container=tk.Frame(master=self.root)
        self.container.pack()
        self.status=tk.StringVar(value=" ")
        #self.root.geometry("350x300")
        # Set title of the form

        padx=10
        pady=5
        
        tk.Label(master=self.container,text='Select approved BeamSet(s) for QA creation/export').pack(anchor='w',padx=padx,pady=pady)

        #Add a checkedlistbox
        self.beam_sets={}
        for beam_set in self.plan.BeamSets:
            if beam_set.Review:
                if beam_set.Review.ApprovalStatus == "Approved":
                    beam_set_name=beam_set.BeamSetIdentifier().split(":")[-1]
                    self.beam_sets[beam_set_name]=tk.BooleanVar();
        
        if len(self.beam_sets)==0:
            self.container.destroy()
            self.update_status("Error")
            self.root.config(bg='red')
            self.root.geometry("300x300")
            tk.Label(self.root, text="Error.\nNo Approved Beamsets", font='Arial 17 bold',bg='red').place(relx=0.5, rely=0.5, anchor='center')
            self.root.update_idletasks()
            tk.messagebox.showerror(title='Error',message="Error. Make sure plan is approved.")
        
        else:
            f2=tk.Frame(master=self.container,bg='white',relief='sunken',borderwidth=2)
            #tk.Label(master=f2,text=' ',bg='white').pack()
            for beamset,var in self.beam_sets.items():
                tk.Checkbutton(master=f2,variable=var,text=beamset,bg='white').pack(anchor='w',padx=2)
            
            tk.Label(master=f2,text=' ',bg='white').pack()
            f2.pack(anchor='w',padx=padx,pady=pady)

            # Add button to press OK and close the form
            button=tk.Button(master=self.container,command=self.button_clicked,text='Generate/Export\nQA Plan(s)').pack(pady=(20,5))
            self.root.mainloop()


    def __progressbar__(self):
        self.progressbar = tk.ttk.Progressbar(master=self.container,mode="indeterminate")
        self.progressbar.pack(pady=(5,20),padx=20,fill='x')
        self.progressbar.start()
    
    def __run__(self):
        try:
            self.update_status("Generating QA Plans")
            
            
            # Access current plan and show the form
            plan = self.plan
            #Get patient
            patient= get_current("Patient")
            patient_name=patient.Name
            patient_ID=patient.PatientID
            # Method invoked when the button is clicked
          
            
            self.plans_to_create_export=[]
            for beamset,var in self.beam_sets.items():
                if var.get():
                    self.plans_to_create_export.append(beamset)
            
            self.QA_plan_names=[]
            
            dosegrid={}
            dosegrid['x']=plan.GetTotalDoseGrid().VoxelSize.x
            dosegrid['y']=plan.GetTotalDoseGrid().VoxelSize.y
            dosegrid['z']=plan.GetTotalDoseGrid().VoxelSize.z
            #Create QA plan and export
            #Here we will create QA plan for all beamsets that have been approved
            print("-------------")
            print("QA Generation")
            print(" ")
            try:
                for beam_set in plan.BeamSets:
                    beam_set_name=beam_set.BeamSetIdentifier().split(":")[-1]
                    if beam_set.Review.ApprovalStatus == "Approved" and beam_set_name in self.plans_to_create_export:
                        qa_name=beam_set_name
                        self.QA_plan_names.append(plan.Name+":"+qa_name)
                        print(f"Creating {plan.Name}:   {qa_name}")
                        self.update_status(f"Creating {qa_name} QA Plan")
                        beam_set.CreateQAPlan(PhantomName=r"PSQA_WaterPhantom", PhantomId=r"PSQA_WaterPhantom", QAPlanName=qa_name, IsoCenter={ 'x': 0, 'y': 0, 'z': 0 }, DoseGrid=dosegrid, GantryAngle=0, CollimatorAngle=None, ComputeDoseWhenPlanIsCreated=False,CouchRotationAngle=0)
                        qa_plan = [x for x in plan.VerificationPlans if x.BeamSet.DicomPlanLabel == qa_name][0]
                        if dosegrid['x'] == .3:
                            corner = { 'x': -25.05, 'y': 0, 'z': -24.3 }
                            voxelsize = dosegrid
                            numVoxels = { 'x': 167, 'y': 120, 'z': 162 }
                        elif dosegrid['x'] == .2:
                            corner = { 'x': -25, 'y': 0, 'z': -24.3 }
                            voxelsize = dosegrid
                            numVoxels = { 'x': 250, 'y': 180, 'z': 243 }
                        elif dosegrid['x'] == .1:
                            corner = { 'x': -25.0, 'y': 0, 'z': -24.25 }
                            voxelsize = dosegrid
                            numVoxels = { 'x': 500, 'y': 360, 'z': 485 }
                        qa_plan.UpdateVerificationPlanDoseGrid(
                            Corner=corner, 
                            VoxelSize=voxelsize, 
                            NumberOfVoxels=numVoxels
                            )
                        qa_plan.BeamSet.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False, RunEntryValidation=True)
                        print(f"Done with {plan.Name}:   {qa_name}")
                        patient.Save()
                
                        # Get the verification plans. This gets all verification plans on a plan.
            except Exception as e:
                self.container.destroy()
                self.update_status("Error")
                self.root.config(bg='red')
                self.root.geometry("300x300")
                tk.Label(self.root, text="Error", font='Arial 17 bold',bg='red').place(relx=0.5, rely=0.5, anchor='center')
                self.root.update_idletasks()
                #tk.messagebox.showerror(title='Error',message="Error. Make sure the QA plan with the same name does not exist and rerun the script")
                tk.messagebox.showerror(title='Error',message=f"Error.\n\n {e}")
                
            else:
                self.update_status(f"Exporting QA plans to 'Plans Pending' folder")
                try:
                    print(" ")
                    print("-------------")
                    verificationPlans = plan.VerificationPlans
                    
                    for verificationPlan in verificationPlans:
                        vp_name=plan.Name+':'+verificationPlan.BeamSet.DicomPlanLabel
                        if vp_name in self.QA_plan_names:
                            bs_name=vp_name.split(":")[-1]
                            #make folders
                            patient_fp=r'\\ad\dfs\Shared Data\MCI Proton\QA\Patient QA\Patient QA Data\Plans Pending QA\{}_{}'.format(patient_name,patient_ID)
                            if not os.path.exists(patient_fp):
                                os.mkdir(patient_fp)
                            if "_SW" in bs_name:    
                                plan_fp=os.path.join(patient_fp,bs_name.split("_SW")[0])
                            else:
                                plan_fp=os.path.join(patient_fp,bs_name)
                            
                            if not os.path.exists(plan_fp):
                                os.mkdir(plan_fp)
                            
                            qa_export_fp=plan_fp
                            
                            # It is not necessary to assign all of the parameters, you only need to assign the
                            # desired export items. In this example we try to export with
                            # IgnorePreConditionWarnings=False. This is an option to handle possible warnings.
                            result = verificationPlan.ScriptableQADicomExport(ExportFolderPath = qa_export_fp,
                                                                              QaPlanIdentity = "Patient",
                                                                              ExportExamination = False,
                                                                              ExportExaminationStructureSet = False,
                                                                              ExportBeamSet = True,
                                                                              ExportBeamSetDose = True,
                                                                              ExportBeamSetBeamDose = True,
                                                                              IgnorePreConditionWarnings = False)

                            # It is important to read the result event if the script was successful.
                            # This gives the user a chance to see possible warnings that were ignored, if for
                            # example the IgnorePreConditionWarnings was set to True by mistake. The result
                            # also contains other notifications the user should read.
                            #LogCompleted(result)
                    self.__complete__()
                except Exception as e:
                    self.__error__(f"Error exporting Dicoms\n{e}")
        except Exception as e:
            # The script failed due to warnings or errors.
            #LogWarning(error)
            self.__error__("Something Went wrong\n"+e)
        finally:
            self.root.destroy()
    def check_clicked(self):
        
        beamset_counts=0

        for beamset,var in self.beam_sets.items():
            if var.get():
                beamset_counts+=1

        if beamset_counts>0:
            return True
        else:
            return False

    def update_status(self,message):
        self.status.set(message)
        self.root.update_idletasks()
    def __complete__(self):
        self.container.destroy()
        self.status.set("Complete!")
        self.root.config(bg='green')
        self.root.geometry("300x300")
        tk.Label(self.root, text="QA Export Complete", font='Arial 17 bold',bg='green').place(relx=0.5, rely=0.5, anchor='center')
        self.root.update_idletasks()
        tk.messagebox.showinfo(title="Complete",message="QA export complete")
    def __error__(self,error_message):
        self.container.destroy()
        self.update_status("Error")
        self.root.config(bg='red')
        self.root.geometry("300x300")
        tk.Label(self.root, text="Error", font='Arial 17 bold',bg='red').place(relx=0.5, rely=0.5, anchor='center')
        self.root.update_idletasks()
        tk.messagebox.showerror(title='Error',message=error_message)
    def button_clicked(self):
        #check that there is something selected
        ready=self.check_clicked()

        if not ready:
            tk.messagebox.showerror(title="Error",message="Ensure a setup and a beamset is selected")
        else:
            tk.Label(master=self.container,textvariable=self.status).pack(pady=(30,5))
            self.root.update_idletasks()
            self.__progressbar__()
            self.t=threading.Thread(target=self.__run__,daemon=True)
            self.t.start()


        

# Example on how to read the JSON error string.
def LogWarning(error):
    try:
        jsonWarnings = json.loads(str(error))
        # If the json.loads() works then the script was stopped due to
        # a non-blocking warning.
        print ("WARNING! Export Aborted!")
        print ("Comment:")
        print (jsonWarnings["Comment"])
        print ("Warnings:")
        # Here the user can handle the warnings. Continue on known warnings,
        # stop on unknown warnings.
        for w in jsonWarnings["Warnings"]:
            print (w)
    except ValueError:
        print ("Error occurred. Could not export.")

# The error was likely due to a blocking warning, and the details should be stated
# in the execution log.
# This prints the successful result log in an ordered way.
def LogCompleted(result):
    try:
        jsonWarnings = json.loads(str(result))
        print ("\nCompleted!")
        if len(jsonWarnings["Comment"])==0 and len(jsonWarnings["Warnings"])==0 and len(jsonWarnings["ExportNotifications"])==0:
            pass
        else: 
            print ("Comment:")
            print (jsonWarnings["Comment"])
            print ("Warnings:")
            for w in jsonWarnings["Warnings"]:
                print (w)
            print ("Export notifications:")

            # Export notifications is a list of notifications that the user should read.
            for w in jsonWarnings["ExportNotifications"]:
                print (w)
    except ValueError:
        print ("Error reading completion messages.")
        


# Create an instance of the form and run it
form = create_export_QA()