# Tx Beam Namer_Protons.py
## Purpose
The Tx Beam Namer_Protons.py script is designed to automate the naming of treatment beams in proton therapy plans within the RayStation treatment planning system. It generates concise, informative names for each beam based on its gantry angle, couch rotation, and other parameters, facilitating clear communication and documentation for physicists and dosimetrists. This script ensures consistency in beam naming conventions, improving workflow efficiency and reducing the potential for errors in treatment plan documentation.
## Workflow
* **Initialization**: The script starts by connecting to the current RayStation session and retrieving the selected plan and its beam sets.
* **Beam Set Processing**: For each beam set in the plan, the script:
	* Extracts the beam set name and identifies if the plan is a replan based on naming conventions.
* Determines the patient positioning (e.g., HeadFirstSupine, FeetFirstSupine, HeadFirstProne) to correctly interpret the gantry and couch angles.
* **Beam Naming**:
	* The script calculates the gantry angle and couch rotation for each beam.
	* It assigns a direction abbreviation (e.g., AP, PA, LL, RL) based on the gantry angle and patient position.
	* Beams are numbered sequentially within each beam set, with an offset to ensure unique numbering across the plan.
	* Additional details such as replan iteration (if applicable) and specific treatment techniques (e.g., repainting) are included in the beam name.
* **Name Assignment**: Each beam is renamed according to the generated naming convention, which includes information on beam number, direction, gantry angle, couch rotation, and any special treatment considerations.
* **Validation and Completion**:
	* The script checks for beam names exceeding the character limit imposed by the treatment delivery system (e.g., ARIA) and alerts the user if any names will be truncated.
* Upon successful renaming of all beams, the script completes its execution, leaving the treatment plan with clearly named beams ready for further processing or delivery.
** How to use
* Press play 😉

### Details
* For beams with no couch kick, the script changes beam name and description to p[Field Number]_[Field Direction]_G[Gantry Angle]
* For beams with a couch kick, the script changes beam name and description to p[Field Number]_G[Gantry Angle]_T[Couch Rotation]
* For repainting, the script appends "_RP" to the beam name and description*
	* Repainting plans are identified based on the minimum spot weight in the optimization settings (.03 MU). If the number of beams is greater than or equal to 6, then it automatically classifies it as repainting. If there are less than 6 beams, then the user is prompted to clarify if the plan is repainting (SDX cases also have a min MU of .03).
* For replans, the script identifies if the plan name contains R1, R2, etc. and adds a B, C, etc. after the p[Field Number] in the beam name and description 
* For boost fields, the script identifies the priority of the beam numbers based on the isocenter naming (i.e. P1, P2, etc.)
* This script labels beam directions accurately for head first supine, feet first supine, and head first prone 