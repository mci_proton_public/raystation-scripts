# Tx Beam Namer_Protons_2

Script name is "Tx Beam Namer_Protons_2" in the MCI RayStation 12A database

## Workflow:

1. Press play

- For beams with no couch kick, the script changes beam name and description to p[Field Number]_[Field Direction]_G[Gantry Angle]
- For beams with a couch kick, the script changes beam name and description to p[Field Number]_G[Gantry Angle]_T[Couch Rotation]
- For repainting, the script appends "_RP" to the beam name and description*
	- Repainting plans are identified based on the minimum spot weight in the optimization settings (.03 MU). If the number of beams is greater than or equal to 6, then it automatically classifies it as repainting. If there are less than 6 beams, then the user is prompted to clarify if the plan is repainting (SDX cases also have a min MU of .03).
- For replans, the script identifies if the plan name contains R1, R2, etc. and adds a B, C, etc. after the p[Field Number] in the beam name and description 

- For boost fields, the script identifies the priority of the beam numbers based on the isocenter naming (i.e. P1, P2, etc.)

- This script labels beam directions accurately for head first supine, feet first supine, and head first prone 
