from connect import get_current
import sys
import re
from string import ascii_uppercase as Letters
import numpy as np
import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox

__version__ = '0.0.1'

def exit_program():
    print("Exiting the program...")
    sys.exit(0)


# Lets set the start point for the beam numbering
def find_offset(plan, beamset_name):
    all_beamsets = []
    for bs in plan.BeamSets:
        bs_name = bs.BeamSetIdentifier().split(":")[1]
        num_beams = len(bs.Beams)
        isocenter_names = [beam_i.Isocenter.Annotation.Name for beam_i in bs.Beams]
        isocenter_num = np.min([int(re.search(r"P(\d)?", x).group().split('P')[1]) for x in isocenter_names])
        all_beamsets.append({'name': bs_name, 'priority': isocenter_num, 'num_beams': num_beams})
    all_beamsets = sorted(all_beamsets, key=lambda x: x['priority'])

    offset = 0
    for bs in all_beamsets:
        if bs['name'] != beamset_name:
            offset += bs['num_beams']
        else:
            return offset


plan = get_current("Plan")

for beam_set in plan.BeamSets:

    plan_name = beam_set.BeamSetIdentifier().split(":")[1]  # Really the beamset name

    # Let's seperate the plan name
    replan = None

    before_caret, after_caret = plan_name.split("^")

    replan = re.search(r"R[1-9]+", after_caret)
    if replan:
        replan = replan.group().split("R")[1]

    save_tx_angles = []
    beam_directions = []
    num_beams = beam_set.Beams.Count

    # Let's check if the plan is repainting.
    Repaint = ""
    min_spot_weight = plan.PlanOptimizations[0].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimits.x
    if min_spot_weight == 0.03 and num_beams % 2 == 0:
        if num_beams >= 6:
            Repaint = "_RP"
        else:
            Repaint = MessageBox.Show("Is this plan a repainting plan?", "Repainting?", 4)
            if Repaint == 6:
                Repaint = "_RP"
            else:
                Repaint = ""

    patient_position = beam_set.PatientPosition
    if patient_position == "HeadFirstSupine":
        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # AP
                direction = "AP"
            elif tx_g_angle == 180:  # PA
                direction = "PA"
            elif tx_g_angle == 90:  # LL
                direction = "LL"
            elif tx_g_angle == 270:  # RL
                direction = "RL"
            elif 0 < tx_g_angle < 90:  # LAO
                direction = "LAO"
            elif 90 < tx_g_angle < 180:  # LPO
                direction = "LPO"
            elif 270 < tx_g_angle < 360:  # RAO
                direction = "RAO"
            elif 180 < tx_g_angle < 270:  # RPO unless 180.1 ---> PA
                if tx_g_angle == 180.1:
                    direction = "PA"
                else:
                    direction = "RPO"

            beam_directions.append(direction)

    elif patient_position == "FeetFirstSupine":

        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # AP
                direction = "AP"
            elif tx_g_angle == 180:  # PA
                direction = "PA"
            elif tx_g_angle == 90:  # RL
                direction = "RL"
            elif tx_g_angle == 270:  # LL
                direction = "LL"
            elif 0 < tx_g_angle < 90:  # RAO
                direction = "RAO"
            elif 90 < tx_g_angle < 180:  # RPO
                direction = "RPO"
            elif 270 < tx_g_angle < 360:  # LAO
                direction = "LAO"
            elif 180 < tx_g_angle < 270:  # LPO unless 180.1 ---> PA
                if tx_g_angle == 180.1:
                    direction = "PA"
                else:
                    direction = "LPO"

            beam_directions.append(direction)

    elif patient_position == "HeadFirstProne":

        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # PA
                direction = "PA"
            elif tx_g_angle == 180:  # AP
                direction = "AP"
            elif tx_g_angle == 90:  # RL
                direction = "RL"
            elif tx_g_angle == 270:  # LL
                direction = "LL"
            elif 0 < tx_g_angle < 90:  # RPO
                direction = "RPO"

            elif 90 < tx_g_angle < 180:  # RAO
                direction = "RAO"
            elif 270 < tx_g_angle < 360:  # LPO
                direction = "LPO"
            elif 180 < tx_g_angle < 270:  # LAO unless 180.1 ---> AP
                if tx_g_angle == 180.1:
                    direction = "AP"
                else:
                    direction = "LAO"

            beam_directions.append(direction)

    try:
        offset_beam_num = find_offset(plan, plan_name)  # plan_name was created at the top (really beamset name)
    except Exception as e:
        MessageBox.Show(f"Please ensure the Isocenters are named correctly\n\n{e}", "Isocenter Naming Error", 0)
        exit_program()

    name_char_count = []
    for i in range(num_beams):

        couch = int(beam_set.Beams[i].CouchRotationAngle)

        if replan:
            if couch != 0:
                string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + "G" + str(save_tx_angles[i]) + "_T" + str(couch)  # save_tx_angles[s]

            else:
                if Repaint=="":
                    string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + beam_directions[i] + "_G" + str(save_tx_angles[i])  # save_tx_angles[s]
                else:
                    string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + "G" + str(save_tx_angles[i])  # save_tx_angles[s]
        else:

            if couch != 0:
                string = f"p{i+1+offset_beam_num:02d}" + "_" + "G" + str(save_tx_angles[i]) + "_T" + str(couch)  # save_tx_angles[s]

            else:
                if Repaint=="":
                    string = f"p{i+1+offset_beam_num:02d}" + "_" + beam_directions[i] + "_G" + str(save_tx_angles[i])  # save_tx_angles[s]
                else:
                    string = f"p{i+1+offset_beam_num:02d}" + "_" + "G" + str(save_tx_angles[i])  # save_tx_angles[s]

        # Add repaint suffix if needed. default  = ''

        string += Repaint
        beam_set.Beams[i].Name = string
        beam_set.Beams[i].Description = string

        name_char_count.append(string)

    # make a warning pop up if name is over 16 characters

    long_names = [x for x in filter(lambda x: len(x) > 16, name_char_count)]
    long_names = [x for x in long_names]
    format_names = ('\n'.join(long_names))
    if len(long_names) > 0:
        MessageBox.Show("Warning: The following beam names are over 16 characters long and will be truncated by ARIA:\n\n" + str(format_names))
        sys.exit()
    else:
        pass
