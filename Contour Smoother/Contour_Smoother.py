import sys, clr, re
clr.AddReference('System.Windows')
clr.AddReference("System.Windows.Forms")
from System.Windows.Media import Brushes
from System.Windows import *
from System.Windows.Controls import *
from System.Windows.Forms import MessageBox
from connect import *

__version__ = '0.0.1'

xaml = """
<Window
  xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
  xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
  Title="Contour Smoother" Height="700" Width="600" FontSize="16" Foreground="#FFFFFF">

  
    <DockPanel Background="#2c2c2c">
        <Border Margin="0,-6,0,0" Background="#2c2c2c" CornerRadius="5" Padding="5">
        </Border>
        <StackPanel Orientation="Horizontal" DockPanel.Dock="Top" Visibility="Collapsed" Name="MsgPanel">
        <TextBlock Margin="20,2,2,2" Name="Msgtext" TextWrapping="Wrap" Width="300" FontSize="16" Foreground="#FFFFFF"/>
    </StackPanel>





    <StackPanel Orientation="Horizontal" DockPanel.Dock="Bottom" HorizontalAlignment="Center" Margin="0,0,0,20">
        
        
        <Button Content="Go" Margin="5,10,0,10" Width="60" Click="StartAnalysis" IsDefault = 'True' FontSize="16" Padding="4" Background="#7f8080" Foreground="#FFFFFF">
                <Button.Resources>
                    <Style TargetType="Border">
                        <Setter Property="CornerRadius" Value="15"/>
                    </Style>
                </Button.Resources>
            </Button>
            <Button Content="Close" Margin="25,10,15,10" Width="70" Click="CloseWindow" FontSize="16" Padding="4" Background="#7f8080" Foreground="#FFFFFF">
                <Button.Resources>
                    <Style TargetType="Border">
                        <Setter Property="CornerRadius" Value="15"/>
                    </Style>
                </Button.Resources>
            </Button>
    </StackPanel>
    

    




    
    <StackPanel DockPanel.Dock="Bottom" HorizontalAlignment="Center" Orientation="Horizontal">
        <ComboBox x:Name="contraction" HorizontalAlignment="Center" Width = "100" Margin="0,0,0,20">
            <ComboBoxItem>0.28</ComboBoxItem>
            <ComboBoxItem>0.29</ComboBoxItem>
            <ComboBoxItem>0.3</ComboBoxItem>
        </ComboBox>    
    </StackPanel>
    



    <DockPanel DockPanel.Dock="Top" Margin="20">
            <TextBlock Text="Selected contours will be uniformly expanded by 3mm then contracted by the amount selected in the dropdown" TextWrapping="Wrap" FontSize="18" Background="#2c2c2c" Foreground="#FFFFFF" Margin="5" TextAlignment="Center" DockPanel.Dock="Top" FontStyle="Italic"/>
    </DockPanel>

    
    <DockPanel DockPanel.Dock="Top">
            <TextBlock Text="Select Contours to Smooth" TextWrapping="Wrap" FontSize="16" Background="#2c2c2c" Foreground="#FFFFFF" Margin="5" TextAlignment="Center" DockPanel.Dock="Top"/>
    </DockPanel>
    

    <DockPanel DockPanel.Dock="Bottom" Margin="10">
            <TextBlock Text="Select contraction amount in cm" TextWrapping="Wrap" FontSize="16" Background="#2c2c2c" Foreground="#FFFFFF" Margin="5" TextAlignment="Center" DockPanel.Dock="Top"/>
    </DockPanel>

 
    <StackPanel DockPanel.Dock="Bottom" HorizontalAlignment="Center" Orientation="Horizontal">           
            <Button Content="Select All" Width ="80" Margin="8" FontSize="14" Padding="4" Background="#7f8080" Foreground="#FFFFFF" Click="SelectAllClicked">
                <Button.Resources>
                    <Style TargetType="Border">
                        <Setter Property="CornerRadius" Value="15"/>
                    </Style>
                </Button.Resources>
            </Button>
            <Button Content="Deselect All" Width="100" Margin="8" FontSize="14" Padding="4" Background="#7f8080" Foreground="#FFFFFF" Click="DeselectAllClicked">
                <Button.Resources>
                    <Style TargetType="Border">
                        <Setter Property="CornerRadius" Value="15"/>
                    </Style>
                </Button.Resources>
            </Button>  
    </StackPanel>
     
    <Border BorderBrush="Black" BorderThickness="0.5" Margin="30,0,40,0" >
        <ScrollViewer Background="#313131">
            
            <Grid DockPanel.Dock="Top" Name="OAR_select"> 
                <Grid.ColumnDefinitions>
                    <ColumnDefinition/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>
            </Grid>
            
                
                
        </ScrollViewer>
    </Border> 
   
    
    </DockPanel>



</Window>

"""




class Contour_Smoother(RayWindow):
  def __init__(self, patient):
    self.LoadComponent(xaml)
    self.Topmost = True
    self.WindowStartupLocation = WindowStartupLocation.CenterScreen
    case = get_current("Case")
    examination = get_current('Examination')
    self.case = case
    self.patient = patient
# List
    self.contours = {}
    for rois in case.PatientModel.StructureSets[examination.Name].RoiGeometries:
        if rois.PrimaryShape != None:
          roiname = rois.OfRoi.Name
          self.add_row(roiname, patient)

  def CloseWindow(self, sender, event):
    self.DialogResult = False

  def SelectAllClicked(self, sender, event):
    for c in self.OAR_select.Children:
      if c.GetValue(Grid.ColumnProperty) == 0:
        c.IsChecked = True

  def DeselectAllClicked(self, sender, event):
    for c in self.OAR_select.Children:
      if c.GetValue(Grid.ColumnProperty) == 0:
        c.IsChecked = False

# This populates the list of OARs in the GUI
  def add_row(self, roiname, patient):
    row = self.OAR_select.RowDefinitions.Count
    case = self.case
    self.OAR_select.RowDefinitions.Add(RowDefinition())
    cb = CheckBox()
    cb.Margin = Thickness(10.,5.,5.,5.)
    cb.SetValue(Grid.RowProperty, row)
    cb.SetValue(Grid.ColumnProperty, 0)
    examination = get_current('Examination')
    text = roiname
    cb.Content = text
    cb.Foreground = Brushes.White;
    self.OAR_select.Children.Add(cb)
    self.contours[row] = roiname

  def StartAnalysis(self, sender, event):
    self.MsgPanel.Visibility = Visibility.Visible
    self.Opti()

  def Opti(self):
    patient = self.patient
    case = self.case
    examination = get_current('Examination')
    self.DialogResult = False
    oarlist = []
    for c in self.OAR_select.Children:
        if c.GetValue(Grid.ColumnProperty) == 0 and c.IsChecked:
          row = c.GetValue(Grid.RowProperty)
          roi = self.contours[row]
          if case.PatientModel.StructureSets[examination.Name].RoiGeometries[roi].HasContours() == False:
            continue
          oarlist.append(roi)
          
    cv =  self.contraction.SelectedItem
    
    contract_value = cv.Content
    
   # MessageBox.Show(contract_value)
   
    with CompositeAction(f'Contour Smoother Expansion = .3 / Contraction = {contract_value}'):
        for contours in oarlist:
            case.PatientModel.RegionsOfInterest[contours].CreateAlgebraGeometry(Examination=examination, Algorithm="Auto", ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [contours], 'MarginSettings': { 'Type': "Expand", 'Superior': 0.3, 'Inferior': 0.3, 'Anterior': 0.3, 'Posterior': 0.3, 'Right': 0.3, 'Left': 0.3 } }, ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } }, ResultOperation="None", ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })
            case.PatientModel.RegionsOfInterest[contours].CreateAlgebraGeometry(Examination=examination, Algorithm="Auto", ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [contours], 'MarginSettings': { 'Type': "Contract", 'Superior': contract_value, 'Inferior': contract_value, 'Anterior': contract_value, 'Posterior': contract_value, 'Right': contract_value, 'Left': contract_value } }, ExpressionB={ 'Operation': "Union", 'SourceRoiNames': [], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } }, ResultOperation="None", ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })

if __name__ == '__main__':
    patient = get_current('Patient')
    dialog = Contour_Smoother(patient)
    dialog.ShowDialog()







