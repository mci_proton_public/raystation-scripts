# README

## Purpose

The purpose of this script is to provide a graphical user interface (GUI) for smoothing contours in Raystation. The script expands the selected contours by 3mm and then contracts them by a user-specified amount.

## Workflow

1. **Run the Script**: Execute the script to open the GUI.
2. **Select Contours**: Use the checkboxes to select the contours you want to smooth.
3. **Choose Contraction Amount**: Select the desired contraction amount from the dropdown.
4. **Start Analysis**: Click the "Go" button to start the contour smoothing process.
