from win32com import client
import time
import os
import openpyxl
import PyPDF2 as pypdf
import sys
import warnings
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import traceback
import datetime
import numpy as np

warnings.filterwarnings("ignore")


def excel_2_pdf(fp,export_loc = r'\\ad\dfs\Shared Data\MCI Radiation Oncology\PROTON\Patient Treatment Plan Documents' ):
        #pythoncom.CoInitialize()
        
        wb = openpyxl.load_workbook(fp)
        ws = wb['PlanCheck']

        # Add logo to excel
        img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
        img.anchor = 'A1'
        ws.add_image(img)
        wb.save(fp)
        wb.close()

        excel=client.gencache.EnsureDispatch('Excel.Application')
        excel.Visible = False
        wb = excel.Workbooks.Open(fp)
        ws = wb.Worksheets['PlanCheck']
        
        run_time = datetime.datetime.today().strftime("%Y%m%d%H%M%S")
        filename = os.path.basename(fp)
        patient_name,mrn,planname,_ = filename.split('__')
        fol = os.path.join(export_loc,f"{patient_name}_{mrn}")
        if not os.path.exists(fol):
            os.mkdir(fol)
        fp_export = os.path.join(fol,f'{planname}_CHECK_{run_time}.pdf')
        ws.ExportAsFixedFormat(0,fp_export)# ,From=1,To=1)
        wb.Close()
        excel.Quit()
        time.sleep(3)
        os.remove(fp)

        



while True:
    try:
        search_fol = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\plan_check\temp'
        for file in os.listdir(search_fol):
            if file == 'pdf':
                pass
            elif file.split('.')[-1] == 'xlsx':
                excel_2_pdf(os.path.join(search_fol,file))#,export_loc=r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\plan_check\temp')
        time.sleep(5)
                
    except Exception:
        tb = traceback.format_exc().replace('\n',' --- ').replace(',',' ') + '\n'
        print(tb)
        '''
        with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\Errors\log_errors.txt','r') as f:
            last_line = f.readlines()[-1]
            if last_line.split(',')[2] != tb:
                with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\Errors\log_errors.txt','a') as f:
                   f.write(f"{datetime.datetime.today().strftime('%x %X')},{file},{tb}") 
        '''

# fp = r"//ad/dfs/Shared Data/MCI Proton/Final TPD/Reynaldo V Abad_pHN_20240520122640.xlsx"
# create_robustness_figure(fp)

