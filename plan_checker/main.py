# from utils import *
from ftplib import all_errors
from token import NUMBER
from connect import *
import openpyxl
from openpyxl.styles import Alignment, Font, PatternFill, Border, Side
from openpyxl.worksheet.pagebreak import Break
import re
import itertools
import os
import numpy as np
import datetime
import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox, MessageBoxButtons, MessageBoxIcon, Form, Label, TextBox, Button, DialogResult
clr.AddReference("System.Drawing")
from System.Drawing import Point, Size, Icon, ContentAlignment
import subprocess
import sys
from string import ascii_uppercase as Letters
import traceback
import ctypes


__version__ = '1.1.20'
'''
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])
try:
    import PIL

except:
    set_progress("Installing Missing Libraries",50)
    install('pillow')
    MessageBox.Show("Missing Libraries installed. Please re-run script")
    exit()
'''    





class InputDialog(Form):
    def __init__(self, title, prompt, width=500, height=200, padx = 20, pady=20):

        #set the sizes of each element (prompt, textbox, buttons)
        prompt_height = 2 * pady
        prompt_width = width - 2 * padx
        prompt_loc = Point(padx, pady)

        textbox_width = int(width / 2)
        textbox_height = 20
        textbox_loc = Point(int(width / 4), int(2 * pady + prompt_height))

        button_width = 120
        button_height = 30
        button_enter_loc = Point(2 * padx, int(2 * pady + prompt_height + textbox_height + pady))
        button_cancel_loc = Point(int(width - 2 * padx - button_width), int(2 * pady + prompt_height + textbox_height + pady))
        
        self.Icon = Icon(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\script_folder_do_not_edit\bap_icon.ico')
        self.Text = title
        self.Width = width
        self.Height = height
        self.StartPosition = 1  # Center screen

        self.label = Label()
        self.label.Text = prompt
        self.label.Location = prompt_loc
        self.label.Size = Size(prompt_width, prompt_height)
        self.label.TextAlign = ContentAlignment.MiddleCenter
        self.Controls.Add(self.label)

        self.textBox = TextBox()
        self.textBox.Location = textbox_loc
        self.textBox.Size = Size(textbox_width, textbox_height)
        self.Controls.Add(self.textBox)

        self.okButton = Button()
        self.okButton.Text = "Enter offset"
        self.okButton.Location = button_enter_loc
        self.okButton.Size = Size(button_width, button_height)
        self.okButton.Click += self.okButton_Click
        self.Controls.Add(self.okButton)

        self.cancelButton = Button()
        self.cancelButton.Text = "No offset"
        self.cancelButton.Location = button_cancel_loc
        self.cancelButton.Size = Size(button_width, button_height)
        self.cancelButton.Click += self.cancelButton_Click
        self.Controls.Add(self.cancelButton)

    def okButton_Click(self, sender, event):
        try:
            self.result = int(self.textBox.Text)
            if self.result<0:
                raise ValueError
            self.DialogResult = DialogResult.OK
            self.Close()
        except ValueError:
            MessageBox.Show("Please enter a valid integer >= 0.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error)

    def cancelButton_Click(self, sender, event):
        self.DialogResult = DialogResult.OK
        self.result = 0
        self.Close()

def show_input_dialog(title, prompt):
    dialog = InputDialog(title, prompt)
    result = dialog.ShowDialog()
    if result == DialogResult.OK:
        return dialog.result
    else:
        return show_input_dialog(title,prompt) 

def show_yes_no_message_box(title, text):
    result = ctypes.windll.user32.MessageBoxW(0, text, title, 36)  # 36 corresponds to Yes/No buttons with a question mark icon
    return result == 6  # Return True if the user clicked Yes, False if the user clicked No


pass_fill = PatternFill(patternType="solid", start_color="00FF00", end_color="00FF00")
fail_fill = PatternFill(patternType="solid", start_color="FF0000", end_color="FF0000")
warn_fill = PatternFill(patternType="solid", start_color="FFFF00", end_color="FFFF00")

center_align = Alignment(horizontal = 'center')
font_normal = Font(name="Century Gothic",size=11)
font_normal_bold = Font(name="Century Gothic",size=11,bold=True)
font_subtitle = Font(name = "Century Gothic", size = 14 , bold = True)
font_title = Font(name = "Century Gothic", size = 20 , bold = True)

# Define a thick border
thick_border_side = Side(style='thick')
normal_border_side = Side(style='medium')


    
def apply_normal_border_everywhere(ws, top_left_cell, bottom_right_cell):
    for row in ws[top_left_cell:bottom_right_cell]:
        for cell in row:
            cell.border = Border(top=normal_border_side, bottom=normal_border_side, left=normal_border_side, right=normal_border_side)
def apply_thick_border(ws,top_left_cell,top_right_cell):
    # Apply thick border to the outer edge of the range
    for cell in ws[top_left_cell:top_right_cell][0]:  # All cells
        cell.border = Border(top=thick_border_side, bottom=thick_border_side)
    ws[top_left_cell].border = Border(top=thick_border_side,bottom=thick_border_side,left = thick_border_side)
    ws[top_right_cell].border = Border(top=thick_border_side,bottom=thick_border_side,right = thick_border_side)
        

def get_DO_materials(case):
    # Get all materials that have been defined -> Density override materials
    return [x.Name for x in case.PatientModel.Materials]

def get_DO_roi_names(case):
    # Get all ROI's that have been assigned a DO material
    do_materials = get_DO_materials(case)
    do_rois = []
    for roi in case.PatientModel.RegionsOfInterest:
        try:
            material = roi.RoiMaterial.OfMaterial.Name
            if material in do_materials:
                do_rois.append(roi.Name)
        except:
            pass
    return do_rois

def return_DO_rois_not_on_current_scan(case,examination):
    # check that the DO structures are on current examination
    
    DO_rois = get_DO_roi_names(case)
    
    examination_ss = None
    for structureSet in case.PatientModel.StructureSets:
        if structureSet.OnExamination.Name == examination.Name:
            examination_ss = structureSet
            break
    for roi in examination_ss.RoiGeometries:
        roi_name = roi.OfRoi.Name
        if roi_name in DO_rois:
            DO_rois = [x for x in DO_rois if x!=roi_name]
    
    return DO_rois

def check_ExternalHD_contoured_on_current_scan():
    case = get_current("Case")
    examination = get_current("Examination")
    examination_ss = None
    for exam_ss in case.PatientModel.StructureSets:
        if exam_ss.OnExamination.Name == examination.Name:
            examination_ss = exam_ss
            break
    for roi in examination_ss.RoiGeometries:
        roi_name = roi.OfRoi.Name
        if re.search("External( |_)?HD",roi_name):
            return roi.HasContours()
    return False

# Get Patient info
def get_patient_info(ws,patient):
    # Insert Name
    name = patient.Name.split('^')
    ws['A8'] = name[0]+','+ " ".join(name[1:])

    # Insert DOB
    ws['B8'] = patient.DateOfBirth.ToShortDateString()
    
    # Insert MRN
    ws['C8'] = patient.PatientID
    
    for i in ['A','B','C']:
        ws[f'{i}8'].font = font_normal
        ws[f'{i}8'].alignment = Alignment(wrapText=True)
 
def report_info(ws):
    ws['F4'] = __version__
    ws['G4'] = datetime.datetime.today().strftime("%x")
    ws['H4'] = os.getlogin()
    for i in ['F','G','H']:
        ws[f'{i}4'].font = font_normal
    

###### CT Scan Checks
def CT_Scan_Checks(ws,case, examination):
    row = 14
    # Insert Examination Name
    ws[f'A{row}'] = examination.Name

    pixelsize = examination.Series[0].ImageStack.PixelSize
    if np.round(pixelsize['x'],3) == np.round(pixelsize['y'],3) == .098:
        ws[f'D{row}'] = str(.098)
        ws[f'D{row}'].fill = pass_fill
    else:
        ws[f'D{row}'] = str(pixelsize)
        ws[f'D{row}'].fill = fail_fill

   
    cal_curve = examination.EquipmentInfo.ImagingSystemReference['ImagingSystemName']
    protocol = examination.GetProtocolName()


    ws[f'C{row}'] = cal_curve
    ws[f'B{row}'] = protocol
    
    if protocol == "MCI_BRAIN_PROTON": 
        passing = cal_curve == 'CTAWP83399:HEAD'
    else:
        passing = cal_curve == 'CTAWP83399:PLVIS'

    #Fill out excel
    if passing:
        ws[f'C{row}'].fill = pass_fill
    else:
        ws[f'C{row}'].fill = fail_fill

    #pixel size of CT scan? Wasn't in the document so skip for now
    
    # check 800 FOV    
    # Study datetime has to match the current examination   
    all_800 = [x.Name for x in case.Examinations if re.search("800",x.Name) 
               and examination.GetAcquisitionDataFromDicom()['StudyModule']['StudyDateTime'] == x.GetAcquisitionDataFromDicom()['StudyModule']['StudyDateTime']]
    if len(all_800)>0:
        existence_800 = True
    else:
        existence_800 = False
    #Check 800
    ws[f'E{row}'] = existence_800
    ws[f'E{row}'].fill = pass_fill if existence_800 else fail_fill
    
    # Get pixel size
    if existence_800:
        pixelsize = case.Examinations[all_800[0]].Series[0].ImageStack.PixelSize
        if pixelsize['x'] == pixelsize['y'] == .15625:
            ws[f'F{row}'] = str(.15625)
            ws[f'F{row}'].fill = pass_fill
        else:
            ws[f'F{row}'] = str(pixelsize)
            ws[f'F{row}'].fill = fail_fill
    else:
        ws[f'F{row}'] = 'N/A'

    # Check External and Check_ExternalHD_on_current_scan
    examination_ss = case.PatientModel.StructureSets[examination.Name]
    external_existence = False
    external_on_current = False
    for roi in examination_ss.RoiGeometries:
        roi_name = roi.OfRoi.Name
        if re.search("External( |_)?HD",roi_name):
            external_existence = True
            external_on_current = roi.HasContours()
    
    
    ws[f'G{row}'] = external_existence
    ws[f'G{row}'].fill = pass_fill if external_existence else fail_fill

    ws[f'H{row}'] = external_on_current
    ws[f'H{row}'].fill = pass_fill if external_on_current else fail_fill

    for col in ['A','B','C','D','E','F','G','H']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
        
def check_dose_grid_resolution(row_start,ws,plan,examination):
    
    row = row_start
    num_fractions = len(plan.TreatmentCourse.TreatmentFractions)
    if num_fractions == 1:
        is_grid_plan = True;
    else:
        is_grid_plan = False;

    protocol = examination.GetProtocolName()
    brain_plan = examination.GetProtocolName() == "MCI_BRAIN_PROTON"
    brain_csi = examination.GetProtocolName() == "MCI_CSI_CERV_THORAX_PROTON"

    slice_thickness = examination.Series[0].ImageStack.SlicePositions[1] - examination.Series[0].ImageStack.SlicePositions[0] 

    resolution = .3 #cm
    if is_grid_plan:
        resolution = .1
    elif brain_plan:
        resolution = .2
    elif brain_csi:
        if slice_thickness == .15:
            resolution = .2


    plan_resolution = np.unique([x for x in plan.GetTotalDoseGrid().VoxelSize.values()])
    if len(plan_resolution)==1:
        plan_resolution =  plan_resolution[0]
        ws[f'D{row}'] = str(plan_resolution)

        if plan_resolution == resolution:
            ws[f'D{row}'].fill = pass_fill
        else:
            ws[f'D{row}'].fill = fail_fill
    else:
        ws[f'D{row}'] = str(plan.GetTotalDoseGrid().VoxelSize)
        ws[f'D{row}'].fill = fail_fill
    ws[f'D{row}'].font = font_normal



def get_valid_plan_names():
    # Based off 'Plan & Rx Naming' last modified 05/15/2024
    #Not including Bst since this is checked elsewhere
    # Added Liver 09/25/2024 per email with Zach/Kristen
    # Changing breast to BR per Zach 01/24/2025
    valid_plan_names = [
        'Liver',
        'Abd',
        'Anal',
        'Br_L_PRON',
        'Br_R_PRON',
        'Br_L',
        'Br_R',
        'APBI_L',
        'APBI_R',
        'Br+N_L',
        'Br+N_R',
        'BiBr',
        'BiCW',
        'BOS',
        'Brain',
        'CSI',
        'CW_L',
        'CW_R',
        'CW+N_L',
        'CW+N_R',
        'Eso',
        'ArmL',
        'ArmR',
        'LegL',
        'LegR',
        'GYN',
        'HN',
        'HN_L',
        'HN_R',
        'Lung_L',
        'Lung_R',
        'MDSTN',
        'PA+N',
        'Pelvis',
        'P+SV',
        'P+SVpx',
        'P+SVps',
        'P+SV+N',
        'PBed+N',
        'PBed',
        'P',
        'PBed_Mid',
        'Rectum',
        'Scalp',
        'CSPN',
        'TSPN',
        'LSPN',
    ]

    # Explicitly detail the possible spine names

    cervical = ['C'+str(i) for i in range(1,8)]
    thoracic = ['T'+str(i) for i in range(1,13)]
    lumbar = ['L'+str(i) for i in range(1,6)]
    sacrum = ['S'+str(i) for i in range(1,6)]
    all_spine = cervical+thoracic+lumbar+sacrum
    all_spine_comb = []
    for a,b in itertools.combinations(all_spine,2):
        if a[0]==b[0]:
            all_spine_comb.append(a+'-'+b[1:])
        else:
            all_spine_comb.append(a+'-'+b)
    valid_plan_names += all_spine_comb

    # Add Grid Combo

    grid_combo = ['Grid_'+x for x in valid_plan_names]
    valid_plan_names += grid_combo
    
    return valid_plan_names


def check_plan_name(ws,plan):
    """ Check plan name in valid plan names.  """
    
    valid_plan_names = get_valid_plan_names()


    name = plan.Name
    ws['B10'] = name
    ws['B10'].font = font_normal
    

    pattern =  r'^(?P<plan_name>.+?)(_Bst(?P<boost>\d+?))?(\^G(?P<gantry>\d+?))(R(?P<replan>\d+))?(_TD)?$'
    match = re.match(pattern, name)

    if match:
        match = match.groupdict()
        plan_name = match['plan_name']
        if plan_name in valid_plan_names:
            ws['B10'].fill = pass_fill
        else:
            ws['B10'].fill = fail_fill
    else:
        ws['B10'].fill = fail_fill
            
def check_beamset_name(row_start,ws,beam_set):
    """ Check plan name in valid plan names. check Gantry against the dicom export properties, which should be generated already. If time permits, check if there are other replans in the same case and ensure that they're in increasing order. """
    
    row = row_start
    valid_plan_names = get_valid_plan_names()


    name = beam_set.BeamSetIdentifier().split(':')[1]
    ws[f'A{row}'] = name

    pattern =  r'^(?P<plan_name>.+?)(_Bst(?P<boost>\d+?))?(\^G(?P<gantry>\d+?))(R(?P<replan>\d+))?(_TD)?$'
    match = re.match(pattern, name)

    planned_gtr = beam_set.DicomExportProperties.ExportedTreatmentMachineName
    ws[f'C{row}'] = planned_gtr if planned_gtr != '' else 'V2023B_May2024' #'V12A_Nov2022'

    if match:
        match = match.groupdict()
        plan_name = match['plan_name']
        ws[f'B{row}'] = plan_name
        if plan_name in valid_plan_names:
            ws[f'B{row}'].fill = pass_fill
        else:
            ws[f'B{row}'].fill = fail_fill

        # Check gantry name matches dicom export properties
        gtr = 'GTR'+str(match['gantry'])
        
        if gtr == planned_gtr:
            ws[f'C{row}'].fill = pass_fill
        else:
            ws[f'C{row}'].fill = fail_fill
    else:
        ws[f'B{row}'] = "Invalid name"
        ws[f'C{row}'] = "Invalid name"
        ws[f'B{row}'].fill = fail_fill
        ws[f'C{row}'].fill = fail_fill
    for col in ['A','B','C']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText = True)

    
def check_dose_algorithm(row,ws,plan,beam_set):
    # result = beam_set.AccurateDoseAlgorithm.DoseAlgorithm
    try:
        optimization_dose_algorithm = list(plan.PlanOptimizations)[-1].OptimizationParameters.DoseCalculation.OptimizationDoseAlgorithm.DoseAlgorithm
    except:
        optimization_dose_algorithm = "No Optimization"
    try:
        final_doses = [x.DoseValues.AlgorithmProperties.DoseAlgorithm for x in beam_set.FractionDose.BeamDoses]
    except:
        final_doses = ['No Final Dose' for _ in beam_set.Beams]
    result = bool(np.all(['MonteCarlo' in x for x in final_doses + [optimization_dose_algorithm]]))
    ws[f'E{row}'] = result
    if result == True:
        ws[f'E{row}'].fill = pass_fill
    else:
        ws[f'E{row}'].fill = fail_fill
    ws[f'E{row}'].font = font_normal

def check_any_beam_has_range_shifter(row_start,ws,beam_set):
    ''' Check if any beam has a RS '''
    row = row_start
    #Current commisioned RS
    RS_NoSnout_7p5_cm_id = "dde33278-7e02-49b3-a539-213b2d209d90"
    num_RS = 0
    for beam in beam_set.Beams:
        rs_id = beam.RangeShifterId
        if rs_id:
            ws[f'F{row}'] = "True"
            num_RS += 1
            
    if num_RS == 0 :
        ws[f'F{row}'] = "False"
        ws[f'G{row}'] = "N/A"
    else:
        all_same_RS = np.all([x.RangeShifterId == RS_NoSnout_7p5_cm_id for x in beam_set.Beams])
        if all_same_RS:
            ws[f'G{row}'] = "True"
            ws[f'G{row}'].fill = pass_fill
        else:
            ws[f'G{row}'] = "False"
            ws[f'G{row}'].fill = fail_fill
    for col in ['F','G']:
        ws[f"{col}{row}"].font = font_normal


def check_min_max_MU(row_start,ws,plan,beam_set):
    # Write title and column names for beam checks
    
    row = row_start
    ws[f'A{row}']= "Beam Checks"
    ws[f'A{row}'].font = font_subtitle
    
    row += 1
    for col,title in zip(['A','B','C','D','E','F','G','H','I','J'],['Beam Name', 'MU per Beam', 'Min MU (?0.015 MU)', 'Max MU (?2 MU, 50 for GRID))', 'Name Matches Angle', 'Name Matches Standard', 'Character Length Check (<=16)', 'Air Gap Appropriate', 'Snout Position','Description Matches Standard']):
        ws[f'{col}{row}'] = title
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText = True)
    apply_thick_border(ws,f'A{row}',f'J{row}')
    

    row_start+=2
    
    #if number of fractions is 1, then it's grid
    #plan = get_current("Plan")
    #beam_set = get_current("BeamSet")
    num_fractions = len(plan.TreatmentCourse.TreatmentFractions)
    if num_fractions == 1:
        is_grid_plan = True;
    else:
        is_grid_plan = False;

    #Set limits
    min_allowed_MU,max_allowed_MU = .015,2
    if is_grid_plan:
        max_allowed_MU = 50
    
    #get min,max of each beam in beamset
    # row_start = 21
    for idx,beam in enumerate(beam_set.Beams):   #Iterate through each beam
        row += 1
        beam_min_mu = 1e8 # initialize
        beam_max_mu = -1 # initialize
        bm_wt = beam.BeamMU   
        for layer in beam.Segments:   #Iterate through each energy layer
            beam_energy = layer.NominalEnergy
            wts = layer.Spots.Weights * bm_wt   #Get the spot weights in MU/fx
            min_mu = np.min(wts)
            max_mu = np.max(wts)

            if min_mu < beam_min_mu:
                beam_min_mu = min_mu
            if max_mu > beam_max_mu:
                beam_max_mu = max_mu
                
        # Add to excel
        # Use ordinal value of columns and convert to character. First beam is 'B34'
        #letter = chr(66+idx)
        
        ws[f'A{row}'] = beam.Name
        # Set the width of column
      
        
        if bm_wt!=0:
            # beam MU
            ws[f'B{row}'] = str(np.round(beam.BeamMU,2))
        
            # check min MU
            ws[f'C{row}'] = f"{beam_min_mu:.3f}"
            if beam_min_mu >= min_allowed_MU:
                ws[f'C{row}'].fill = pass_fill
            else:
                ws[f'C{row}'].fill = fail_fill

            # check max MU
            ws[f'D{row}'] = f"{beam_max_mu:.1f}"
            if beam_max_mu <= max_allowed_MU:
                ws[f'D{row}'].fill = pass_fill
            else:
                ws[f'D{row}'].fill = fail_fill
        else:
            #No final dose on plan
            ws[f'B{row}'] = "No MU"
            ws[f'B{row}'].fill = fail_fill
            ws[f'C{row}'] = "No MU"
            ws[f'C{row}'].fill = fail_fill
            ws[f'D{row}'] = "No MU"
            ws[f'D{row}'].fill = fail_fill
        for col in ['A','B','C','D']:
            ws[f"{col}{row}"].font = font_normal
    return row_start

def get_standard_beam_names(plan,beam_set,offset_beam_num):
    standard_beam_names = []
     # Lets set the start point for the beam numbering
    def find_offset(plan, beamset_name):
        # Use the index of the plan for the offset
        offset = 0
        for bs in plan.BeamSets:
            bs_name = bs.BeamSetIdentifier().split(":")[1]
            if bs_name == beamset_name:
                return offset
            else:
                offset += len(bs.Beams)
                

        '''
        if len(plan.BeamSets) == 1:
            return 0
        all_beamsets = []
        for bs in plan.BeamSets:
            bs_name = bs.BeamSetIdentifier().split(":")[1]
            num_beams = len(bs.Beams)
            isocenter_names = [beam_i.Isocenter.Annotation.Name for beam_i in bs.Beams]
            isocenter_num = np.min([int(re.search(r"P(\d)?", x).group().split('P')[1]) for x in isocenter_names])
            all_beamsets.append({'name': bs_name, 'priority': isocenter_num, 'num_beams': num_beams})
        all_beamsets = sorted(all_beamsets, key=lambda x: x['priority'])

        offset = 0
        for bs in all_beamsets:
            if bs['name'] != beamset_name:
                offset += bs['num_beams']
            else:
                return offset
        '''    

    plan_name = beam_set.BeamSetIdentifier().split(":")[1]  # Really the beamset name

    # Let's seperate the plan name
    replan = None

    #before_caret, after_caret = plan_name.split("^")

    replan = re.search(r"R[1-9]+", plan_name)#after_caret)
    if replan:
        replan = replan.group().split("R")[1]

    save_tx_angles = []
    beam_directions = []
    num_beams = beam_set.Beams.Count

    # Let's check if the plan is repainting.
    Repaint = ""
    min_spot_weight = plan.PlanOptimizations[0].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimits.x
    if min_spot_weight == 0.03 and num_beams % 2 == 0:
        if num_beams >= 6:
            Repaint = "_RP"
        else:
            Repaint = MessageBox.Show("Is this plan a repainting plan?", "Repainting?", 4)
            if Repaint == 6:
                Repaint = "_RP"
            else:
                Repaint = ""

    patient_position = beam_set.PatientPosition
    if patient_position == "HeadFirstSupine":
        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # AP
                direction = "AP"
            elif tx_g_angle == 180:  # PA
                direction = "PA"
            elif tx_g_angle == 90:  # LL
                direction = "LL"
            elif tx_g_angle == 270:  # RL
                direction = "RL"
            elif 0 < tx_g_angle < 90:  # LAO
                direction = "LAO"
            elif 90 < tx_g_angle < 180:  # LPO
                direction = "LPO"
            elif 270 < tx_g_angle < 360:  # RAO
                direction = "RAO"
            elif 180 < tx_g_angle < 270:  # RPO unless 180.1 ---> PA
                if tx_g_angle == 180.1:
                    direction = "PA"
                else:
                    direction = "RPO"

            beam_directions.append(direction)

    elif patient_position == "FeetFirstSupine":

        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # AP
                direction = "AP"
            elif tx_g_angle == 180:  # PA
                direction = "PA"
            elif tx_g_angle == 90:  # RL
                direction = "RL"
            elif tx_g_angle == 270:  # LL
                direction = "LL"
            elif 0 < tx_g_angle < 90:  # RAO
                direction = "RAO"
            elif 90 < tx_g_angle < 180:  # RPO
                direction = "RPO"
            elif 270 < tx_g_angle < 360:  # LAO
                direction = "LAO"
            elif 180 < tx_g_angle < 270:  # LPO unless 180.1 ---> PA
                if tx_g_angle == 180.1:
                    direction = "PA"
                else:
                    direction = "LPO"

            beam_directions.append(direction)

    elif patient_position == "HeadFirstProne":

        for b, beams in enumerate(beam_set.Beams):
            tx_g_angle = beam_set.Beams[b].GantryAngle
            if tx_g_angle == 180.1:
                save_tx_angles.append(float(tx_g_angle))
            else:
                save_tx_angles.append(int(tx_g_angle))

            if tx_g_angle == 0 or tx_g_angle == 360:  # PA
                direction = "PA"
            elif tx_g_angle == 180:  # AP
                direction = "AP"
            elif tx_g_angle == 90:  # RL
                direction = "RL"
            elif tx_g_angle == 270:  # LL
                direction = "LL"
            elif 0 < tx_g_angle < 90:  # RPO
                direction = "RPO"

            elif 90 < tx_g_angle < 180:  # RAO
                direction = "RAO"
            elif 270 < tx_g_angle < 360:  # LPO
                direction = "LPO"
            elif 180 < tx_g_angle < 270:  # LAO unless 180.1 ---> AP
                if tx_g_angle == 180.1:
                    direction = "AP"
                else:
                    direction = "LAO"

            beam_directions.append(direction)

    #try:
    offset_beam_num += find_offset(plan, plan_name)  # plan_name was created at the top (really beamset name)
    '''
    except:
        MessageBox.Show("Check isocenter names in plan. Unable to get correct offset for beam numbering if isocenter does not meet standard\n\nIn general, the standard iso name is pISO_P1 for the nominal plan and pISO_P# for the additional phases.")
        exit()
    '''
    for i in range(num_beams):

        couch = int(beam_set.Beams[i].CouchRotationAngle)

        if replan:
            if couch != 0:
                string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + "G" + str(save_tx_angles[i]) + "_T" + str(couch)  # save_tx_angles[s]

            else:
                if Repaint=="":
                    string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + beam_directions[i] + "_G" + str(save_tx_angles[i])  # save_tx_angles[s]
                else:
                    string = f"p{i+1+offset_beam_num:02d}" + f"{Letters[int(replan)]}_" + "G" + str(save_tx_angles[i])  # save_tx_angles[s]
        else:

            if couch != 0:
                string = f"p{i+1+offset_beam_num:02d}" + "_" + "G" + str(save_tx_angles[i]) + "_T" + str(couch)  # save_tx_angles[s]

            else:
                if Repaint=="":
                    string = f"p{i+1+offset_beam_num:02d}" + "_" + beam_directions[i] + "_G" + str(save_tx_angles[i])  # save_tx_angles[s]
                else:
                    string = f"p{i+1+offset_beam_num:02d}" + "_" + "G" + str(save_tx_angles[i])  # save_tx_angles[s]

        # Add repaint suffix if needed. default  = ''

        string += Repaint
        expected_name = string
        standard_beam_names.append(expected_name)
    
    return standard_beam_names

def check_beam_names(row_start,ws,plan,beam_set,offset_beam_num):
    num_beams = beam_set.Beams.Count
    standard = get_standard_beam_names(plan,beam_set,offset_beam_num)
    for i in range(num_beams):
        row = row_start + i
        beam_name = beam_set.Beams[i].Name
        
        expected_name = standard[i]

        # Make checks
        
        beam_name_angle = re.search(r"G(\d+(?:\.\d+)?)", beam_name).group(1)
        beam_angle = beam_set.Beams[i].GantryAngle
        ws[f"E{row}"] = str(beam_angle)
        # Angle check
        if float(beam_name_angle) == beam_angle:
            ws[f"E{row}"].fill = pass_fill
        else:
            ws[f"E{row}"].fill = fail_fill
        # Beam Name check
        ws[f'F{row}'] = expected_name    
        if beam_name == expected_name:
            ws[f'F{row}'].fill = pass_fill
        else:
            ws[f'F{row}'].fill = fail_fill
        # beam name length check
        beam_name_length = len(beam_name)
        ws[f'G{row}'] = str(beam_name_length)
        if beam_name_length<=16:
            ws[f'G{row}'].fill = pass_fill
        else:
            ws[f'G{row}'].fill = fail_fill
        
        #beam description check
        beam_description = beam_set.Beams[i].Description
        if beam_description == None or beam_description == '':
            beam_description = 'N/A'
        ws[f'J{row}'] = beam_description
        ws[f'J{row}'].font = font_normal
        ws[f'J{row}'].alignment = Alignment(wrapText = True)

        if beam_description == expected_name:
            ws[f'J{row}'].fill = pass_fill
        else:
            ws[f'J{row}'].fill = fail_fill
        
        for col in ['E','F','G','J']:
            ws[f"{col}{row}"].font = font_normal
            



def check_RS_min_airGap(row_start,ws,patient,plan,beam_set, case, examination, min_airGap_accepted = 7.0,preplan=False,use_externalHD = False):
    ''' check that all beams have a minimum airGap greater than 7.0 or other if specified '''
    

    #get the row that corresponds to this beamset for the beamset checks
    bs_names = [bs.BeamSetIdentifier().split(':')[1] for bs in plan.BeamSets]
    row_bs = 18 + bs_names.index(beam_set.BeamSetIdentifier().split(':')[1])
    # row_start = 21
    switch_to_externalHD = None
    
    #Get existence from worksheet
    externalHD_existence = ws['G14'].value
    externalHD_on_current_scan = ws['H14'].value
    if use_externalHD:
        if externalHD_on_current_scan:
            
            switch_to_externalHD = True
            external = None
            externalHD = None
            for roi in case.PatientModel.StructureSets[examination.Name].RoiGeometries:
                if roi.OfRoi.Type == 'External':
                    external = roi
                if re.search("External( |_)?HD",roi.OfRoi.Name):
                    externalHD = roi
                    try:
                        externalHD.OfRoi.SetAsExternal()
                        ws[f'H{row_bs}'] = externalHD.OfRoi.Name
                    except:
                        switch_to_externalHD = False
                        break

        
    if not switch_to_externalHD:
        external = None
        for roi in case.PatientModel.StructureSets[examination.Name].RoiGeometries:
            if roi.OfRoi.Type == 'External':
                external = roi
                break
        ws[f'H{row_bs}'] = external.OfRoi.Name
        
    ws[f'H{row_bs}'].font = font_normal
    ws[f'H{row_bs}'].alignment = Alignment(wrapText=True)
    

    for idx,beam in enumerate(beam_set.Beams):
        
        row = row_start + idx
        air_gap = np.round(beam.GetMinAirGap(),2)
        ws[f'H{row}'] = str(air_gap)
        if air_gap < min_airGap_accepted:
            ws[f'H{row}'].fill = fail_fill
        else:
            ws[f'H{row}'].fill = pass_fill
        ws[f'H{row}'].font = font_normal

    #Set external back if it was changed
    if switch_to_externalHD:
        external.OfRoi.SetAsExternal()
        if not preplan:
            beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo",ForceRecompute=False,RunEntryValidation=True)
            # Compute radiation set scenario groups
            for rssg in case.TreatmentDelivery.RadiationSetScenarioGroups:
                rssg.ComputeScenarioGroupDoseValues()
            case.TreatmentDelivery.RadiationSetScenarioGroups[1].ComputeScenarioGroupDoseValues()
        patient.Save()


def check_snout_position(row_start,ws,beam_set, default_snout_position = 42.2):
    ''' Check that all beams have the snout at default position '''
    # row_start = 21
    for idx,beam in enumerate(beam_set.Beams):
        row = row_start + idx
        snout = np.round(beam.SnoutPosition,2)
        ws[f"I{row}"] = str(snout)
        ws[f"I{row}"].font = font_normal

        if not beam.RangeShifterId:
            if snout != default_snout_position:
                ws[f"I{row}"].fill = fail_fill
            else:
                ws[f"I{row}"].fill = pass_fill
    return row_start + len(beam_set.Beams) + 1


def check_setup_beams(row_start,ws,beam_set,plan,offset_beam_num):
    planned_gtr = beam_set.DicomExportProperties.ExportedTreatmentMachineName
    
    # row_start = 21 + len(beam_set.Beams)
    
    ws[f'A{row_start}'] = "Setup Beam Checks"
    ws[f'A{row_start}'].font = font_subtitle
    for col,data in zip(['A','B','C','D','E','F','G','H'],['Setup Beam Name','Name Matches Angle','Name Matches Standard','Angle Matches Standard','Imaging System','Only 1 Rad-A','Gantry matches DICOM export','Description Matches Standard']):
        ws[f'{col}{row_start+1}'] = data
        ws[f'{col}{row_start+1}'].font = font_normal
        ws[f'{col}{row_start+1}'].alignment = Alignment(wrapText=True)
    
    apply_thick_border(ws,f'A{row_start+1}',f'H{row_start+1}')
    row_start = row_start +2
    
        
    plan_name = str(plan.Name)
    setup_beams = beam_set.PatientSetup.SetupBeams

    # Verify Setup beams match standard. Below I've copied the initial part of the setup beam script that assigns the orthogonal setup beams
    setup_g_angles = []

    patient_position = beam_set.PatientPosition

    #determine which orthogonal setup fields are needed
    first_g_angle = beam_set.Beams[0].GantryAngle

    if patient_position == "HeadFirstSupine":

        if 180 >= first_g_angle > 90: #start with a PA/LL setup
            setup_g_angles = [180,90]
            name_0 = "PA_Setup"
            name_1 = "LL_Setup"
        if 90 >= first_g_angle > 0: #start with an LL/AP setup
            setup_g_angles = [90,0]
            name_0 = "LL_Setup"
            name_1 = "AP_Setup"
        if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/RL setup
            setup_g_angles = [0,270]
            name_0 = "AP_Setup"
            name_1 = "RL_Setup"
        if 270 >= first_g_angle > 180: #start with RL/PA setup
            setup_g_angles = [270,180]
            name_0 = "RL_Setup"
            name_1 = "PA_Setup"

    elif patient_position == "FeetFirstSupine":

        if 180 >= first_g_angle > 90: #start with a PA/RL setup
            setup_g_angles = [180,90]
            name_0 = "PA_Setup"
            name_1 = "RL_Setup"
        if 90 >= first_g_angle > 0: #start with an RL/AP setup
            setup_g_angles = [90,0]
            name_0 = "RL_Setup"
            name_1 = "AP_Setup"
        if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/LL setup
            setup_g_angles = [0,270]
            name_0 = "AP_Setup"
            name_1 = "LL_Setup"
        if 270 >= first_g_angle > 180: #start with LL/PA setup
            setup_g_angles = [270,180]
            name_0 = "LL_Setup"
            name_1 = "PA_Setup"
       
    elif patient_position == "HeadFirstProne":

        if 180 >= first_g_angle > 90: #start with a PA/RL setup
            setup_g_angles = [180,90]
            name_0 = "AP_Setup"
            name_1 = "RL_Setup"
        if 90 >= first_g_angle > 0: #start with an RL/AP setup
            setup_g_angles = [90,0]
            name_0 = "RL_Setup"
            name_1 = "PA_Setup"
        if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/LL setup
            setup_g_angles = [0,270]
            name_0 = "PA_Setup"
            name_1 = "LL_Setup"
        if 270 >= first_g_angle > 180: #start with LL/PA setup
            setup_g_angles = [270,180]
            name_0 = "LL_Setup"
            name_1 = "AP_Setup"    
       
    expected_orthogonal_setup = {
    "HeadFirstSupine":{"AP_Setup":0,"PA_Setup":180,"LL_Setup":90,"RL_Setup":270},
     "FeetFirstSupine":{"AP_Setup":0,"PA_Setup":180,"LL_Setup":270,"RL_Setup":90},
     "HeadFirstProne":{"AP_Setup":180,"PA_Setup":0,"LL_Setup":270,"RL_Setup":90}
     }   
       
    name_list = ['CW','Br_L','Br_R','Br+N_L','Br+N_R']   
    if any(n in plan_name for n in name_list): #if plan is breast or chestwall, use these setup orthogonals  
        setup_g_angles = [0,270]        
        name_0 = "AP_Setup"
        name_1 = "RL_Setup"

    if 'HN_R' in plan_name:  # if plan is HN_R, use these setup orthogonals
        setup_g_angles = [270, 180]
        name_0 = "RL_Setup"
        name_1 = "PA_Setup"
    elif 'HN_L' in plan_name or 'HN' in plan_name:  # if plan is HN_L or HN, use these setup orthogonals
        setup_g_angles = [90, 0]
        name_0 = "LL_Setup"
        name_1 = "AP_Setup"


    #Get the other expected angles
    for b, beams in enumerate(beam_set.Beams):
        tx_g_angle = beam_set.Beams[b].GantryAngle #record the gantry angle of each beam
        if tx_g_angle <=89:
            setup_g_angle = tx_g_angle+360-90
        else:    
            setup_g_angle = tx_g_angle-90
        setup_g_angles.append(setup_g_angle) #save a list of the setup gantry angles needed

    #Get expected names
    standard_names = get_standard_beam_names(plan,beam_set,offset_beam_num)    
    expected_setup_names = [name_0,name_1] + [x + "_B" for x in standard_names]
    
    #First lets check that there is only one radA
    pattern = r"(?P<gtr>GTR[123])-rad-(?P<imager>A|B).*"
    total_radA = 0
    for sb in setup_beams:
        imager = re.match(pattern,sb.SetupImagingSystemName).group("imager")
        if imager == 'A':
            total_radA += 1

    # Let's break it up by beam
    # check first setup beam name matches angle
    # for the orthogonal setup beams, given the orientation of the patient (HFS, FFS, etc) we verify that name of the beam matches the angle
    for i,sb in enumerate(setup_beams):
        row = row_start + i
        

        #Check that the setup beam name matches angle
        try:
            if i>1:
                name_angle = float(re.match(".*_G(?P<angle>\d+(\.\d*)?)(_T\d+)?(_RP)?_B",setup_beams[i].Name).group("angle"))-90
                if name_angle <0:
                    name_angle+=360
            else:
                #we check based on the name in the expexted ortho angles
                name_angle = expected_orthogonal_setup[patient_position][setup_beams[i].Name]
        except:
            name_angle = -1


        # Fill Excel
        ws[f'A{row}'] = sb.Name


        # Name matches angle
        ws[f'B{row}'] = str(sb.GantryAngle)
        if name_angle == sb.GantryAngle:
            ws[f'B{row}'].fill = pass_fill
        else:
            ws[f'B{row}'].fill = fail_fill

        # Name matches Standard
        ws[f'C{row}'] = expected_setup_names[i]    
        if sb.Name == expected_setup_names[i]:
            ws[f'C{row}'].fill = pass_fill
        else:
            ws[f'C{row}'].fill = fail_fill

        # Angle matches standard
        ws[f'D{row}'] = str(setup_g_angles[i])   
        if sb.GantryAngle == setup_g_angles[i]:
            ws[f'D{row}'].fill = pass_fill
        else:
            ws[f'D{row}'].fill = fail_fill
        
        #Imager
        sb_imager = sb.SetupImagingSystemName    
        match =  re.match(pattern,sb_imager)
        if match:
            imager = match.group("imager")
            gtr = match.group("gtr")
        else:
            gtr = "Error"
        
        ws[f'E{row}'] = sb_imager
        ws[f'F{row}'] = imager
        ws[f'G{row}'] = gtr
        
        if total_radA == 1:
            ws[f'F{row}'].fill = pass_fill
        else:
            ws[f'F{row}'].fill = fail_fill
        
        if gtr == planned_gtr:
            ws[f'G{row}'].fill = pass_fill
        else:
            ws[f'G{row}'].fill = fail_fill

        #beam description check
        setup_beam_description = sb.Description
        if setup_beam_description == None or setup_beam_description == '':
            setup_beam_description = 'BLANK'
        ws[f'H{row}'] = setup_beam_description
        ws[f'H{row}'].font = font_normal
        ws[f'H{row}'].alignment = Alignment(wrapText = True)

        if setup_beam_description == expected_setup_names[i]:
            ws[f'H{row}'].fill = pass_fill
        else:
            ws[f'H{row}'].fill = fail_fill

        for col in ['A','B','C','D','E','F','G','H']:
            ws[f'{col}{row}'].font = font_normal
            ws[f'{col}{row}'].alignment = Alignment(wrapText=True)

    return row+2 #This is the row we left off at

def get_robust_optimization_parameters(plan):
    d_position = {'Anterior':None, 'Inferior': None, 'Left':None, 'Right':None, 'Posterior':None, 'Superior':None}
    if plan.PlanOptimizations[0].OptimizationParameters.RobustnessParameters == None:
        return "No Robustness Parameters present in optimization"

    for pos in d_position.keys():
        d_position[pos] = getattr(plan.PlanOptimizations[0].OptimizationParameters.RobustnessParameters.PositionUncertaintyParameters,f"PositionUncertainty{pos}")
                                  
    d_range = np.round(plan.PlanOptimizations[0].OptimizationParameters.RobustnessParameters.DensityUncertaintyParameters.DensityUncertainty * 100,1)

    return {"Position":d_position,"Range":d_range}

def get_robust_optimization_standard_parameters(beam_set):
    name = beam_set.BeamSetIdentifier().split(':')[1]
    pattern =  r'^(?P<plan_name>.+?)(_Bst(?P<boost>\d+?))?(\^G(?P<gantry>\d+?))(R(?P<replan>\d+))?(_TD)?$'
    match = re.match(pattern, name)
    if match:
        plan_name = match['plan_name']
        if plan_name in ['Lung_L','Lung_R','Lung']:
            return {"Position":.5,"Range":5} #cm , %
        elif plan_name in [
                            'P+SV',
                            'P+SVpx',
                            'P+SVps',
                            'P+SV+N',
                            'PBed+N',
                            'PBed',
                            'PBed_Mid']:
            return {"Position":.4,"Range":3.5}
        elif plan_name in [
                            'Brain',
                            'BOS',
                            'HN',
                            'HN_L',
                            'HN_R', 
                            ]:
            return {"Position":.3,"Range":3.5}
        else:
            valid_names = get_valid_plan_names()
            if plan_name in valid_names:
                return {"Position":.5,"Range":3.5}
            else:
                return "Plan name isn't in standard set and optimization parameters cannot be checked against standard. Please edit name to match standard."
    else:
        return "Plan name isn't matching standard and as such plan name cannot be extracted to check against standard values of optimization parameters. Please edit name to match standard."
        
def check_robust_optimization(ws,plan,beam_set,row_start):
    row = row_start
    ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=3)
    ws[f'A{row}'] = "Robustness Optimization Parameters"
    ws[f'A{row}'].font = font_subtitle
    
    standard_par = get_robust_optimization_standard_parameters(beam_set)
    row +=1
    if type(standard_par) == str:
        # We have an error
        ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=6)
        ws[f'A{row}'] = standard_par
        ws[f'A{row}'].font = font_normal
        ws[f'A{row}'].fill = fail_fill
 
    else:
        ws[f'A{row}'] = "Standard Setup Uncertainty [cm]"
        ws[f'B{row}'] = "Plan Setup Uncertainty [cm]"
        ws[f'C{row}'] = "Standard Range Uncertainty [%]"
        ws[f'D{row}'] = "Plan Range Uncertainty [%]"
        
        for col in ['A','B','C','D']:
            ws[f'{col}{row}'].font = font_normal
            ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
        
        apply_thick_border(ws,f'A{row}',f'D{row}')
        row += 1

        standard_pos = standard_par['Position']
        standard_range = standard_par['Range']
        
        ws[f'A{row}'] = str(standard_pos)
        ws[f'C{row}'] = str(standard_range)
        
        plan_par = get_robust_optimization_parameters(plan)
        if type(plan_par)!=str:
            plan_par_pos = np.unique(list(plan_par['Position'].values()))
            plan_par_range = plan_par['Range']
            if len(plan_par_pos) == 1:
                ws[f'B{row}'] = str(plan_par_pos[0])
                if plan_par_pos[0] == standard_pos:
                    ws[f'B{row}'].fill = pass_fill
                else:
                    ws[f'B{row}'].fill = fail_fill
            else:
                ws[f'B{row}'] = f"Non-isotropic uncertainty {plan_par}"
                ws[f'B{row}'].fill = fail_fill
            
            ws[f'D{row}'] = str(plan_par_range)
            if plan_par_range == standard_range:
                ws[f'D{row}'].fill = pass_fill
            else:
                ws[f'D{row}'].fill = fail_fill
        else:
            ws[f'B{row}'] = plan_par
            ws[f'B{row}'].fill = fail_fill

            ws[f'D{row}'] = plan_par
            ws[f'D{row}'].fill = fail_fill
        
        for col in ['A','B','C','D']:
            ws[f'{col}{row}'].font = font_normal
            ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
        
    return row + 2


def get_robust_evaluation_standard_parameters(beam_set):
    valid_names = get_valid_plan_names()
    name = beam_set.BeamSetIdentifier().split(':')[1]
    pattern =  r'^(?P<plan_name>.+?)(_Bst(?P<boost>\d+?))?(\^G(?P<gantry>\d+?))(R(?P<replan>\d+))?(_TD)?$'
    match = re.match(pattern, name)
    if match:
        plan_name = match['plan_name']
        if plan_name in valid_names:
            if plan_name in ['Lung_L','Lung_R','Lung']:
                return {"Position":[.5],"Range":5} #cm , %
            elif plan_name in [
                                'P+SV',
                                'P+SVpx',
                                'P+SVps',
                                'P+SV+N',
                                'PBed+N',
                                'PBed',
                                'PBed_Mid']:
                return {"Position":[.4],"Range":3.5}
            elif plan_name in [
                                'Brain',
                                'BOS',
                                'HN',
                                'HN_L',
                                'HN_R', 
                                ]:
                return {"Position":[.3],"Range":3.5}
            elif plan_name in ['CSI']:
                return {"Position":[.3,.5], "Range":3.5}
            else:
                return {"Position":[.5],"Range":3.5}
        else:
            return "Plan name isn't in standard set and evaluation parameters cannot be checked against standard. Please manually check."
    else:
        return "Plan name isn't matching standard and as such plan name cannot be extracted to check against standard values of evaluation parameters. Please manually check"
        
def get_robust_evaluation_standard_list(beam_set):
    standard = get_robust_evaluation_standard_parameters(beam_set)
    if type(standard)==str:
        return standard
    else:
        d_range = standard['Range']
        all_scenarios=[]

    
        for d_pos in standard['Position']:
            x = d_pos
            all_d_pos = [[x, 0, 0], [0, x, 0], [0, 0, x], [-x, 0, 0], [0, -x, 0], [0, 0, -x]]
            for d_pos_i in all_d_pos:
                scenario_i = f"Range 0.0 --- IsoCenterShift{d_pos_i}"
                all_scenarios.append(scenario_i)
        all_scenarios.append(f"Range {d_range:.1f} --- IsoCenterShift[0, 0, 0]")
        all_scenarios.append(f"Range {-1*d_range:.1f} --- IsoCenterShift[0, 0, 0]")
    
        return all_scenarios
    
def get_robust_evaluation_parameters(case,beam_set):
    all_scenarios=[]
    rsgs = [x for x in case.TreatmentDelivery.RadiationSetScenarioGroups if x.DiscreteFractionDoseScenarios[0].ForBeamSet.BeamSetIdentifier() == beam_set.BeamSetIdentifier()]
    for rsg in rsgs:
        scenarios = list(rsg.DiscreteFractionDoseScenarios)
        
        # We will create an object for each scenario that consists of density and isocenter shift. This can then be checked against standard
        for scenario in scenarios:
            scenario_i = ""
            shifts = scenario.PerturbedDoseProperties
            
            scenario_i += f"Range {shifts.RelativeDensityShift*100:.1f} --- IsoCenterShift"
            scenario_i += str(list(shifts.IsoCenterShift.values())).replace('-0.0','0').replace('0.0','0')
            all_scenarios.append(scenario_i)
    return all_scenarios


def check_robust_evaluation_parameters(row_start,ws,case,beam_set):
    row = row_start
    ws[f"A{row}"] = "Robust Evaluation Parameters"
    ws[f"A{row}"].font = font_subtitle
    
    row +=1
    
    standard = get_robust_evaluation_standard_list(beam_set)
    if type(standard)==str:
        ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=6)
        ws[f'A{row}'] = standard
        ws[f'A{row}'].fill = fail_fill
        ws[f'A{row}'].font = font_normal
        return row + 2
    
    current_scenario_list = get_robust_evaluation_parameters(case,beam_set)

    ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=2)
    ws.merge_cells(start_row=row, start_column=3, end_row=row, end_column=4)
    ws[f"A{row}"] = "Standard Evaluation Scenario"
    ws[f"C{row}"] = "Scenario present in Evaluation Scenarios"
    for col in ['A','C']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
    apply_thick_border(ws,f'A{row}',f'D{row}')
    for scenario in standard:
        row +=1
        ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=2)
        ws.merge_cells(start_row=row, start_column=3, end_row=row, end_column=4)
        ws[f"A{row}"] = scenario
        if scenario in current_scenario_list:
            ws[f"C{row}"]="True"
            ws[f"C{row}"].fill = pass_fill
        else:
            ws[f"C{row}"]="False"
            ws[f"C{row}"].fill = fail_fill
        
        for col in ['A','C']:
            ws[f'{col}{row}'].font = font_normal
            ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
            
    return row + 2
    

              
def other_checks(row_start,ws,beam_set):
    #Check that table is never at 270
    row = row_start
    ws[f'A{row}'] = "Other Checks"
    ws[f'A{row}'].font = font_subtitle
    
    row+=1
    ws[f'A{row}'] = "Description"
    ws[f'B{row}'] = "Result"
    
    for col in ['A','B']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
    
    apply_thick_border(ws,f'A{row}',f'B{row}')
    
    row +=1
    
    # Couch table cannot be at 270
    ws[f'A{row}'] = "No treatment beams utilize a table yaw of 270"
    couch_angles = [int(x.CouchRotationAngle) for x in beam_set.Beams]
    if 270 in couch_angles:
        ws[f'B{row}'] = False
        ws[f'B{row}'].fill = fail_fill
    else:
        ws[f'B{row}'] = True
        ws[f'B{row}'].fill = pass_fill
    
    for col in ['A','B']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
    

    return row + 2
        
def number_to_letter(number):
    return chr(65 + number)

def check_isocenters(row_start,ws,plan):
    beamset_name_pattern =  r'^(?P<plan_name>.+?)(_Bst(?P<boost>\d+?))?(\^G(?P<gantry>\d+?))(R(?P<replan>\d+))?(_TD)?$'
    
    
    iso_pattern = r'^pISO(?:_(?P<site>[A-Za-z]+))?_P(?P<plan_num>\d+)(?:_R(?P<replan>\d+))?$'
    
    row = row_start #19
    
    ws[f'A{row}'] = "Isocenter Checks"
    ws[f'A{row}'].font = font_subtitle
    
    row+=1
    
    for idx,title in enumerate(['Beamset Name','Isocenter Name','Name Matches Standard','X','Y','Z','Iso Parameters equal (where applicable)']):
        col = number_to_letter(idx)
        ws[f'{col}{row}'] = title
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrap_text = True)
    apply_thick_border(ws,f'A{row}',f'G{row}')
    
    row += 1
    
    all_isocenter_names = np.unique([beam.Isocenter.Annotation.Name for beam_set in plan.BeamSets for beam in beam_set.Beams])
    all_isocenter_dict = {}
    for beam_set in plan.BeamSets:
        bs_name = beam_set.BeamSetIdentifier().split(":")[1]
        all_isocenter_dict[bs_name] = {}
        match_bs = re.match(beamset_name_pattern,bs_name)
        if not match_bs:
            ws.merge_cells(start_row=row, start_column=1, end_row=row, end_column=3)
            MessageBox.Show("BeamSet name does not follow standard formatting. Please fix and re-run script")
        #Check position of isocenters
            ws[f'A{row}'] = "BeamSet name does not follow standard formatting. Please fix and re-run script"
            ws[f'A{row}'].font = font_normal
            ws[f'A{row}'].fill = fail_fill
            return row + 2
          
        for beam in beam_set.Beams:
            name = beam.Isocenter.Annotation.Name
            if name in  all_isocenter_dict[bs_name]:
                pass
            else:
                all_isocenter_dict[bs_name][name]=beam.Isocenter.Position

                #assume they have same iso
                iso_pos_check = True
                if match_bs['plan_name'] not in ['BiBrst','BiCW','CSI']:
                    #all isos should be in the same position
                    all_iso_pos = np.unique([str(beam_i.Isocenter.Position) for BS in plan.BeamSets for beam_i in BS.Beams])
                    if len(all_iso_pos) != 1:
                        iso_pos_check = False
                elif match_bs['plan_name'] == 'CSI':
                    #Only z_component should change
                    if len(np.unique([beam_i.Isocenter.Position['x'] for BS in plan.BeamSets for beam_i in BS.Beams])) != 1 or len(np.unique([beam_i.Isocenter.Position['y'] for BS in plan.BeamSets for beam_i in BS.Beams])) != 1:
                        iso_pos_check = False
                else:
                    # Bibrst and BiCW can have different isos
                    iso_pos_check = "N/A"
                all_isocenter_dict[bs_name][name]['Iso Parameters equal (where applicable)'] = iso_pos_check
                
                #NAme standard check
                
                match_iso = re.match(iso_pattern,name)
                check_count=0
                if match_iso:
                    #Check that if there is a site it belongs to CSI or BiBrst
                    if match_iso['site']:
                        if match_bs['plan_name'] not in ['BiBrst','BiCW','CSI']:
                            check_count -= 1
                    #Check if replan matches beam_replan
                    if match_iso['replan']:
                        if match_iso['replan'] != match_bs['replan']:
                            check_count -= 1
                    # Check that plan_num matches index of beamset in plan.BeamSets
                    if not match_iso['plan_num']:
                        check_count -= 1
                    else: 
                        beamset_names = [bs.BeamSetIdentifier().split(':')[1] for bs in plan.BeamSets]
                        if beamset_names.index(beam_set.BeamSetIdentifier().split(':')[1]) + 1 != int(match_iso['plan_num']):
                            check_count -= 1
                else:
                    check_count -=1
                
                if check_count == 0:
                    all_isocenter_dict[bs_name][name]['Name matches standard'] = True
                else:
                    all_isocenter_dict[bs_name][name]['Name matches standard'] = False

        
              
        
                
    
    for bs_name in all_isocenter_dict:
        for iso_name in all_isocenter_dict[bs_name]:
            data = all_isocenter_dict[bs_name][iso_name]
            data = [bs_name,iso_name,data['Name matches standard'],data['x'],data['y'],data['z'],data['Iso Parameters equal (where applicable)']]
            for idx,value in enumerate(data):
                col = number_to_letter(idx)
                ws[f'{col}{row}'] = value
                
                ws[f'{col}{row}'].font = font_normal
                ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
                
                if isinstance(value,bool):
                    if value == True:
                        ws[f'{col}{row}'].fill = pass_fill
                    elif value == False:
                        ws[f'{col}{row}'].fill = fail_fill
            
            row += 1
            
    return row + 1
        
def check_beamsets(ws,case,plan,examination,preplan = False):
    row = 17
    for beam_set in plan.BeamSets:
        row += 1
        check_beamset_name(row,ws,beam_set)
        check_dose_grid_resolution(row,ws,plan,examination)
        if not preplan:
            check_dose_algorithm(row,ws,plan,beam_set)
        check_any_beam_has_range_shifter(row,ws,beam_set)
    return row + 2


def get_hinge_angle(beam1,beam2):
    # if angle is 180.1, treat like 180
    if beam1.GantryAngle == 180.1:
        angle1 = 180
    else:
        angle1 = beam1.GantryAngle
    if beam2.GantryAngle == 180.1:
        angle2 = 180
    else:
        angle2 = beam2.GantryAngle
    # Convert to radians
    theta_g1 = angle1 * np.pi/180
    theta_c1 = beam1.CouchRotationAngle * np.pi/180
    
    theta_g2 = angle2 * np.pi/180
    theta_c2 = beam2.CouchRotationAngle * np.pi/180
    
    # Get components
    x1 = np.sin(theta_g1)*np.cos(theta_c1)
    y1 = np.sin(theta_g1)*np.sin(theta_c1)
    z1 = np.cos(theta_g1)
    
    x2 = np.sin(theta_g2)*np.cos(theta_c2)
    y2 = np.sin(theta_g2)*np.sin(theta_c2)
    z2 = np.cos(theta_g2)
    
    b1 = np.array([x1,y1,z1])
    b2 = np.array([x2,y2,z2])
    
    H = np.arccos(b1@b2)
    
    return np.round(H*180/np.pi,1)
    
def check_hinge_angles(row_start,ws,beam_set):
    beam_names = [beam.Name for beam in beam_set.Beams]
    if len(beam_names) < 2:
        return row_start
    plan_name = beam_set.BeamSetIdentifier().split(":")[1]
    min_hinge_angle = 30
    if 'Brain' in plan_name or 'BOS' in plan_name:
        min_hinge_angle = 50
        
    row = row_start + 1
    
    
    ws[f'A{row_start}'] = "Hinge Angle Check"
    ws[f'A{row_start}'].font = font_subtitle
    
    row += 1
    upper_left_cell = None
    bottom_right_cell = None
    for idx,beam in enumerate(beam_names):
        # column
        ws[f'{number_to_letter(idx+1)}{row}'] = beam
        ws[f'{number_to_letter(idx+1)}{row}'].font = font_normal
        ws[f'{number_to_letter(idx+1)}{row}'].alignment = Alignment(wrap_text = True)
        # row
        ws[f'A{row+idx+1}'] = beam
        ws[f'A{row+idx+1}'].font = font_normal
        ws[f'A{row+idx+1}'].alignment = Alignment(wrap_text = True)
        
    c=1 #offset for table so that nothing is put in 1st col , which is for row titles
    
    while len(beam_names)>0:
        c+=1
        row += 1
        # get beam that you'll check against all others
        beam_i = beam_names.pop(0)
        for idx2,beam_2 in enumerate(beam_names):
            if c==2 and idx2 == 0:
                upper_left_cell = f'{number_to_letter(idx2+c-1)}{row}'
            if beam_set.Beams[beam_i].Isocenter.Annotation.Name == beam_set.Beams[beam_2].Isocenter.Annotation.Name:
                H = get_hinge_angle(beam_set.Beams[beam_i],beam_set.Beams[beam_2])
                ws[f'{number_to_letter(idx2+c)}{row}'] = f"{H:.1f}"
                ws[f'{number_to_letter(idx2+c)}{row}'].font = font_normal
                ws[f'{number_to_letter(idx2+c)}{row}'].alignment = Alignment(wrap_text=True, horizontal = "center")
                if H >= min_hinge_angle:
                    ws[f'{number_to_letter(idx2+c)}{row}'].fill = pass_fill
                else:
                    ws[f'{number_to_letter(idx2+c)}{row}'].fill = fail_fill
            else:
                ws[f'{number_to_letter(idx2+c)}{row}'] = "N/A"
                ws[f'{number_to_letter(idx2+c)}{row}'].font = font_normal
                ws[f'{number_to_letter(idx2+c)}{row}'].alignment = Alignment(wrap_text=True, horizontal = "center")
    
    bottom_right_cell = f'{number_to_letter(idx2+c-1)}{row}'
    apply_normal_border_everywhere(ws,upper_left_cell,bottom_right_cell)
            
    return row + 2
    
def check_beam_order(row_start,ws,beam_set):
    row = row_start
    ws[f'A{row}'] = "Beams in correct order?"
    ws[f'A{row}'].font = font_normal_bold
    ws[f'A{row}'].alignment = Alignment(wrap_text = True)

    #Beams should go from 180 to 180.1, counter clockwise, i.e, 180,90,0,315,270,181, 180.1

    # First the beams without couch roations are delivered. Then beams with rototaion less than or equal to 90 are delivered. Then beams with rotation greater than or equal to 270 are delivered
    #We will get the correct order and then compare to order in the plan

    #Correct order
    expected_order = []
    #Coplanar check
    coplanar = [beam for beam in beam_set.Beams if beam.CouchRotationAngle==0]
    expected_order += sorted(coplanar, key = lambda x: x.GantryAngle +180 if x.GantryAngle <=180.0 else x.GantryAngle-180.0, reverse=True)
    
    #Non coplanar check - <= 90

    non_coplanar_90 = [beam for beam in beam_set.Beams if beam.CouchRotationAngle<=90 and beam.CouchRotationAngle!=0]
    expected_order += sorted(non_coplanar_90, key = lambda x: x.GantryAngle +180 if x.GantryAngle <=180.0 else x.GantryAngle-180.0, reverse=True)

    #Non coplanar check - >= 270
    non_coplanar_270 = [beam for beam in beam_set.Beams if beam.CouchRotationAngle>=270]
    expected_order += sorted(non_coplanar_270, key = lambda x: x.GantryAngle +180 if x.GantryAngle <=180.0 else x.GantryAngle-180.0, reverse=True)

    error_message = None
    # Now compare to order in plan
    if len(expected_order) == len(beam_set.Beams):
        correct_order = [beam.Name for beam in expected_order] == [beam.Name for beam in beam_set.Beams]
        
    else:
        correct_order = False
        error_beams = [x.Name for x in beam_set.Beams if x.Name not in [y.Name for y in expected_order]]
        error_message = f"Beams {error_beams} have errors. Please check that the beams couch rotation angles isn't in the range (90,270)"
    
    if correct_order:
        ws[f'B{row}'] = "Yes"
        ws[f'B{row}'].fill = pass_fill
        ws[f'B{row}'].font = font_normal
    else:
        ws[f'B{row}'] = "No" if not error_message else error_message
        ws[f'B{row}'].fill = fail_fill
        ws[f'B{row}'].font = font_normal
    
    return row + 2

def get_DO_values():
    # Reviewed with Kristen 02/28/2025
    values = {'DO_Artifact_Adipose': {'Name': 'adipose', 'Density': 0.95},
                'DO_Artifact_Muscle': {'Name': 'muscle', 'Density': 1.05},
                'DO_Artifact_Skin': {'Name': 'skin', 'Density': 1.09},
                'DO_BowelAir_Water': {'Name': 'water', 'Density': 1.0},
                'DO_EXP CASE_Steel': {'Name': 'steel', 'Density': 7.952},
                'DO_EXP MAG_Nd2Fe14B': {'Name': 'Nd2Fe14B', 'Density': 7.4},
                'DO_Implant_Water': {'Name': 'water', 'Density': 1.0},
                'DO_Implant_Silicone': {'Name': 'water', 'Density': 0.94},
                'DO_BariumCatheter_Water': {'Name': 'water', 'Density': 1.0},
                'DO_FeedingTube_Water': {'Name': 'water', 'Density': 1.0},
                'DO_ChemoPort_WET': {'Name': 'water', 'Density': 'WET'},
                'DO_TitanTouch_Steel': {'Name': 'steel', 'Density': 7.952},
                'DO_Contrast_Water': {'Name': 'water', 'Density': 1.0},
                'DO_CouchInsert_CarbonFiber': {'Name': 'carbon Fiber', 'Density': 0.212},
                'DO_kVueBase_CarbonFiber': {'Name': 'carbon Fiber', 'Density': 0.129},
                'DO_HydroGel_Water': {'Name': 'Water', 'Density': 1.018},
                'DO_SpaceIT_Water': {'Name': 'Water', 'Density': 1.03},
                'DO_Vaclock_Water': {'Name': 'water', 'Density': 0.089},
                'DO_ThickPad_Water': {'Name': 'water', 'Density': 0.077},
                'DO_ThinPad_Water': {'Name': 'water', 'Density': 0.089},
                'DO_BB_Air': {'Name': 'air', 'Density': 0.00121},
                'DO_Sheets_Air': {'Name': 'air', 'Density': 0.00121},
                'DO_Trach_WET': {'Name': 'water', 'Density': 'WET'}}
    
    
    return values

def check_all_density_overrides(row_start, ws, case):
    warn_fill = PatternFill(patternType="solid", start_color="FFFF00", end_color="FFFF00")

    do_values = get_DO_values()
    
    row = row_start
    ws[f'A{row}'] = "Density Override Checks"
    ws[f'A{row}'].font = font_subtitle
    
    row+=1
    ws[f'A{row}'] = "Density Override"
    ws[f'B{row}'] = "Name Check"
    ws[f'C{row}'] = "Material Expected"
    ws[f'D{row}'] = "Material Applied"
    ws[f'E{row}'] = "Density Expected [g/cc]"
    ws[f'F{row}'] = "Density Applied [g/cc]"

    
    for col in ['A','B','C','D','E','F']:
        ws[f'{col}{row}'].font = font_normal
        ws[f'{col}{row}'].alignment = Alignment(wrapText=True)
    
    apply_thick_border(ws,f'A{row}',f'F{row}')
    
    

    # Get results for all density overrides
    results = []
    # all rois starting with DO_
    roi_DO = [x.Name for x in case.PatientModel.RegionsOfInterest if x.Name.startswith('DO_')]
    for do in set(roi_DO + get_DO_roi_names(case)):
        roi = case.PatientModel.RegionsOfInterest[do]
        row_ = {"Density Overrides": roi.Name}
        if roi.Name.startswith('DO_'):
            row_['Name Check'] = 'Pass'
        else:
            row_['Name Check'] = 'Fail'
        
        try:
            expected_values = do_values[roi.Name]
            row_['Material Expected'] = expected_values['Name']
            row_['Density Expected [g/cc]'] = expected_values['Density']
        except KeyError:
            row_['Material Expected'] = 'Not in list'
            row_['Density Expected [g/cc]'] = 'Not in list'

        if hasattr(roi.RoiMaterial,'OfMaterial'):
            row_['Material Applied'] = roi.RoiMaterial.OfMaterial.Name
            row_['Density Applied [g/cc]'] = roi.RoiMaterial.OfMaterial.MassDensity
        else:
            row_['Material Applied'] = "No override applied"
            row_['Density Applied [g/cc]'] = "No override applied"
        results.append(row_)
    
    # Write results to worksheet
    for result in results:
        row +=1
        ws[f'A{row}'] = result['Density Overrides']
        ws[f'B{row}'] = result['Name Check']
        if result['Name Check'] == 'Fail':
            ws[f'B{row}'].fill = fail_fill
        else:
            ws[f'B{row}'].fill = pass_fill
        ws[f'C{row}'] = result['Material Expected']
        ws[f'D{row}'] = result['Material Applied']
        ws[f'E{row}'] = result['Density Expected [g/cc]']
        ws[f'F{row}'] = result['Density Applied [g/cc]']
        if result['Density Expected [g/cc]'] != result['Density Applied [g/cc]']:
            if result['Density Expected [g/cc]'] == 'WET':
                ws[f'F{row}'].fill = warn_fill
            else:
                ws[f'F{row}'].fill = fail_fill
        else:
            ws[f'F{row}'].fill = pass_fill
    
    row += 2
    return row



def run_checks(template = r"\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\plan_check\template_no_logo.xlsx",preplan=False):
    wb = openpyxl.load_workbook(template)
    ws = wb['PlanCheck']

    '''
    # Add logo to excel
    img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
    img.anchor = 'A1'
    ws.add_image(img)
    '''

    offset_beam_num = show_input_dialog

    patient = get_current("Patient")
    plan = get_current("Plan")
    examination = get_current("Examination")
    case = get_current("Case")

    offset_beam_num = show_input_dialog("Offset for Beam Numbering", "If there are multiple beamsets, please enter the offset for the beam numbering (if X is entered, the first beam will be expected to be X+1)")
    
    #Add plan name to A4
    ws['A4'] = f"Plan Check Report - {plan.Name}"
    
    get_patient_info(ws,patient)
    report_info(ws)
    check_plan_name(ws,plan)
    CT_Scan_Checks(ws,case,examination)
    
    
    # All BeamSet checks
    
    row_start = check_beamsets(ws,case,plan,examination,preplan=preplan)
    row_start = check_isocenters(row_start,ws,plan)
    row_start = check_all_density_overrides(row_start,ws,case)
    
    for idx,beam_set in enumerate(plan.BeamSets):
        row_start += 1
        ws.row_breaks.append(Break(id=row_start-1))
        ws[f'A{row_start}'] = beam_set.BeamSetIdentifier() + ' - AUTOMATED CHECKS'
        ws[f'A{row_start}'].font = font_title
        row_start += 2
        
        row_start = check_min_max_MU(row_start,ws,plan,beam_set)
        check_beam_names(row_start,ws,plan,beam_set,offset_beam_num)
        check_RS_min_airGap(row_start,ws,patient,plan,beam_set, case, examination, min_airGap_accepted = 7.0,preplan = preplan)
        row_start = check_snout_position(row_start,ws,beam_set, default_snout_position = 42.2)
        row_start = check_beam_order(row_start,ws,beam_set)
        row_start = check_setup_beams(row_start,ws,beam_set,plan,offset_beam_num)
        row_start = other_checks(row_start,ws,beam_set)
        row_start = check_hinge_angles(row_start,ws,beam_set)
        row_start = check_robust_optimization(ws,plan,beam_set,row_start)
        row_start = check_robust_evaluation_parameters(row_start,ws,case,beam_set)
        
    

    fp = r"\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\plan_check\temp"
    fp = os.path.join(fp,f"{patient.Name}__{patient.PatientID}__{plan.Name}__.xlsx")
    wb.save(fp)
    wb.close()
    


if __name__ == '__main__':
    try:
        pp = get_current("Plan")
    except:
        MessageBox.Show("No plan is loaded. Load a plan and then re-run the script")
        exit()
    
    approvals = []
    for bs in pp.BeamSets:
        if hasattr(bs.Review,"ApprovalStatus"):
            if bs.Review.ApprovalStatus == "Approved":
                approvals.append(True)
            else:
                approvals.append(False)
        else:
            approvals.append(False)
            
    if np.any(approvals):
        MessageBox.Show("Cannot have approved beamsets in plan")
        exit()
    else:
        try:
            preplan = False #show_yes_no_message_box("Pre-Plan?","Is this a Pre-Plan check?")
            run_checks(preplan=preplan)
            '''
            if not preplan:
                plan = get_current("Plan")
                optimization_dose_algorithm = list(plan.PlanOptimizations)[-1].OptimizationParameters.DoseCalculation.OptimizationDoseAlgorithm.DoseAlgorithm
                final_doses = []
                
                ui_algo = []
                for beam_set in plan.BeamSets:
                    if hasattr(beam_set,'FractionDose'):
                        for x in beam_set.FractionDose.BeamDoses:
                            if x.DoseValues.AlgorithmProperties.DoseAlgorithm != None:
                                final_doses.append(x.DoseValues.AlgorithmProperties.DoseAlgorithm + '__' + beam_set.BeamSetIdentifier().split(':')[1] + '__' + x.ForBeam.Name)
                            else:
                                final_doses.append('None__' + beam_set.BeamSetIdentifier().split(':')[1] + '__' + x.ForBeam.Name)
                    else:
                        final_doses.append('None__' + beam_set.BeamSetIdentifier().split(':')[1])# + '__' + x.ForBeam.Name)
                    #final_doses += [x.DoseValues.AlgorithmProperties.DoseAlgorithm + '__' + beam_set.BeamSetIdentifier().split(':')[1] + '__' + x.ForBeam.Name for x in beam_set.FractionDose.BeamDoses]
                    ui_algo += [beam_set.AccurateDoseAlgorithm.DoseAlgorithm + '__' + beam_set.BeamSetIdentifier().split(':')[1]]
                result = np.all(['MonteCarlo' in x for x in final_doses + [optimization_dose_algorithm] + ui_algo])
                if result:
                    run_checks()
                else:
                    message = "All options below should use MonteCarlo. Please adjust and re-run\n\n"
                    message += "Optimization Dose Algorithm:\n" + "\t" + f"{optimization_dose_algorithm}" +"\n"
                    message += "Final Dose Algorithm:\n"
                    for final_dose in final_doses:
                        message += "\t" + f"{final_dose}" + "\n"
                    message += f"UI Final Dose Algorithm:\n"
                    for ui in ui_algo:
                        message += "\t" + f"{ui}" + "\n"
                    MessageBox.Show(message,"Error with Dose Algorithm", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    exit()
            else:
                run_checks(preplan = preplan)
            '''
        except:
            MessageBox.Show("Error has occurred. Please contact Mauricio Acosta\n\n" + f"{traceback.format_exc()}","Error running script", MessageBoxButtons.OK, MessageBoxIcon.Error)
            


