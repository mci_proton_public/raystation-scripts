# Purpose

This script is designed to perform various checks and operations related
to patient data and examination scans. It includes functions for
retrieving and verifying patient and examination information and
standards are met.

# Workflow

**Plan Name Check**

-   check_plan_name(plan): Verifies that the plan name adheres to
    predefined naming conventions.

**CT Scan Checks**

-   CT_Scan_Checks(ws, case, examination): Performs various checks on
    the CT scan, including pixel size, calibration curve, protocol, and
    the existence of an 800 FOV scan.

**Beam Sets Check**

-   check_beamsets(plan): Ensures that the beam sets in the plan follow
    the established naming conventions, dose grid, dose algorithm, and
    range shifter checks.

**Isocenters Check**

-   check_isocenters(plan): Verifies the isocenter names and equality of
    isocenters where applicable.

**Beam Set Specific Checks**

-   check_min_max_MU(beam_set): Checks the minimum and maximum monitor
    units (MU) for the beam set.

-   check_beam_names(beam_set): Ensures that the beam names follow the
    required naming conventions.

-   check_RS_min_airGap(beam_set): Verifies the minimum air gap for the
    RS (Range Shifter).

-   check_snout_position(beam_set): Checks the snout position for the
    beam set.

-   check_beam_order(beam_set): Ensures that the beams are in the
    correct order.

-   check_setup_beams(beam_set): Verifies the setup beams in the beam
    set.

-   other_checks(beam_set): Currently this includes verifying that no
    treatment beams utilize a table yaw of 270

-   check_hinge_angles(beam_set): Checks the hinge angles for the beam
    set.

-   check_robust_optimization(beam_set): Verifies the robust
    optimization parameters for the beam set.

-   check_robust_evaluation_parameters(beam_set): Ensures that the
    standard robust evaluation parameters are included for the beam set.

# How to use

-   Press play
