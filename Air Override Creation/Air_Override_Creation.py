import clr
clr.AddReference("System.Windows.Forms")
clr.AddReference('System.Windows')
from System.Windows.Forms import MessageBox
from connect import get_current, RayWindow
from System.Windows import WindowStartupLocation, Thickness
from System.Windows.Controls import CheckBox, RowDefinition, Grid
from System.Windows.Media import Brushes
import numpy as np

__version__ = "0.0.1"



class Air_Override_Creation(RayWindow):
    def __init__(self, patient):
        '''
        fp_xaml = r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Air Override Creation\Air_Override_Creation.xaml'
        with open(fp_xaml, 'r') as f:
            self.LoadComponent(f.read())
        '''
        fp_xaml="""<Window
  xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
  xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
  Title="Air Override Creation" Height="Auto" MaxHeight="600" Width="600" SizeToContent="WidthAndHeight" Foreground="#FFFFFF">
<DockPanel Background="#2c2c2c">

  
  <StackPanel Orientation="Horizontal" DockPanel.Dock="Bottom" HorizontalAlignment="Center">
    <Button Content="Go" Width ="70" FontSize="16" Padding="4" Margin="20,10,0,30"  Click="StartAnalysis" IsDefault = 'True' Background="#7f8080" Foreground="#FFFFFF">
        <Button.Resources>
            <Style TargetType="Border">
                <Setter Property="CornerRadius" Value="15"/>
            </Style>
        </Button.Resources>
    </Button>
    <Button Content="Close" Width ="70" Margin="20,10,0,30" FontSize="16" Padding="4" Click="CloseWindow"  Background="#7f8080" Foreground="#FFFFFF">
        <Button.Resources>
            <Style TargetType="Border">
                <Setter Property="CornerRadius" Value="15"/>
            </Style>
        </Button.Resources>
    </Button>
  </StackPanel>
  

  
  <DockPanel DockPanel.Dock="Top">
    <TextBlock Text="Select OARs to intersect with air structure:" TextWrapping="Wrap" FontSize="16" Margin="0,30,10,10" TextAlignment="Center"/>
  </DockPanel>
  

  

  <Border BorderBrush="Black" BorderThickness="0.5" Margin="30,0,40,0" >  
    <ScrollViewer Background="#313131" Width="300">
      <Grid DockPanel.Dock="Top" Name="OARGrid">
        <Grid.ColumnDefinitions>
          <ColumnDefinition/>
          <ColumnDefinition/>
        </Grid.ColumnDefinitions>
      </Grid>
    </ScrollViewer>
  </Border>





</DockPanel>
</Window>
"""     
        self.LoadComponent(fp_xaml)
        self.Topmost = True
        self.WindowStartupLocation = WindowStartupLocation.CenterScreen
        self.case = case
    # A list of OARs
        self.contours = {}
        self.contours2 = {}
        self.contours3 = {}
        for rois in structure_set.RoiGeometries:
            if rois.HasContours() and (rois.OfRoi.Type == 'Organ' or rois.OfRoi.Type == 'Undefined' or rois.OfRoi.Type == 'Control'):
                roiname = rois.OfRoi.Name
                self.add_row_OAR(roiname, patient)

    def CloseWindow(self, sender, event):
        self.DialogResult = False

    # Populate the list of OARs in the GUI
    def add_row_OAR(self, roiname, patient):
        row_OAR = self.OARGrid.RowDefinitions.Count
        self.OARGrid.RowDefinitions.Add(RowDefinition())
        cb_OAR = CheckBox()
        cb_OAR.Margin = Thickness(10., 5., 5., 5.)
        cb_OAR.SetValue(Grid.RowProperty, row_OAR)
        cb_OAR.SetValue(Grid.ColumnProperty, 0)
        text2 = roiname
        cb_OAR.Content = text2
        cb_OAR.Foreground = Brushes.White
        self.OARGrid.Children.Add(cb_OAR)
        self.contours2[row_OAR] = roiname

    def StartAnalysis(self, sender, event):
        self.Remove()

    def Remove(self):
        global oarlist
        for d in self.OARGrid.Children:
            if d.GetValue(Grid.ColumnProperty) == 0 and d.IsChecked:
                row2 = d.GetValue(Grid.RowProperty)
                roi = self.contours2[row2]
                oarlist.append(roi)
        self.DialogResult = False

        rois = [r.OfRoi.Name for r in structure_set.RoiGeometries]
        # Update name to be f"DO_BowelAir_Water
        air_name = "DO_BowelAir_Water"
        '''
        if air_name in rois:
            last_versions = [int(x.split('_')[-1]) for x in rois if air_name in x if x.split('_')[-1] != 'Water']
            if len(last_versions) == 0:
                next_version = 1
            else:
                next_version = int(np.max(last_versions) + 1)
            air_name = air_name + f'_{next_version}'
        '''
        overridenumber = None
        water_material = None
        for m, materials in enumerate(patient_db.GetTemplateMaterials()):
            if materials.Material.Name == 'Water':
                overridenumber = m
                water_material = materials.Material
                print(f"\n\nFound water : {m = }\n\n")
                break
        print(f"\n\nOutside of loop: {m = }\n\n")
        if not water_material:
            MessageBox.Show("Water material is not in patient_db. Contact Mauricio Acosta", "Error: Water material not found")
            quit()
        case.PatientModel.CreateRoi(Name=r"Air", Color="Yellow", Type="Undefined", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)
        case.PatientModel.RegionsOfInterest[r"Air"].GrayLevelThreshold(Examination=examination, LowThreshold=-1024, HighThreshold=-150, PetUnit=r"", CbctUnit=None, BoundingBox=None)

        if air_name not in rois:
            case.PatientModel.CreateRoi(Name=air_name, Color="Yellow", Type='Undefined', TissueName=None, RbeCellTypeName=None, RoiMaterial=water_material)

        case.PatientModel.RegionsOfInterest[air_name].CreateAlgebraGeometry(
            Examination=examination,
            Algorithm="Auto",
            ExpressionA={
                'Operation': "Union",
                'SourceRoiNames': [r"Air"],
                'MarginSettings': {
                    'Type': "Expand",
                    'Superior': 0,
                    'Inferior': 0,
                    'Anterior': 0,
                    'Posterior': 0,
                    'Right': 0,
                    'Left': 0}},
            ExpressionB={
                'Operation': "Union",
                'SourceRoiNames': oarlist,
                'MarginSettings': {
                    'Type': "Expand",
                    'Superior': 0,
                    'Inferior': 0,
                    'Anterior': 0,
                    'Posterior': 0,
                    'Right': 0,
                    'Left': 0}},
            ResultOperation="Intersection",
            ResultMarginSettings={
                'Type': "Expand",
                'Superior': 0,
                'Inferior': 0,
                'Anterior': 0,
                'Posterior': 0,
                'Right': 0,
                'Left': 0})

        case.PatientModel.RegionsOfInterest[r"Air"].DeleteRoi()


if __name__ == "__main__":
    try:
        plan = get_current("Plan")
    except Exception as e:
        MessageBox.Show(f"This script requires a loaded plan, try again.\n\nError message:\n\n{e}", "Error: Load Plan")
        quit()

    case = get_current("Case")
    plan = get_current("Plan")
    beam_set = get_current("BeamSet")
    structure_set = plan.GetTotalDoseStructureSet()
    patient = get_current("Patient")
    examination = get_current("Examination") # beam_set.GetPlanningExamination()
    exam = examination.Name
    patient_db = get_current("PatientDB")
    oarlist = []
    try:
        dialog = Air_Override_Creation(patient)
        dialog.ShowDialog()
    except Exception as e:
        MessageBox.Show("Error. Please revise changes and undo if necessary.\n\n" + f"{e}")
