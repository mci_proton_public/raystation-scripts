import clr
# from System.Windows import *
# from System.Windows.Controls import *
from connect import get_current, RayWindow
from System.Windows.Forms import MessageBox
from System.Windows import WindowStartupLocation, Thickness
from System.Windows.Controls import CheckBox, RowDefinition, Grid
from System.Windows.Media import Brushes
clr.AddReference("System.Windows.Forms")
clr.AddReference('System.Windows')

__version__='0.0.1'

class OAR_Opti_Creation(RayWindow):
    def __init__(self, patient):
        global targetlist, oarlist, beamlist, structure_set, case, plan, beam_set, examination, exam
        targetlist = []
        oarlist = []
        beamlist = []
        case = get_current("Case")
        plan = get_current("Plan")
        beam_set = get_current("BeamSet")
        structure_set = plan.GetTotalDoseStructureSet()
        examination = beam_set.GetPlanningExamination()
        exam = examination.Name

        self.load_xaml()
        self.LoadComponent(self.fp_xaml)
        self.Topmost = True
        self.WindowStartupLocation = WindowStartupLocation.CenterScreen
        self.case = case
        # A list of OARs
        self.contours = {}
        self.contours2 = {}
        self.contours3 = {}
        self.crop.Text = str(0)
        for rois in structure_set.RoiGeometries:
            if rois.HasContours() and (rois.OfRoi.Type == 'Gtv' or rois.OfRoi.Type == 'Ctv' or rois.OfRoi.Type == 'Ptv'):
                roiname = rois.OfRoi.Name
                self.add_row_target(roiname, patient)
            if rois.HasContours() and (rois.OfRoi.Type == 'Organ' or rois.OfRoi.Type == 'Undefined' or rois.OfRoi.Type == 'Control'):
                roiname = rois.OfRoi.Name
                self.add_row_OAR(roiname, patient)

    def load_xaml(self):
        self.fp_xaml = '''<Window
                              xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                              xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                              Title="OAR Opti Creation" Height="Auto" MaxHeight="600" Width="600" SizeToContent="WidthAndHeight" Foreground="#FFFFFF">
                            <DockPanel Background="#2c2c2c">
                              <StackPanel Orientation="Horizontal" DockPanel.Dock="Bottom" HorizontalAlignment="Center">
                                <Button Content="Go" Width ="70" FontSize="16" Padding="4" Margin="25,0,0,30"  Click="StartAnalysis" IsDefault = 'True' Background="#7f8080" Foreground="#FFFFFF">
                                    <Button.Resources>
                                        <Style TargetType="Border">
                                            <Setter Property="CornerRadius" Value="15"/>
                                        </Style>
                                    </Button.Resources>
                                </Button>
                                <Button Content="Close" Width ="70" Margin="20,0,0,30" FontSize="16" Padding="4" Click="CloseWindow"  Background="#7f8080" Foreground="#FFFFFF">
                                    <Button.Resources>
                                        <Style TargetType="Border">
                                            <Setter Property="CornerRadius" Value="15"/>
                                        </Style>
                                    </Button.Resources>
                                </Button>
                              </StackPanel>
                              <DockPanel DockPanel.Dock="Top">
                                <TextBlock Text="Select OARs:" TextWrapping="Wrap" FontSize="16" Margin="35,30,10,10" TextAlignment="Center"/>
                                <TextBlock Text="Select targets to crop from:" TextWrapping="Wrap" FontSize="16" Margin="125,30,10,10" TextAlignment="Center"/>
                              </DockPanel>
                              <DockPanel DockPanel.Dock="Bottom" Margin="10">
                                <TextBlock Text="Enter cropping margin in mm:" TextWrapping="Wrap" FontSize="16" Background="#2c2c2c" Foreground="#FFFFFF" Margin="200,25,0,25" Padding="4" TextAlignment="Center"/>
                                <TextBox Name="crop" Width="50" Margin="0,25,210,25" Padding="4" FontSize="16" Foreground="#000000" TextAlignment="Center"/>
                              </DockPanel>
                              <Border BorderBrush="Black" BorderThickness="0.5" Margin="30,0,40,0" >
                                <ScrollViewer Background="#313131" Width="300">
                                  <Grid DockPanel.Dock="Top" Name="OARGrid">
                                    <Grid.ColumnDefinitions>
                                      <ColumnDefinition/>
                                      <ColumnDefinition/>
                                    </Grid.ColumnDefinitions>
                                  </Grid>
                                </ScrollViewer>
                              </Border>
                              <Border BorderBrush="Black" BorderThickness="0.5" Margin="30,0,40,0" >
                                <ScrollViewer Background="#313131" Width="300">
                                  <Grid DockPanel.Dock="Top" Name="targetGrid">
                                    <Grid.ColumnDefinitions>
                                      <ColumnDefinition/>
                                      <ColumnDefinition />
                                    </Grid.ColumnDefinitions>
                                  </Grid>
                                </ScrollViewer>
                              </Border>
                            </DockPanel>
                            </Window>
                            '''

    def CloseWindow(self, sender, event):
        self.DialogResult = False

    # Populate the list of targets in the GUI
    def add_row_target(self, roiname, patient):
        row_target = self.targetGrid.RowDefinitions.Count
        self.targetGrid.RowDefinitions.Add(RowDefinition())
        cb_target = CheckBox()
        cb_target.Margin = Thickness(10., 5., 5., 5.)
        cb_target.SetValue(Grid.RowProperty, row_target)
        cb_target.SetValue(Grid.ColumnProperty, 0)
        text = roiname
        cb_target.Content = text
        cb_target.Foreground = Brushes.White
        self.targetGrid.Children.Add(cb_target)
        self.contours[row_target] = roiname

    # Populate the list of OARs in the GUI
    def add_row_OAR(self, roiname, patient):
        row_OAR = self.OARGrid.RowDefinitions.Count
        self.OARGrid.RowDefinitions.Add(RowDefinition())
        cb_OAR = CheckBox()
        cb_OAR.Margin = Thickness(10., 5., 5., 5.)
        cb_OAR.SetValue(Grid.RowProperty, row_OAR)
        cb_OAR.SetValue(Grid.ColumnProperty, 0)
        text2 = roiname
        cb_OAR.Content = text2
        cb_OAR.Foreground = Brushes.White
        self.OARGrid.Children.Add(cb_OAR)
        self.contours2[row_OAR] = roiname

    def StartAnalysis(self, sender, event):
        self.Remove()

    def Remove(self):
        global targetlist
        crop_text = self.crop.Text
        for c in self.targetGrid.Children:
            if c.GetValue(Grid.ColumnProperty) == 0 and c.IsChecked:
                row = c.GetValue(Grid.RowProperty)
                roi = self.contours[row]
                if not structure_set.RoiGeometries[roi].HasContours():
                    continue
                targetlist.append(roi)
        global oarlist
        for d in self.OARGrid.Children:
            if d.GetValue(Grid.ColumnProperty) == 0 and d.IsChecked:
                row2 = d.GetValue(Grid.RowProperty)
                roi = self.contours2[row2]
                if not structure_set.RoiGeometries[roi].HasContours():
                    continue
                oarlist.append(roi)
        self.DialogResult = False

        crop_num = float(crop_text) / 10

        for oar in oarlist:
            oar_name = str(oar)
            opti_oar_name_prelim = r"z_" + str(oar) + "_opt_" + crop_text + "mm"
            rois = [r.OfRoi.Name for r in structure_set.RoiGeometries]
            if opti_oar_name_prelim + "_3" in rois:
                opti_oar_name = opti_oar_name_prelim + "_4"
            elif opti_oar_name_prelim + "_2" in rois:
                opti_oar_name = opti_oar_name_prelim + "_3"
            elif opti_oar_name_prelim + "_1" in rois:
                opti_oar_name = opti_oar_name_prelim + "_2"
            elif opti_oar_name_prelim in rois:
                opti_oar_name = opti_oar_name_prelim + "_1"
            else:
                opti_oar_name = opti_oar_name_prelim

            case.PatientModel.CreateRoi(Name=opti_oar_name, Color="Purple", Type="Undefined", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)

            case.PatientModel.RegionsOfInterest[opti_oar_name].CreateAlgebraGeometry(
                Examination=examination,
                Algorithm="Auto",
                ExpressionA={'Operation': "Union", 'SourceRoiNames': [r"" + oar_name + ""], 'MarginSettings': {'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0}},
                ExpressionB={'Operation': "Union", 'SourceRoiNames': targetlist, 'MarginSettings': {'Type': "Expand", 'Superior': crop_num, 'Inferior': crop_num, 'Anterior': crop_num, 'Posterior': crop_num, 'Right': crop_num, 'Left': crop_num}},
                ResultOperation="Subtraction",
                ResultMarginSettings={'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0})


if __name__ == '__main__':
    try:
        plan = get_current("Plan")
    except Exception as e:
        MessageBox.Show(f"This script requires a loaded plan, try again\n\n{e}")
        quit()
    patient = get_current('Patient')
    dialog = OAR_Opti_Creation(patient)
    dialog.ShowDialog()
