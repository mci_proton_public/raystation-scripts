# OAR_Opti_Creation.py
## Purpose
OAR_Opti_Creation.py automates the creation of optimized Organ at Risk (OAR) contours to enhance OAR protection by maintaining a specified margin from target volumes during treatment planning.
## Workflow
* **Initialization**: Gathers necessary data from the treatment planning system, including details about the current case, plan, beam set, structure set, and planning examination.
* **GUI Presentation**: A user interface lists all target volumes and OARs, allowing selection of OARs for optimization and target volumes for margin application.
* **Margin Specification**: Users define a cropping margin in millimeters to determine the subtraction distance from the OAR volume around the target volumes for optimized contour creation.
* **Optimized Contour Creation**: Generates a new contour for each selected OAR, named to indicate the optimization process and the margin applied. This contour is produced by subtracting the expanded target volume(s) from the original OAR volume based on the user-defined margin.
* **Contour Algebra**: Performs the subtraction operation using contour algebra, ensuring optimized OAR contours do not intersect with target volumes and adhere to the specified margin.
* **Finalization**: Completes the optimized OAR contour creation process, making them available for review and adjustment within the treatment planning system to confirm their clinical appropriateness.
## How to use
* Press play
* Select the OAR/OARs to crop the targets out of
* Select the target/targets for cropping
* Enter the cropping margin in mm
* The script takes each individual OAR and crops the combination of the selected targets out of the OAR with the specified margin
* The new cropped OARs are named z_[OAR Name]_opt_[Cropping Margin in mm]