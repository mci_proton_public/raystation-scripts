Script name is "OAR Opti Creation" in the MCI RayStation 9A database

Workflow:

-Press play
-Select the OAR/OARs to crop the targets out of
-Select the target/targets for cropping
-Enter the cropping margin in mm
-The script takes each individual OAR and crops the combination of the selected targets out of the OAR with the specified margin
-The new cropped OARs are named z_[OAR Name]_opt_[Cropping Margin in mm]


