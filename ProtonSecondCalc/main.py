from connect import *
import openpyxl
from openpyxl.styles import Font, Alignment, PatternFill, Border, Side
import datetime
import sys
import subprocess
import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox, MessageBoxButtons, MessageBoxIcon
import os
import re


def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

try:
    from reportlab.lib.pagesizes import letter
    from reportlab.pdfgen import canvas

except:
    set_progress("Installing Missing Libraries",50)
    install('reportlab')
    MessageBox.Show("Missing Libraries installed. Please re-run script","Restart Script", MessageBoxButtons.OK, MessageBoxIcon.Information)
    exit()

__version__ = '0.0.5'
TEMPLATE = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\script_folder_do_not_edit\ProtonSecondCalc\SecondCalc.xlsx'
case = get_current("Case")
plan = get_current("Plan")
date_approval = plan.Review.ReviewTime.ToShortDateString()
beam_set = get_current("BeamSet")
patient = get_current("Patient")

patient_name = ' '.join(patient.Name.split('^')[::-1])
pid = patient.PatientID
if plan.Review.ApprovalStatus != 'Approved':
    MessageBox.Show("Plan is not approved. Please approve plan before running script","Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    exit()
else:
    physician = "Dr."+plan.Review.ReviewerFullName.split('^')[0]
#physician = "Dr."+case.Physician.Name.split('^')[0]

# Define the font
FONT = Font(name='Calibri', size=11, bold=True)
ALIGNMENT = Alignment(horizontal='center', vertical='center')
LOGO = r"\\ad\dfs\Shared Data\MCI Proton\Mauricio\script_folder_do_not_edit\ProtonSecondCalc\LOGO_MCI.png"
FOLDER = r'\\ad\dfs\Shared Data\MCI Radiation Oncology\PROTON\Patient Treatment Plan Documents'


def get_folder_path():
    fname = patient.Name + '_' + pid
    if not os.path.exists(os.path.join(FOLDER, fname)):
        os.mkdir(os.path.join(FOLDER, fname))
    return os.path.join(FOLDER, fname)

FOL_I = get_folder_path()

def create_pdf(pdf_name, patient_name, pid, bs_name, total_dose, number_of_fractions, physician, data,date_approval):
    # Edit bs_name to remove ^G with digit
    match = re.match(r'^(?P<before_gantry>.*?)\^(?P<gantry>G\d)(?P<after_gantry>.*)$', bs_name)
    if match:
        groups = match.groupdict()
        if groups['after_gantry'] == '':
            bs_name = groups['before_gantry']
        else:
            bs_name = groups['before_gantry'] + '^' + groups['after_gantry']

    pdf_path = os.path.join(FOL_I, pdf_name)
    c = canvas.Canvas(pdf_path, pagesize=letter)
    width, height = letter
    
    # Accessory function to get where the text should start
    def get_text_start(text, column_text, column_start,
                       column_font_size = 12, text_font_size = 12,text_font = "Helvetica", column_font = "Helvetica-Bold"):
        return (2*column_start + c.stringWidth(column_text, column_font, column_font_size))/2 - c.stringWidth(text, text_font, text_font_size)/2
    
    # Add Baptist logo to report
    # y_position = height - 50
    # c.drawImage(image = LOGO, x=120, y=y_position, width=200, height=100, preserveAspectRatio=True, anchorAtXY=True)
    y_position = height - 60
    c.drawInlineImage(image = LOGO, x=200, y=y_position, height = 72, preserveAspectRatio=True, anchorAtXY=True)
    y_position -= 80
    # Add title to the PDF
    title_text = '2nd Check  - Pencil Beam Scanning Proton Plan'
    c.setFont("Helvetica",20)
    c.setFillColorRGB(125/255, 230/255, 155/255)
    c.rect(0, y_position, width, 25, fill=1, stroke=1)
    c.setFillColorRGB(0,0,0)
    c.drawString((width - c.stringWidth(title_text,"Helvetica",20))/2, y_position+4, title_text)
    y_position -= 30

    c.setFont("Helvetica-Bold", 12)
    # Add text to the PDF
    c.drawString(50, y_position, f"Patient Name: ")
    c.setFont("Helvetica",12)
    c.drawString(50 + c.stringWidth("Patient Name: ", "Helvetica-Bold", 12), y_position, patient_name)
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Patient ID: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Patient ID: ", "Helvetica-Bold", 12), y_position, pid)
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Treatment Plan Name: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Treatment Plan Name: ", "Helvetica-Bold", 12), y_position, bs_name)
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Total Dose: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Total Dose: ", "Helvetica-Bold", 12), y_position, f"{total_dose:.3f} Gy")
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Number of Fractions: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Number of Fractions: ", "Helvetica-Bold", 12), y_position, str(number_of_fractions))
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Physician: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Physician: ", "Helvetica-Bold", 12), y_position, physician)
    y_position -= 20

    c.setFont("Helvetica-Bold", 12)
    c.drawString(50, y_position, "Approval Date: ")
    c.setFont("Helvetica", 12)
    c.drawString(50 + c.stringWidth("Approval Date: ", "Helvetica-Bold", 12), y_position, date_approval)#datetime.datetime.now().strftime('%m/%d/%Y'))
    y_position -= 40
    
    # Add disclaimer
    c.setFont("Helvetica", 10)
    c.drawString(50, y_position, "* Secondary verification plan scaled to ensure same D95% as clinical treatment plan ")
    y_position -= 40

    c.setFont("Helvetica-Bold",14)
    c.setFillColorRGB(125/255, 230/255, 155/255)
    c.rect(0, y_position, width, 21, fill=1, stroke=1)
    c.setFillColorRGB(0,0,0)
    c.drawString((width - c.stringWidth("Beam Parameters","Helvetica-Bold",14))/2, y_position+4, "Beam Parameters")
    
    y_position -= 30

    c.setFont("Helvetica-Bold", 12)
    # Add table headers
    c.drawString(50, y_position, "Field #")
    c.drawString(100, y_position, "Range Shifter")
    c.drawString(200, y_position, "Field Description")
    c.drawString(320, y_position, "Treatment MUs")
    c.drawString(430, y_position, "2nd MUs")
    c.drawString(500, y_position, "% Difference")
    y_position -= 20

    c.setFont("Helvetica", 12)
    for idx, row in enumerate(data):
        c.drawString(get_text_start(str(row['index']), "Field #",50), y_position, str(row['index']))
        c.drawString(get_text_start(row['range_shifter'], "Range Shifter",100), y_position, row['range_shifter'])
        c.drawString(200, y_position, row['beam_name'])
        c.drawString(get_text_start(f"{row['mc_mu']:.3f}", "Treatment MUs",320), y_position, f"{row['mc_mu']:.3f}")
        c.drawString(get_text_start(f"{row['pba_mu']:.3f}", "2nd MUs",430), y_position, f"{row['pba_mu']:.3f}")
        c.drawString(get_text_start(row['difference'], "% Difference",500), y_position, row['difference'])
        y_position -= 20
    c.save()

MC_MU = {}
for beam_set_i in plan.BeamSets:
    MC_MU[beam_set_i.DicomPlanLabel] = {x.Name:x.BeamMU for x in beam_set_i.Beams}

new_plan_name = f"SecondCalc_{plan.Name}"

case.CopyPlan(PlanName=plan.Name, NewPlanName=new_plan_name, KeepBeamSetNames=True)

 
PBA_MU = {}
new_plan = case.TreatmentPlans[new_plan_name]
for iii,beam_set_i in enumerate(new_plan.BeamSets):
    set_progress(f"Computing dose for {beam_set_i.DicomPlanLabel}...",iii)
    prescription_roi = beam_set_i.Prescription.PrimaryPrescriptionDoseReference.OnStructure.Name
    #prescription_dose = beam_set_i.Prescription.PrimaryPrescriptionDoseReference.DoseValue
    #prescription_volume = beam_set_i.Prescription.PrimaryPrescriptionDoseReference.DoseVolume #Should always be 95 for 
    #Get D95 on nominal plan
    n_fractions = beam_set_i.FractionationPattern.NumberOfFractions
    prescription_dose = beam_set_i.FractionDose.GetDoseAtRelativeVolumes(RoiName=prescription_roi, RelativeVolumes=[.95])[0]*n_fractions
    beam_set_i.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False, RunEntryValidation=True)
    beam_set_i.ScaleToDoseGoal(DspName=None, RoiName=prescription_roi, DoseValue=prescription_dose, DoseVolume=95, PrescriptionType="DoseAtVolume", LockedBeamNames=None, EvaluateOptimizationFunctionsAfterScaling=True, IncludeBackgroundDose=False)
    PBA_MU[beam_set_i.DicomPlanLabel] = {x.Name:x.BeamMU for x in beam_set_i.Beams}

for idx_bs,bs_name in enumerate(MC_MU.keys()):
    set_progress(f"Creating report for {bs_name}...", (idx_bs+ .1)/len(MC_MU.keys())*100)
    total_dose = plan.BeamSets[bs_name].Prescription.PrimaryPrescriptionDoseReference.DoseValue/100
    number_of_fractions = plan.BeamSets[bs_name].FractionationPattern.NumberOfFractions
    
    data = []
    for idx,beam in enumerate(new_plan.BeamSets[bs_name].Beams):

        data.append({
            'index': idx + 1,
            'range_shifter': 'X' if beam.RangeShifterId else '',
            'beam_name': beam.Name,
            'mc_mu': MC_MU[bs_name][beam.Name],
            'pba_mu': PBA_MU[bs_name][beam.Name],
            'difference': f"{(PBA_MU[bs_name][beam.Name] - MC_MU[bs_name][beam.Name]) / MC_MU[bs_name][beam.Name] * 100:.2f}%"
        })
        
    # Create the PDF
    pdf_name = f"Second_Calc_{bs_name}_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}.pdf"
    create_pdf(pdf_name, patient_name, pid, bs_name, total_dose, number_of_fractions, physician, data, date_approval)
set_progress("Saving ... ",99.9)
patient.Save()
exit()