# ProtonSecondCalc Script
## Purpose
The ProtonSecondCalc script is designed to automate the process of calculating the difference in Monitor Units (MU) for each beam in the beamset when comparing the MonteCarlo algorithm and the Pencil Beam Algorithm in RayStation. This script aims to streamline the workflow for dosimetrists and physicists by automating the calculation and documentation process, ensuring that all necessary files are saved in the appropriate locations.

## Workflow
1. Initialization:
    * The script connects to the current RayStation session and retrieves the current patient, plan, and beam set information.
    * It ensures that all required Python libraries are installed and ready for use.

2. Folder Creation:
    * The script creates a dedicated folder for the patient within the shared network drive if it does not already exist. This folder is used to store all generated documents.

3. MU Calculation:
    * The script calculates the difference in Monitor Units (MU) for each beam in the beamset when comparing the MonteCarlo algorithm and the Pencil Beam Algorithm.
    * The results are documented and included in the generated PDF.

4. PDF Generation:
    * The script generates a PDF document from the treatment plan using the ReportLab library.
    * The PDF includes important details such as patient name, patient ID, beam set name, total dose, number of fractions, physician name, and the MU differences.
    * The script also includes the institution's logo in the PDF for branding purposes.

5. Saving Documents:
    * The generated PDF and any other relevant files are saved in the patient's dedicated folder on the shared network drive.

## Usage
* The script is intended to be run within the RayStation environment.
* Dosimetrists and physicists can execute the script to automatically calculate MU differences and generate treatment plan documents without manual intervention.

## Benefits
* **Efficiency:** Automates the repetitive task of calculating MU differences and generating treatment plan documents, saving time for dosimetrists and physicists.
* **Consistency:** Ensures that all calculations and documents are generated and saved in a consistent manner, reducing the risk of errors.
* **Organization:** Automatically organizes documents in a structured folder system on the shared network drive.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact
For any issues or questions regarding the script, please contact Mauricio Acosta at [MauricioAc@baptisthealth.net](mailto:MauricioAc@baptisthealth.net)