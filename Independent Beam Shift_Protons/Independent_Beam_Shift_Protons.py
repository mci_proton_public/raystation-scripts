"""
Last edited on 7/26/2022 by Lauren Rigsby
"""

import wpf, sys, clr, re
sys.path.append(r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Independent Beam Shift_Protons')
from System.Windows import *
from System.Windows.Controls import *
from connect import *
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox
from System.Windows.Media import Brushes
from connect import *


class Independent_Beam_Shift_Protons(Window):
  def __init__(self, patient):
    wpf.LoadComponent(self, r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Independent Beam Shift_Protons\Independent_Beam_Shift_Protons.xaml')
    self.Topmost = True
    self.WindowStartupLocation = WindowStartupLocation.CenterScreen
    case = get_current("Case")
    patient = get_current('Patient')
    beam_set = get_current("BeamSet")
    try:
      plan = get_current("Plan")
    except:
      MessageBox.Show("This script requires a loaded plan, try again")
      quit()
    self.beamsets = {}
    self.beams = {}
    self.xiso.Text = str(0)
    self.yiso.Text = str(0)
    self.ziso.Text = str(0)

    for beam in beam_set.Beams:
        beamname = beam.Name 
        self.add_beam_RL(beamname, patient)
        self.add_beam_SI(beamname, patient)
        self.add_beam_AP(beamname, patient)

  def CloseWindow(self, sender, event):
    self.DialogResult = False

  def Go(self, sender, event):
    self.MsgPanel.Visibility = Visibility.Visible
    self.Analyze()
    
# =============================================================================
#   def add_row(self, beamsetname, patient):
#     row = self.doseGrid.RowDefinitions.Count
#     self.doseGrid.RowDefinitions.Add(RowDefinition())
#     cb = CheckBox()
#     cb.Margin = Thickness(10,5,5,5)
#     cb.SetValue(Grid.RowProperty, row)
#     cb.SetValue(Grid.ColumnProperty, 0)
#     text = beamsetname
#     cb.Content = text
#     self.doseGrid.Children.Add(cb)
#     self.beamsets[row] = beamsetname
# =============================================================================
    
  def add_beam_RL(self, beamname, patient):
    row = self.beamGrid_RL.RowDefinitions.Count
    self.beamGrid_RL.RowDefinitions.Add(RowDefinition())
    cb = CheckBox()
    cb.Margin = Thickness(10,5,5,5)
    cb.SetValue(Grid.RowProperty, 0)
    cb.SetValue(Grid.ColumnProperty, row)
    text = beamname[0:3]
    cb.Content = text
    cb.Foreground = Brushes.White;
    self.beamGrid_RL.Children.Add(cb)
    self.beams[row] = beamname
    
  def add_beam_SI(self, beamname, patient):
    row = self.beamGrid_SI.RowDefinitions.Count
    self.beamGrid_SI.RowDefinitions.Add(RowDefinition())
    cb = CheckBox()
    cb.Margin = Thickness(10,5,5,5)
    cb.SetValue(Grid.RowProperty, 0)
    cb.SetValue(Grid.ColumnProperty, row)
    text = beamname[0:3]
    cb.Content = text
    cb.Foreground = Brushes.White;
    self.beamGrid_SI.Children.Add(cb)
    self.beams[row] = beamname

  def add_beam_AP(self, beamname, patient):
    row = self.beamGrid_AP.RowDefinitions.Count
    self.beamGrid_AP.RowDefinitions.Add(RowDefinition())
    cb = CheckBox()
    cb.Margin = Thickness(10,5,5,5)
    cb.SetValue(Grid.RowProperty, 0)
    cb.SetValue(Grid.ColumnProperty, row)
    text = beamname[0:3]
    cb.Content = text
    cb.Foreground = Brushes.White;
    self.beamGrid_AP.Children.Add(cb)
    self.beams[row] = beamname    

#===============================================================================
  def Analyze(self, sender, event):
    case = get_current("Case")
    patient = get_current('Patient')
    plan = get_current("Plan")
    planname = plan.Name
    newplanname = planname.replace(' ','')
    plantrunc = str(newplanname)[0:6]
    plan = case.TreatmentPlans[planname]
    beam_set = get_current("BeamSet")
    xtext = self.xiso.Text
    ytext = self.yiso.Text
    ztext = self.ziso.Text
#    beamsetlist = []

# =============================================================================
#     for c in self.doseGrid.Children:
#       if c.GetValue(Grid.ColumnProperty) == 0 and c.IsChecked:
#         row = c.GetValue(Grid.RowProperty)
#         beamsets = self.beamsets[row]
#         beamsetlist.append(beamsets)
# =============================================================================
        
    beams_RL_list = []   
    for c in self.beamGrid_RL.Children:
      if c.GetValue(Grid.RowProperty) == 0 and c.IsChecked:
        row = c.GetValue(Grid.ColumnProperty)
        beams = self.beams[row]
        beams_RL_list.append(beams)    

    beams_SI_list = []   
    for c in self.beamGrid_SI.Children:
      if c.GetValue(Grid.RowProperty) == 0 and c.IsChecked:
        row = c.GetValue(Grid.ColumnProperty)
        beams = self.beams[row]
        beams_SI_list.append(beams)            

    beams_AP_list = []   
    for c in self.beamGrid_AP.Children:
      if c.GetValue(Grid.RowProperty) == 0 and c.IsChecked:
        row = c.GetValue(Grid.ColumnProperty)
        beams = self.beams[row]
        beams_AP_list.append(beams)    
        
    # MessageBox.Show(str(beams_RL_list))   
    # MessageBox.Show(str(xtext))  
        
    #see if the checked beams are being read in correctly
    # beamsetname = []
    # for beamsets in plan.BeamSets:
    #   try:
    #     beamsetname.append(beamsets.DicomPlanLabel)
    #   except:
    #     beamsetname.append('Blank')
    # beamnumber = self.beam.Text
    # beam = int(beamnumber) - 1
    
    xshift = float(xtext)/10
    yshift = float(ytext)/10
    zshift = float(ztext)/10
    
    machinename = beam_set.MachineReference.MachineName
    exam = plan.TreatmentCourse.TotalDose.OnDensity.FromExamination.Name
    self.DialogResult = False

    if beam_set.AccurateDoseAlgorithm.DoseAlgorithm == 'IonMonteCarlo':
        MC = 1
    else:
        MC = 0

    name = beam_set.DicomPlanLabel
    beamset_num = beam_set.Number

    for b, beam in enumerate(beam_set.Beams):
        #X shift the selected beams
        if xshift != 0.0 and beam.Name in beams_RL_list:
          
          #shift negative x  
          shiftname = "-{0}x {1} {2}".format(xtext,beam.Name[0:3],planname)
          
          for plans in case.TreatmentPlans: #if the plan name already exists append it with V2
            if plans.Name == shiftname:
              shiftname = "-{0}x {1} {2}V2".format(xtext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel

          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "-{0}x {1}".format(xtext, iso_name)
          isocolor = beam.Isocenter.Annotation.DisplayColor
          newiso = {'x':iso.x - xshift, 'y':iso.y, 'z':iso.z}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          
          if MC == 0:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "-{0}x {1}".format(xtext, name)

          #shift positive x
          shiftname = "+{0}x {1} {2}".format(xtext,beam.Name[0:3],planname)
    
          
          for plans in case.TreatmentPlans:
             if plans.Name == shiftname:
               shiftname = "+{0}x {1} {2}V2".format(xtext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel

          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "+{0}x {1}".format(xtext, iso_name)
          newiso = {'x':iso.x + xshift, 'y':iso.y, 'z':iso.z}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          if MC == 0:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "+{0}x {1}".format(xtext, name)
        
        else:
            pass
    
    
    
        #Y shift the selected beams
        if yshift != 0.0 and beam.Name in beams_SI_list:
          
          #shift negative y  
          shiftname = "-{0}y {1} {2}".format(ytext,beam.Name[0:3],planname)
          
          for plans in case.TreatmentPlans: #if the plan name already exists append it with V2
            if plans.Name == shiftname:
              shiftname = "-{0}y {1} {2}V2".format(ytext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel
        
          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "-{0}y {1}".format(ytext, iso_name)
          isocolor = beam.Isocenter.Annotation.DisplayColor
          newiso = {'x':iso.x, 'y':iso.y, 'z':iso.z - yshift}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          
          if MC == 0:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "-{0}y {1}".format(ytext, name)
        
          #shift positive y
          shiftname = "+{0}y {1} {2}".format(ytext,beam.Name[0:3],planname)
        
          
          for plans in case.TreatmentPlans:
             if plans.Name == shiftname:
               shiftname = "+{0}y {1} {2}V2".format(ytext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel
        
          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "+{0}y {1}".format(ytext, iso_name)
          newiso = {'x':iso.x, 'y':iso.y, 'z':iso.z + yshift}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          if MC == 0:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "+{0}y {1}".format(ytext, name)
        
        else:
            pass


        #Z shift the selected beams
        if zshift != 0.0 and beam.Name in beams_AP_list:
          
          #shift negative z  
          shiftname = "-{0}z {1} {2}".format(ztext,beam.Name[0:3],planname)
          
          for plans in case.TreatmentPlans: #if the plan name already exists append it with V2
            if plans.Name == shiftname:
              shiftname = "-{0}z {1} {2}V2".format(ztext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel
        
          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "-{0}z {1}".format(ztext, iso_name)
          isocolor = beam.Isocenter.Annotation.DisplayColor
          newiso = {'x':iso.x, 'y':iso.y + zshift, 'z':iso.z}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          
          if MC == 0:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
               bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "-{0}z {1}".format(ztext, name)
        
          #shift positive z
          shiftname = "+{0}z {1} {2}".format(ztext,beam.Name[0:3],planname)
        
          
          for plans in case.TreatmentPlans:
             if plans.Name == shiftname:
               shiftname = "+{0}z {1} {2}V2".format(ztext,beam.Name[0:3],planname)
              
          case.CopyPlan(PlanName = plan.Name, NewPlanName = shiftname)
          shift_plan = case.TreatmentPlans[shiftname]
          for n, orig_beamset in enumerate(plan.BeamSets):
              shift_plan.BeamSets[n].DicomPlanLabel=orig_beamset.DicomPlanLabel
              
          for i, beamset in enumerate(shift_plan.BeamSets):
               if beamset.Number == beamset_num:
                  patient.Save()
                  shift_plan.SetCurrent()
                  beamset.SetCurrent()
                  bset = get_current("BeamSet")
                  bset_name=bset.DicomPlanLabel
        
          iso = beam.Isocenter.Position
          iso_name=beam.Isocenter.Annotation.Name
          shift_iso_name = "+{0}z {1}".format(ztext, iso_name)
          newiso = {'x':iso.x, 'y':iso.y - zshift, 'z':iso.z}
          bset.Beams[b].SetIsocenter(Name=iso_name)
          bset.Beams[b].Isocenter.EditIsocenter(Name = shift_iso_name, Position = newiso)
          for n, shift_beam in enumerate(bset.Beams):
              if shift_beam.Isocenter.Annotation.Name != shift_iso_name:
                  shift_beam.Isocenter.EditIsocenter(Name=iso_name, Color=isocolor)
          if MC == 0:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="SpotWeightPencilBeam", ForceRecompute=False)
          else:
                bset.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)
          bset.DicomPlanLabel = "+{0}z {1}".format(ztext, name)
        
        else:
            pass


