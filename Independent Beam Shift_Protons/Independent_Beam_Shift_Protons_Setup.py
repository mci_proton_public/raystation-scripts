"""
Last edited on 7/26/2022 by Lauren Rigsby
"""

import sys
sys.path.append(r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Independent Beam Shift_Protons')
from Independent_Beam_Shift_Protons import *
from connect import *
patient = get_current('Patient')
dialog = Independent_Beam_Shift_Protons(patient)
dialog.ShowDialog()

