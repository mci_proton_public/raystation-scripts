import re
import os
from readline import set_pre_input_hook
import openpyxl
from openpyxl.styles import Alignment, Font, PatternFill, Border, Side
from openpyxl.drawing.image import Image
from connect import *
import datetime
import numpy as np
import traceback

__version__ = "0.0.4"

def get_goals(plan):
    return [x for x in plan.TreatmentCourse.EvaluationSetup.EvaluationFunctions if x.PlanningGoal.Priority not in [99,100,9000]]

def split_string_by_capital(text):
    # Split the string based on capital letters
    parts = re.findall('[A-Z][^A-Z]*', text)
    
    # Join the parts with a space between each
    result = ' '.join(parts)
    return result

def get_planning_goal(goal):
    if goal.PlanningGoal.Type == "VolumeAtDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*100:.2f}%", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*10**-2:.2f}"," Gy (RBE)"])
    elif goal.PlanningGoal.Type == "DoseAtVolume":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*100:.2f}%"])
    elif goal.PlanningGoal.Type == "AverageDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type)]) 
    elif goal.PlanningGoal.Type == "DoseAtAbsoluteVolume":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue:.2f} cm3"])
    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel:.2f} cm3", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*10**-2:.2f} Gy (RBE)"])
def get_roi_poi(goal):
    return goal.ForRegionOfInterest.Name

def get_priority(goal):
    return goal.PlanningGoal.Priority

def get_goal_value(goal):
    if goal.PlanningGoal.Type == "VolumeAtDose":
        return f"{goal.GetClinicalGoalValue()*100:.2f}%"
    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        return f"{goal.GetClinicalGoalValue():.2f} cm3"
    else:
        return f"{goal.GetClinicalGoalValue()*10**-2:.2f} Gy (RBE)"

def get_status(goal):
    if goal.EvaluateClinicalGoal():
        return "Pass"
    else:
        return "Fail"
    '''
    # This had an rounding issue. Will simply used the discovered built in comparison
    goal_value = goal.PlanningGoal.AcceptanceLevel
    num_decimals = len(str(goal_value).split('.')[1])
    if goal.PlanningGoal.GoalCriteria == "AtLeast":
        if np.round(goal.GetClinicalGoalValue(),num_decimals) >= goal.PlanningGoal.AcceptanceLevel:
            return "Pass"
        else:
            return "Fail"
    else:
        if np.round(goal.GetClinicalGoalValue(),num_decimals) <= goal.PlanningGoal.AcceptanceLevel:
            return "Pass"
        else:
            return "Fail"
    '''

def get_robustness_goals():
    plan = get_current("Plan")
    return [x for x in plan.TreatmentCourse.EvaluationSetup.EvaluationFunctions if x.PlanningGoal.Priority in [99,100,9000]]

def get_structure_set():
    case = get_current("Case")
    examination = get_current("Examination")
    
    for ss in case.PatientModel.StructureSets:
        if ss.OnExamination.Name == examination.Name:
            return ss

def get_Roi_volume(RoiName):
    ss = get_structure_set()
    for roi in ss.RoiGeometries:
        if roi.OfRoi.Name == RoiName:
            return roi.GetRoiVolume()
    
def evaluate_goal_on_robustness_scenario(goal,rob_scene):
    beam_set = get_current("BeamSet")
    num_fractions = beam_set.FractionationPattern.NumberOfFractions
    RoiName = goal.ForRegionOfInterest.Name
    if goal.PlanningGoal.Type == 'VolumeAtDose':
        #Remember that the goal is for all fractions (total Dose) and we are evaluation per fraction
        dose = goal.PlanningGoal.ParameterValue #cGy
        dose /= num_fractions
        acceptance_vol = goal.PlanningGoal.AcceptanceLevel
        
        goal_value = rob_scene.GetRelativeVolumeAtDoseValues(RoiName = RoiName, DoseValues = [dose])[0]
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value*100:.2f} %"
        
    elif goal.PlanningGoal.Type == 'AverageDose':
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        goal_value = rob_scene.GetDoseStatistic(RoiName = RoiName,DoseType='Average')*num_fractions
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"
                
    elif goal.PlanningGoal.Type == "DoseAtAbsoluteVolume":
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        abs_volume = goal.PlanningGoal.ParameterValue #cm^3
        rel_volume = abs_volume/get_Roi_volume(RoiName)
        
        goal_value = rob_scene.GetDoseAtRelativeVolumes(RoiName = RoiName,RelativeVolumes = [rel_volume])[0]*num_fractions
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"
    
    elif goal.PlanningGoal.Type == "DoseAtVolume":
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        rel_volume = goal.PlanningGoal.ParameterValue 
        
        goal_value = rob_scene.GetDoseAtRelativeVolumes(RoiName = RoiName,RelativeVolumes = [rel_volume])[0]*num_fractions
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
                
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"

    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        dose = goal.PlanningGoal.ParameterValue #cGy
        dose /= num_fractions
        acceptance_vol = goal.PlanningGoal.AcceptanceLevel #cm3
        
        goal_value = rob_scene.GetRelativeVolumeAtDoseValues(RoiName = RoiName, DoseValues = [dose])[0]
        goal_value *= get_Roi_volume(RoiName)
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value:.2f} cm^3"
    
    
    return goal_value,goal_passing

def get_col_name(dd):
    xyz = 'xyz:('
    for i in np.array(list(dd.PerturbedDoseProperties.IsoCenterShift.values()))*10:
        if i==0:
            xyz+='0'
        elif int(str(float(i)).split('.')[1])==0:
            xyz+=str(float(i)).split('.')[0]
        else:
            xyz+=str(np.round(i,2))
        xyz+=','
    xyz = xyz[:-1]
    xyz+=")mm"
    return xyz            
           
     


def write_df(export_loc=r'\\ad\dfs\Shared Data\MCI Proton\Final TPD'):
    try:
        plan = get_current("Plan")
        patient = get_current("Patient")
        examination = get_current("Examination")
        case = get_current("Case")
        beam_set = get_current("BeamSet")
    except Exception as e:
        MessageBox.Show(f"Error: {e}")
        sys.exit()

    name = patient.Name.split('^')
    name.append(name[0])
    name.pop(0)
    name = ' '.join(name)


    '''
    df = pd.DataFrame()
    df['Priority'] = [get_priority(goal) for goal in get_goals(plan)]
    df['ROI/POI'] = [get_roi_poi(goal) for goal in get_goals(plan)]
    df['Clinical goal'] = [get_planning_goal(goal) for goal in get_goals(plan)]
    df['Value'] = [get_goal_value(goal) for goal in get_goals(plan)]
    df['Pass/Fail'] = [get_status(goal) for goal in get_goals(plan)]
    df['Comments'] = 20*' '
    '''
    set_progress("Creating Excel file",20)
    fp = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\clinical_goals\template_no_logo.xlsx'
    wb = openpyxl.load_workbook(fp)
    ws = wb['Nominal']
    '''
    # Add logo to excel
    img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
    img.anchor = 'A1'
    ws.add_image(img)
    '''

    pass_fill = PatternFill(patternType="solid", start_color="00008000", end_color="00008000")
    fail_fill = PatternFill(patternType="solid", start_color="00FF0000", end_color="00FF0000")
    font_normal = Font(name="Century Gothic",size=11)
    font_subtitle = Font(name = "Century Gothic", size = 14 , bold = True)

    bottom_border = Border(bottom = Side(style='thin'))

    ws['B6'] = name
    ws['B7'] = patient.PatientID
    ws['B8'] = beam_set.BeamSetIdentifier() 
    ws['B9'] = examination.Name
    ws['B10'] = datetime.date.today().strftime("%x")



    for i in range(6,11):
        ws[f'B{i}'].font = font_normal

    set_progress("Extracting nominal goal values",40)
    priorities = [get_priority(goal) for goal in get_goals(plan)]
    roi_poi = [get_roi_poi(goal) for goal in get_goals(plan)]
    clinical_goals = [get_planning_goal(goal) for goal in get_goals(plan)]
    values = [get_goal_value(goal) for goal in get_goals(plan)]
    pass_fail = [get_status(goal) for goal in get_goals(plan)]

    all_data = zip(priorities,roi_poi,clinical_goals,values,pass_fail)
    all_data = sorted(all_data, key = lambda x: (x[0],x[1],"Dose At Absolute Volume" not in  x[2]))

    set_progress("Writing to Excel File",50)
    start_row = 15
    for idx,data in enumerate(all_data):
        ws[f'A{idx+start_row}'] = data[0]
        ws[f'B{idx+start_row}'] = data[1]
        ws[f'C{idx+start_row}'] = data[2]
        ws[f'D{idx+start_row}'] = data[3]
        ws[f'E{idx+start_row}'] = data[4]
        
        for c in ['A','B','C','D','E']:
            ws[f'{c}{idx+start_row}'].font = font_normal
            ws[f'{c}{idx+start_row}'].alignment = Alignment(horizontal = 'center')
            if c not in ['A','B']:
                if ws.column_dimensions[c].width < len(str(ws[f'{c}{idx+start_row}'].value)):
                    ws.column_dimensions[c].width = len(str(ws[f'{c}{idx+start_row}'].value)) + 8
        ws[f'E{idx+start_row}'].fill = pass_fill if data[4] == 'Pass' else fail_fill
        ws[f'F{idx+start_row}'].border = None if data[4] == 'Pass' else bottom_border

    #Robustness tab

    thick_border = Border(left=None, 
                     right=None, 
                     top=Side(style='medium'), 
                     bottom=Side(style='medium'))
    right_border = Border(left=None, 
                     right=Side(style='medium'),
                     top=Side(style='medium'), 
                     bottom=Side(style='medium'))
    ws = wb['Robustness']
    start_row =8 # += idx + 4
    start_col = 4
    '''
    ws[f'A{start_row-2}'] = "Robustness Clinical Goals"
    ws[f'A{start_row-2}'].font = font_subtitle

    #Set table headers
    ws[f'A{start_row-1}'] = "Priority"
    ws[f'B{start_row-1}'] = "ROI/POI"
    ws[f'C{start_row-1}'] = "Clinical Goal"
    '''
    rob_goals = get_robustness_goals()
    # rsgs = [x for x in case.TreatmentDelivery.RadiationSetScenarioGroups if x.ReferencedRadiationSet.DicomPlanLabel == plan.Name and x.DiscreteFractionDoseScenarios[0].ForBeamSet.BeamSetIdentifier() == beam_set.BeamSetIdentifier()]
    rsgs = [x for x in case.TreatmentDelivery.RadiationSetScenarioGroups if x.DiscreteFractionDoseScenarios[0].ForBeamSet.BeamSetIdentifier() == beam_set.BeamSetIdentifier()]

    other_col = []
    #Evaluate on each discrete scenario for each radiation set group
    set_progress("Calculating robustness goal values",60)
    for rsg in rsgs:
        if "mm" in rsg.Name:
            for dd in rsg.DiscreteFractionDoseScenarios:
                other_col.append(get_col_name(dd))
        else:
            for dd in rsg.DiscreteFractionDoseScenarios:
                other_col.append(f"{dd.PerturbedDoseProperties.RelativeDensityShift*100:.1f}%")
                
    set_progress("Writing to Excel file",70)
    for i,col in enumerate(other_col):
        ws[f"{openpyxl.utils.get_column_letter(start_col+i)}{start_row-1}"] = col
        ws[f"{openpyxl.utils.get_column_letter(start_col+i)}{start_row-1}"].border = thick_border
        ws[f"{openpyxl.utils.get_column_letter(start_col+i)}{start_row-1}"].alignment = Alignment(textRotation = 90,vertical = 'center', horizontal = 'center')
        ws.column_dimensions[f"{openpyxl.utils.get_column_letter(start_col+i)}"].width = 3
        '''
        if ws.column_dimensions[f"{openpyxl.utils.get_column_letter(4+i)}"].width < len(col):
            ws.column_dimensions[f"{openpyxl.utils.get_column_letter(4+i)}"].width = len(col) + 2 #Alignment(wrap_text=True)
        '''
    ws[f"{openpyxl.utils.get_column_letter(start_col+i+1)}{start_row-1}"] = "Comments"
    ws[f"{openpyxl.utils.get_column_letter(start_col+i+1)}{start_row-1}"].border = right_border
    ws.column_dimensions[f"{openpyxl.utils.get_column_letter(start_col+i+1)}"].width = 20
    comment_column = f"{openpyxl.utils.get_column_letter(start_col+i+1)}"


    for idx,goal in enumerate(rob_goals):
        ws[f'A{idx+start_row}'] = goal.PlanningGoal.Priority
        ws[f'B{idx+start_row}'] = goal.ForRegionOfInterest.Name
        ws[f'C{idx+start_row}'] = get_planning_goal(goal)

        for col in ['A','B','C']:
            if ws.column_dimensions[f"{col}"].width < len(str(ws[f'{col}{idx+start_row}'].value)):
                ws.column_dimensions[f"{col}"].width = 4 + len(str(ws[f'{col}{idx+start_row}'].value))

        c=3
        for i,rsg in enumerate(rsgs):
            for ii,dd in enumerate(rsg.DiscreteFractionDoseScenarios):
                c+=1
                goal_value,goal_passing = evaluate_goal_on_robustness_scenario(goal,dd)
                ws[f'{openpyxl.utils.get_column_letter(c)}{idx+start_row}'] = goal_value

                if ws.column_dimensions[f"{openpyxl.utils.get_column_letter(c)}"].width < len(str(goal_value)):
                    ws.column_dimensions[f"{openpyxl.utils.get_column_letter(c)}"].width =len(str(goal_value)) + 2 #Alignment(wrap_text=True)

                if goal_passing:
                    ws[f'{openpyxl.utils.get_column_letter(c)}{idx+start_row}'].fill = pass_fill
                else:
                    ws[f'{openpyxl.utils.get_column_letter(c)}{idx+start_row}'].fill = fail_fill
                    ws[f'{comment_column}{idx+start_row}'].border = bottom_border




    
    fname = '_'.join([name,beam_set.DicomPlanLabel,datetime.datetime.today().strftime("%Y%m%d%H%M%S")])+'.xlsx'
    if not export_loc:
        fp = os.path.join(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Computational physicist\clinical_goals',fname)
    else:
        fp = os.path.join(export_loc,fname)
    
    wb.save(fp)
    wb.close()
    
    set_progress("Creating PDF",90)
    fp_pdf = os.path.join(r"\\ad\dfs\Shared Data\MCI Proton\Final TPD\pdf",fname.replace("xlsx","pdf"))
    
    import time
    def check_pdf_existence(fp_pdf, max_time_until_pdf_created = 60):
        print(fp_pdf)
        start_time = time.time()
        while time.time() - start_time < max_time_until_pdf_created:
            if os.path.exists(fp_pdf):
                return True
            time.sleep(1)
        return False
    
    found_pdf = check_pdf_existence(fp_pdf)

def register(fp = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\history.csv'):
    plan_id = get_current("BeamSet").BeamSetIdentifier()
    pName = get_current("Patient").Name
    pid = get_current("Patient").PatientID
    plan_name,beam_name = plan_id.split(':')
    with open(fp,'a') as f:
        f.write(f"{datetime.datetime.today().strftime('%x %X')},{pName},{pid},{plan_name},{beam_name}\n")
        
if __name__ == "__main__":
    try:
        #initialize var
        plan_id = None
        pName = None
        pid = None
        plan_name = None
        beam_name = None

        pName = get_current("Patient").Name
        pid = get_current("Patient").PatientID
        plan_id = get_current("BeamSet").BeamSetIdentifier()
        plan_name,beam_name = plan_id.split(':')
        
        register()
        write_df()
    except Exception as e:
        print(e)
        tb = traceback.format_exc().replace('\n',' --- ') + '\n'
        with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\Errors\log_errors_ray.txt','a') as f:
           f.write(f"{datetime.datetime.today().strftime('%x %X')},{pName},{pid},{plan_name},{beam_name},{tb}")
        