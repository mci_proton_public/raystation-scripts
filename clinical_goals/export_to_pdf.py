from win32com import client
import time
import os
import openpyxl
import PyPDF2 as pypdf
import sys
import warnings
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import traceback
import datetime
import numpy as np

warnings.filterwarnings("ignore")
def get_parameter_value(row):
    if row['Goal Type'] != 'Average Dose':
        return float(row['Clinical goal'].split(row['Goal Type'])[1].strip().split(" ")[0].split('%')[0])
    else:
        return 0

def create_robustness_figure(fp,export_loc=r'\\ad\dfs\Shared Data\MCI Proton\Final TPD\pdf'):
    different_goal_types = ['Volume At Dose','Average Dose','Dose At Absolute Volume','Dose At Volume',"Absolute Volume At Dose"]

    df = pd.read_excel(fp, sheet_name='Robustness',skiprows=6)
    if np.all(df.iloc[:,3:].isna()):
        # Create figure with text 'No robusteness scenarios for this plan:beamset'
        fig, ax = plt.subplots()
        ax.text(0.5, 0.5, 'No robustness scenarios for this plan:beamset', fontsize=12, ha='center')
        ax.axis('off')
        fp_fig = os.path.join(export_loc,os.path.basename(fp).replace('xlsx','png'))
        fig.savefig(fp_fig)
        return fp_fig
    else:
        #Extract info from clinical goal to create similarity
        df['Goal Type'] = df['Clinical goal'].apply(lambda x: [y for y in different_goal_types if y in x][0])
        df['Goal Criteria'] = df['Clinical goal'].apply(lambda x: [y for y in ['At Most','At Least'] if y in x][0])
        df['Acceptance Level'] = [float(x['Clinical goal'].split(x['Goal Type'])[0].split(x['Goal Criteria'])[1].strip().split(' ')[0].split('%')[0]) for _,x in df.iterrows()]
        df['Parameter Value'] = [get_parameter_value(x) for _,x in df.iterrows()]

        df_sorted = pd.DataFrame(columns = df.columns)
        for roi,data in df.groupby("ROI/POI"):
            found = False
            for goal_type, data2 in data.groupby("Goal Type"):
                for goal_criteria,data3 in data2.groupby("Goal Criteria"):
                    if len(data3) == 1:
                        df_sorted = df_sorted.append(data3)
                    elif len(data3) == 2:
                        df_sorted = df_sorted.append(data3.sort_values(by='Priority',ascending=True))
                    else:
                        sorted_df = pd.DataFrame(columns = df.columns)
                        for idx,row in data3.loc[data3['Priority']==99].iterrows():
                            sorted_df = sorted_df.append(row,ignore_index=False)
                            data_remaining = data3.drop(index=list(sorted_df.index))
                            data_remaining = data_remaining.loc[data_remaining['Priority']!=99]
                            if len(data_remaining)!=0:
                                data_remaining['Dist'] = [((x['Acceptance Level']-row['Acceptance Level'])/row['Acceptance Level'])**2 + ((x['Parameter Value']-row['Parameter Value'])/row['Parameter Value'])**2 for _,x in data_remaining.iterrows()]
                                sorted_df = sorted_df.append(data_remaining.sort_values(by='Dist').iloc[0,:],ignore_index=False)
                        data_remaining = data3.drop(index=list(sorted_df.index))
                        sorted_df = sorted_df.append(data_remaining)
                        df_sorted = df_sorted.append(sorted_df)

        df = df_sorted.copy()


        df.index = df['ROI/POI']+":"+df['Clinical goal']
        exclude_col = ['Priority','ROI/POI','Clinical goal','Goal Type','Goal Criteria','Acceptance Level','Parameter Value','Dist','Comments']
        df_color = pd.DataFrame()
        for goal,data in df.iterrows():
            if data['Goal Criteria'] == 'At Least':
                data_new = data.loc[[x for x in df.columns if x not in exclude_col]].apply(lambda x: float(x.split(' ')[0]) >= data['Acceptance Level'])
            else:
                data_new = data.loc[[x for x in df.columns if x not in exclude_col]].apply(lambda x: float(x.split(' ')[0]) <= data['Acceptance Level'])
            df_color = df_color.append(data_new)


        df_values = df.loc[:,[x for x in df_color.columns]].applymap(lambda x:float(x.split(' ')[0]))
        df_values = df_values.join(df['Priority'].astype(float))
        df_color = df_color.join(df['Priority'].astype(float))



        mask = df_color.copy()
        mask.loc[:,:] = False
        mask.loc[:,'Priority'] = True

        fig,ax=plt.subplots(figsize=(7.2,8))
        sns.heatmap(df_color,cmap='binary',vmin=98,vmax=100,annot=df_values,annot_kws={'size':8},fmt=".1f",cbar = False,ax=ax,linewidth=.5)
        sns.heatmap(df_color,cmap='RdYlGn',vmin=0,vmax=1,annot=df_values,annot_kws={'size':8},fmt=".1f",cbar = False,ax=ax,linewidth=.5,mask=mask)

        ax.set_yticklabels([x.get_text().replace(" At ","\nAt ") for x in ax.get_ymajorticklabels()],{'fontsize': 8})
        fig.tight_layout()
        fp_fig = os.path.join(export_loc,os.path.basename(fp).replace('xlsx','png'))
        fig.savefig(fp_fig)
        return fp_fig

def excel_2_pdf(fp,export_loc = r'\\ad\dfs\Shared Data\MCI Proton\Final TPD\pdf'):
        #pythoncom.CoInitialize()
        
        wb = openpyxl.load_workbook(fp)
        ws = wb['Nominal']

        # Add logo to excel
        img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
        img.anchor = 'A1'
        ws.add_image(img)

        '''
        ws = wb['Robustness']

        # Add logo to excel
        img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
        img.anchor = 'A1'
        ws.add_image(img)
        '''
        #Add detailed figure
        ws = wb['Robustness_details']

        # Add logo to excel
        img = openpyxl.drawing.image.Image(r"//ad/dfs/Shared Data/MCI Proton/Mauricio/Computational physicist/clinical_goals/resized_logo.png")
        img.anchor = 'A1'
        ws.add_image(img)
        fp_fig = create_robustness_figure(fp)
        img2 = openpyxl.drawing.image.Image(fp_fig)
        img2.anchor = 'A7'
        ws.add_image(img2)

        file = fp.replace(".xlsx","_with_figure.xlsx")
        wb.save(file)
        wb.close()

        os.remove(fp_fig)
        os.remove(fp)

        
        paths = []

        pdf_filepath_1 = os.path.join(export_loc,"_1_"+os.path.basename(file).replace('xlsx','pdf'))
        excel=client.gencache.EnsureDispatch('Excel.Application')
        excel.Visible = False
        wb = excel.Workbooks.Open(file)
        ws = wb.Worksheets['Nominal']
        ws.ExportAsFixedFormat(0,pdf_filepath_1)# ,From=1,To=1)
        paths.append(pdf_filepath_1)
        '''
        #Robustness
        pdf_filepath_2 = os.path.join(export_loc,"_2_"+os.path.basename(fp).replace('xlsx','pdf'))
        paths.append(pdf_filepath_2)
        ws = wb.Worksheets['Robustness']
        ws.ExportAsFixedFormat(0,pdf_filepath_2)# ,From=1,To=1)
        '''

        #Robustness
        pdf_filepath_3 = os.path.join(export_loc,"_3_"+os.path.basename(file).replace('xlsx','pdf'))
        paths.append(pdf_filepath_3)
        ws = wb.Worksheets['Robustness_details']
        ws.ExportAsFixedFormat(0,pdf_filepath_3)# ,From=1,To=1)

        wb.Close()
        excel.Quit()

        


        output_pdf = os.path.join(export_loc,os.path.basename(file).replace("_with_figure.xlsx",'.pdf'))
        while True:
            try:
                merge_pdfs(paths=paths, output = output_pdf)
                time.sleep(3)
                for path in paths:
                    os.remove(path)
                os.remove(file)
                break
            except:
                pass




def merge_pdfs(paths=[], output=None):
    '''Merge pdf files into one pdf file
    
    This function takes as input the paths to pdf files and merges them to output
    '''
    
    #Here we modify paths and output to correct format
    cwd= os.getcwd()
    if output[-4:]!='.pdf':
        output=output+'.pdf'
    if not os.path.isdir(os.path.dirname(output)):
        output=os.path.join(cwd,output)
        
    if len(paths)==0:
        for directory,folders,files in os.walk(cwd):
            files.sort()
            for file in files:
                if file[-4:]=='.pdf' and file!=os.path.basename(output):
                    paths.append(file)
            break
    if len(paths)==0:
        sys.exit('No pdf files provided and no pdf file in current directory = {}'.format(cwd))     
    
    #Done formatting. Now we can merge
    
    pdf_writer = pypdf.PdfFileWriter()

    for path in paths:
        pdf_reader = pypdf.PdfFileReader(path)
        for page in range(pdf_reader.getNumPages()):
            # Add each page to the writer object
            pdf_writer.addPage(pdf_reader.getPage(page))

    # Write out the merged PDF
    
    with open(output, 'wb') as out:
        pdf_writer.write(out)
    print('\n{:.0f} files merged to {}'.format(len(paths),os.path.basename(output)))

while True:
    try:
        search_fol = r'\\ad\dfs\Shared Data\MCI Proton\Final TPD'
        for file in os.listdir(search_fol):
            if file == 'pdf':
                pass
            elif file.split('.')[-1] == 'xlsx':
                excel_2_pdf(os.path.join(search_fol,file))
                
    except Exception:
        tb = traceback.format_exc().replace('\n',' --- ').replace(',',' ') + '\n'
        with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\Errors\log_errors.txt','r') as f:
            last_line = f.readlines()[-1]
            if last_line.split(',')[2] != tb:
                with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Log\ClinicalGoals\Errors\log_errors.txt','a') as f:
                   f.write(f"{datetime.datetime.today().strftime('%x %X')},{file},{tb}") 

# fp = r"//ad/dfs/Shared Data/MCI Proton/Final TPD/Reynaldo V Abad_pHN_20240520122640.xlsx"
# create_robustness_figure(fp)
