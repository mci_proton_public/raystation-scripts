import pandas as pd
from connect import get_current
import re
import numpy as np


def split_string_by_capital(text):
    # Split the string based on capital letters
    parts = re.findall('[A-Z][^A-Z]*', text)
    
    # Join the parts with a space between each
    result = ' '.join(parts)
    return result

def get_Roi_volume(RoiName):
    ss = get_structure_set()
    for roi in ss.RoiGeometries:
        if roi.OfRoi.Name == RoiName:
            return roi.GetRoiVolume()


def get_structure_set():
    case = get_current("Case")
    examination = get_current("Examination")
    
    for ss in case.PatientModel.StructureSets:
        if ss.OnExamination.Name == examination.Name:
            return ss
        
def get_goals(plan):
    return [x for x in plan.TreatmentCourse.EvaluationSetup.EvaluationFunctions if x.PlanningGoal.Priority not in [99,100]]

def get_priority(goal):
    return goal.PlanningGoal.Priority

def get_roi_poi(goal):
    return goal.ForRegionOfInterest.Name

def get_planning_goal(goal):
    if goal.PlanningGoal.Type == "VolumeAtDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*100:.2f}%", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*10**-2:.2f}"," Gy (RBE)"])
    elif goal.PlanningGoal.Type == "DoseAtVolume":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*100:.2f}%"])
    elif goal.PlanningGoal.Type == "AverageDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type)]) 
    elif goal.PlanningGoal.Type == "DoseAtAbsoluteVolume":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel*10**-2:.2f} Gy (RBE)", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue:.2f} cm3"])
    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        return ' '.join([split_string_by_capital(goal.PlanningGoal.GoalCriteria), f"{goal.PlanningGoal.AcceptanceLevel:.2f} cm3", split_string_by_capital(goal.PlanningGoal.Type),f"{goal.PlanningGoal.ParameterValue*10**-2:.2f} Gy (RBE)"])

def get_goal_value(goal):
    if goal.PlanningGoal.Type == "VolumeAtDose":
        return f"{goal.GetClinicalGoalValue()*100:.2f}%"
    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        return f"{goal.GetClinicalGoalValue():.2f} cm3"
    else:
        return f"{goal.GetClinicalGoalValue()*10**-2:.2f} Gy (RBE)"
    
def get_status(goal):
    goal_value = goal.PlanningGoal.AcceptanceLevel
    num_decimals = len(str(goal_value).split('.')[1])
    if goal.PlanningGoal.GoalCriteria == "AtLeast":
        if np.round(goal.GetClinicalGoalValue(),num_decimals) >= goal.PlanningGoal.AcceptanceLevel:
            return "Pass"
        else:
            return "Fail"
    else:
        if np.round(goal.GetClinicalGoalValue(),num_decimals) <= goal.PlanningGoal.AcceptanceLevel:
            return "Pass"
        else:
            return "Fail"

def get_robustness_goals():
    plan = get_current("Plan")
    return [x for x in plan.TreatmentCourse.EvaluationSetup.EvaluationFunctions if x.PlanningGoal.Priority in [99,100]]

def evaluate_goal_on_robustness_scenario(goal,rob_scene):
    beam_set = get_current("BeamSet")
    num_fractions = beam_set.FractionationPattern.NumberOfFractions
    RoiName = goal.ForRegionOfInterest.Name
    if goal.PlanningGoal.Type == 'VolumeAtDose':
        #Remember that the goal is for all fractions (total Dose) and we are evaluation per fraction
        dose = goal.PlanningGoal.ParameterValue #cGy
        dose /= num_fractions
        acceptance_vol = goal.PlanningGoal.AcceptanceLevel
        
        goal_value = rob_scene.GetRelativeVolumeAtDoseValues(RoiName = RoiName, DoseValues = [dose])[0]
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value*100:.2f} %"
        
    elif goal.PlanningGoal.Type == 'AverageDose':
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        goal_value = rob_scene.GetDoseStatistic(RoiName = RoiName,DoseType='Average')*30
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"
                
    elif goal.PlanningGoal.Type == "DoseAtAbsoluteVolume":
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        abs_volume = goal.PlanningGoal.ParameterValue #cm^3
        rel_volume = abs_volume/get_Roi_volume(RoiName)
        
        goal_value = rob_scene.GetDoseAtRelativeVolumes(RoiName = RoiName,RelativeVolumes = [rel_volume])[0]*num_fractions
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"
    
    elif goal.PlanningGoal.Type == "DoseAtVolume":
        acceptance_dose = goal.PlanningGoal.AcceptanceLevel
        rel_volume = goal.PlanningGoal.ParameterValue 
        
        goal_value = rob_scene.GetDoseAtRelativeVolumes(RoiName = RoiName,RelativeVolumes = [rel_volume])[0]*num_fractions
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_dose:
                goal_passing = True
            else:
                goal_passing = False
                
        goal_value = f"{goal_value/100:.2f} Gy (RBE)"

    elif goal.PlanningGoal.Type == "AbsoluteVolumeAtDose":
        dose = goal.PlanningGoal.ParameterValue #cGy
        dose /= num_fractions
        acceptance_vol = goal.PlanningGoal.AcceptanceLevel #cm3
        
        goal_value = rob_scene.GetRelativeVolumeAtDoseValues(RoiName = RoiName, DoseValues = [dose])[0]
        goal_value *= get_Roi_volume(RoiName)
        
        if goal.PlanningGoal.GoalCriteria == "AtLeast":
            if goal_value >= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        elif goal.PlanningGoal.GoalCriteria == "AtMost":
            if goal_value <= acceptance_vol:
                goal_passing = True
            else:
                goal_passing = False
        goal_value = f"{goal_value:.2f} cm^3"
    
    
    return goal_value,goal_passing




def write_df(export_loc=r'\\ad\dfs\Shared Data\MCI Proton\Final TPD'):
    try:
        plan = get_current("Plan")
        patient = get_current("Patient")
        examination = get_current("Examination")
        case = get_current("Case")
        beam_set = get_current("BeamSet")
    except Exception as e:
        MessageBox.Show(f"Error: {e}")
        sys.exit()

    name = patient.Name.split('^')
    name.append(name[0])
    name.pop(0)
    name = ' '.join(name)

    # Create dataframes for nominal and robust evaluation
    all_goals = get_goals(plan)
    df_nominal = pd.DataFrame({
        'Priority': [get_priority(goal) for goal in all_goals],
        'ROI/POI': [get_roi_poi(goal) for goal in all_goals],
        'Clinical goal': [get_planning_goal(goal) for goal in all_goals],
        'Value': [get_goal_value(goal) for goal in all_goals],
        'Pass/Fail': [get_status(goal) for goal in all_goals]
        })

    df_robust = pd.DataFrame()

    rob_goals = get_robustness_goals()
    rsgs = [x for x in case.TreatmentDelivery.RadiationSetScenarioGroups if x.DiscreteFractionDoseScenarios[0].ForBeamSet.BeamSetIdentifier() == beam_set.BeamSetIdentifier()]

                
    for goal in rob_goals:
        
        for rsg in rsgs:
            for dd in rsg.DiscreteFractionDoseScenarios:
                goal_value,goal_passing = evaluate_goal_on_robustness_scenario(goal,dd)
                # Add data to the robust dataframe
                df_robust = df_robust._append({'Priority': goal.PlanningGoal.Priority, 'ROI/POI': goal.ForRegionOfInterest.Name, 'Clinical goal': get_planning_goal(goal), 'Value': goal_value, 'Pass/Fail': 'Pass' if goal_passing else 'Fail'}, ignore_index=True)

    # Return the nominal and robust dataframes
    return df_nominal, df_robust