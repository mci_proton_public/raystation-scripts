#Created by Lauren Rigsby in august of 2022

import wpf, sys, clr, re
sys.path.append(r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Target Opti Creation')
from System.Windows import *
from System.Windows.Controls import *
from connect import *
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox
from connect import *


clr.AddReference('System.Windows')
from System.Windows.Media import Brushes

try:
    plan = get_current("Plan")
except:
    MessageBox.Show("This script requires a loaded plan, try again")
    quit()
case = get_current("Case")
plan = get_current("Plan")
beam_set = get_current("BeamSet")
structure_set = plan.GetStructureSet()
patient = get_current("Patient")
examination = beam_set.GetPlanningExamination()
exam = examination.Name



targetlist = []
oarlist = []
beamlist = []
class Target_Opti_Creation(Window):
    def __init__(self, patient):
        wpf.LoadComponent(self, r'\\ad\dfs\Shared Data\MCI Radiation Oncology Scripting\RaySearch\Active\raystation-scripts\Target Opti Creation\Target_Opti_Creation.xaml')
        self.Topmost = True
        self.WindowStartupLocation = WindowStartupLocation.CenterScreen
        self.case = case
    # A list of OARs
        self.contours = {}
        self.contours2 = {}
        self.contours3 = {}
        self.crop.Text = str(0)
        for rois in structure_set.RoiGeometries:
            if rois.HasContours() == True and (rois.OfRoi.Type == 'Gtv' or rois.OfRoi.Type == 'Ctv' or rois.OfRoi.Type == 'Ptv'):
                roiname = rois.OfRoi.Name
                self.add_row_target(roiname, patient)
            if rois.HasContours() == True and (rois.OfRoi.Type == 'Organ' or rois.OfRoi.Type == 'Undefined' or rois.OfRoi.Type == 'Control'):
                roiname = rois.OfRoi.Name
                self.add_row_OAR(roiname, patient)


    def CloseWindow(self, sender, event):
        self.DialogResult = False

    #Populate the list of targets in the GUI
    def add_row_target(self, roiname, patient):
        row_target = self.targetGrid.RowDefinitions.Count
        self.targetGrid.RowDefinitions.Add(RowDefinition())
        cb_target = CheckBox()
        cb_target.Margin = Thickness(10,5,5,5)
        cb_target.SetValue(Grid.RowProperty, row_target)
        cb_target.SetValue(Grid.ColumnProperty, 0)
        text = roiname
        cb_target.Content = text
        cb_target.Foreground = Brushes.White;
        self.targetGrid.Children.Add(cb_target)
        self.contours[row_target] = roiname

    #Populate the list of OARs in the GUI
    def add_row_OAR(self, roiname, patient):
        row_OAR = self.OARGrid.RowDefinitions.Count
        self.OARGrid.RowDefinitions.Add(RowDefinition())
        cb_OAR = CheckBox()
        cb_OAR.Margin = Thickness(10,5,5,5)
        cb_OAR.SetValue(Grid.RowProperty, row_OAR)
        cb_OAR.SetValue(Grid.ColumnProperty, 0)
        text2 = roiname
        cb_OAR.Content = text2
        cb_OAR.Foreground = Brushes.White;
        self.OARGrid.Children.Add(cb_OAR)
        self.contours2[row_OAR] = roiname


    def StartAnalysis(self, sender, event):
        self.Remove()
        

    def Remove(self):
        global targetlist
        crop_text = self.crop.Text
        for c in self.targetGrid.Children:
            if c.GetValue(Grid.ColumnProperty) == 0 and c.IsChecked:
                row = c.GetValue(Grid.RowProperty)
                roi = self.contours[row]
                if structure_set.RoiGeometries[roi].HasContours() == False:
                    continue
                targetlist.append(roi)
        global oarlist
        for d in self.OARGrid.Children:
            if d.GetValue(Grid.ColumnProperty) == 0 and d.IsChecked:
                row2 = d.GetValue(Grid.RowProperty)
                roi = self.contours2[row2]
                if structure_set.RoiGeometries[roi].HasContours() == False:
                    continue
                oarlist.append(roi)
        self.DialogResult = False

        def MessageBoxW(hwnd, text, caption, utype):
            result = _MessageBoxW(hwnd, text, caption, utype)
            if not result:
                raise ctypes.WinError(ctypes.get_last_error())
            return result

        crop_num = float(crop_text)/10

        for target in targetlist: 
            
            target_name = str(target)
            opti_target_name_prelim = r"z_"+str(target)+"_opt_"+ crop_text + "mm"          
            rois = [r.OfRoi.Name for r in structure_set.RoiGeometries]            
            if opti_target_name_prelim +"_3" in rois:
                opti_target_name = opti_target_name_prelim +"_4"            
            elif opti_target_name_prelim +"_2" in rois:
                opti_target_name = opti_target_name_prelim +"_3"             
            elif opti_target_name_prelim +"_1" in rois:
                opti_target_name = opti_target_name_prelim +"_2"      
            elif opti_target_name_prelim in rois:
                opti_target_name = opti_target_name_prelim +"_1" 
            else:
                opti_target_name = opti_target_name_prelim
            
            case.PatientModel.CreateRoi(Name=opti_target_name, Color="Blue", Type="Undefined", TissueName=None, RbeCellTypeName=None, RoiMaterial=None)


            case.PatientModel.RegionsOfInterest[opti_target_name].CreateAlgebraGeometry(Examination=examination, Algorithm="Auto",
                                            ExpressionA={ 'Operation': "Union", 'SourceRoiNames': [r"" + target_name + ""], 'MarginSettings': { 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 } },
                                            ExpressionB={ 'Operation': "Union", 'SourceRoiNames': oarlist, 'MarginSettings': { 'Type': "Expand", 'Superior': crop_num, 'Inferior': crop_num, 'Anterior': crop_num, 'Posterior': crop_num, 'Right': crop_num, 'Left': crop_num } },
                                            ResultOperation="Subtraction", ResultMarginSettings={ 'Type': "Expand", 'Superior': 0, 'Inferior': 0, 'Anterior': 0, 'Posterior': 0, 'Right': 0, 'Left': 0 })

            #UpdateDerivedGeometry(Examination=examination, Algorithm="Auto")


