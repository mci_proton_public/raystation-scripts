# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 07:23:17 2023

@author: mau22560
"""

import tkinter as tk
from tkinter import messagebox
from connect import *
import math
import random
import json
from connect import *



def apply_repainting(beam, repainting_type, volumetric_repainting_order, splitting_method, splitting_parameter, min_spot_weight):
    
    if (repainting_type != 'Volumetric repainting' and min_spot_weight == 0 and splitting_method != 'Max MU/spot'):
    # Layer repainting, where SetLayerRepainting-action can be used
        if splitting_method == 'Fixed per plan':
            beam.SetLayerRepainting(FixedNumberOfPaintings = splitting_parameter)
        elif splitting_method == 'Max MU/energy layer':
            maxWeightPerRescan = splitting_parameter
            beam.SetLayerRepainting(MaxMuPerPainting = maxWeightPerRescan * beam.BeamMU)
    else:        
    # In this case first divide the layers and sort them
        paintings_per_spot_and_energy_layer = split_energy_layers(beam, splitting_method, splitting_parameter, min_spot_weight)
        if repainting_type == 'Volumetric repainting':
            add_energy_layers_for_volumetric_repainting(beam, paintings_per_spot_and_energy_layer, volumetric_repainting_order)
        else:
            add_energy_layers_for_layer_repainting_no_dcm_repainting_set(beam, paintings_per_spot_and_energy_layer)
 
    
def add_energy_layers_for_volumetric_repainting(beam, paintings_per_spot_and_energy_layer, volumetric_repainting_order):
    initial_number_of_layers = len(beam.Segments)
    assert(initial_number_of_layers == len(paintings_per_spot_and_energy_layer))
    initial_energy_layers = beam.Segments
    initial_energy_layers_ordered = list(initial_energy_layers)

    maxNumberOfPaintings = max(list([max(paintings_per_spot) for paintings_per_spot in paintings_per_spot_and_energy_layer]))
    remaining_paintings_per_spot_and_energy_layer = list(paintings_per_spot_and_energy_layer)

    for paintingidx in range(1, maxNumberOfPaintings):
        if volumetric_repainting_order == 'Alternating':
                initial_energy_layers_ordered = list(reversed(list(initial_energy_layers_ordered)))
                remaining_paintings_per_spot_and_energy_layer = list(reversed(remaining_paintings_per_spot_and_energy_layer))
                paintings_per_spot_and_energy_layer = list(reversed(paintings_per_spot_and_energy_layer))
            
        for elidx, energy_layer in enumerate(initial_energy_layers_ordered):
            # Proceed to calculate weights for initial energy layer and all repainted layers, i.e. when the number of remaining paintings is larger than 1.
            if max( remaining_paintings_per_spot_and_energy_layer[elidx]) > 1:
                initial_paintings_per_spot = paintings_per_spot_and_energy_layer[elidx]
                weights = [w / initial_paintings_per_spot[i] for i, w in enumerate(energy_layer.Spots.Weights)]
                maxNumberOfPaintingsForLayer = max(initial_paintings_per_spot)
                
                # Find spot indices for repainted layers, i.e. when the number of remaining paintings is larger than 1.                
                spot_indices = [idx for idx, num in enumerate(remaining_paintings_per_spot_and_energy_layer[elidx]) if num > 1]
                spotPositionsX = [pos.x for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
                spotPositionsY = [pos.y for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
                spotWeights = [weights[i] for i in spot_indices]
                beam.AddEnergyLayerWithSpots(Energy = energy_layer.NominalEnergy, SpotPositionsX = spotPositionsX, SpotPositionsY = spotPositionsY, SpotWeights = spotWeights)                
                
                current_layer_idx = len(beam.Segments) - 1 
                current_layer = beam.Segments[current_layer_idx]
                current_layer.RelativeWeight = sum(spotWeights)
                              
                #Remove one painting since it has already been delivered
                remaining_paintings_per_spot_and_energy_layer[elidx] = [num-1 for idx, num in enumerate(remaining_paintings_per_spot_and_energy_layer[elidx])]              
                    
                #Adjust the spot weights of the first energy layer in the last iteration  
                if  paintingidx == maxNumberOfPaintingsForLayer - 1:
                    # print ("Resetting weight of initial layer for segment:")
                    # print  (energy_layer.SegmentNumber)                 
                    energy_layer.Spots.Weights = weights
                    energy_layer.RelativeWeight = sum(weights)

def add_energy_layers_for_layer_repainting_no_dcm_repainting_set(beam, paintings_per_spot_and_energy_layer):
    initial_number_of_layers = len(beam.Segments)
    assert(initial_number_of_layers == len(paintings_per_spot_and_energy_layer))
    initial_energy_layers = beam.Segments
    #with CompositeAction('Apply repainting for beam {0}'.format(beam.Name)): # This line needs to be removed due to a problem with the composite action in 8B and 9A
    for elidx, energy_layer in enumerate(initial_energy_layers):
        paintings_per_spot = paintings_per_spot_and_energy_layer[elidx]
        spot_size_x = energy_layer.Spots.SpotSize.x
        spot_size_y = energy_layer.Spots.SpotSize.y
        
        layers_to_add = max(paintings_per_spot) 
        remaining_paintings_per_spot = paintings_per_spot
        

        totalweight = 0
        layerPaintings = []
        weights = [w / paintings_per_spot[i] for i, w in enumerate(energy_layer.Spots.Weights)]
        for layer_idx in range(layers_to_add):
            spot_indices = [idx for idx, num in enumerate(remaining_paintings_per_spot) if (num > 0)]
            assert(len(spot_indices)>0)
            spotPositionsX = [pos.x for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
            spotPositionsY = [pos.y for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
            spotWeights = [weights[i] for i in spot_indices]
            energyLayer = EnergyLayer(energy_layer.NominalEnergy, spotPositionsX, spotPositionsY, spotWeights)
            
            layerPaintings.append(energyLayer)
            remaining_paintings_per_spot = [num-1 for idx, num in enumerate(remaining_paintings_per_spot)]
            
        #Set spot weights to 0 for initial segment and then delete it
        initial_segment = beam.Segments[0]
        initial_segment.Spots.Weights = [0 for i in initial_segment.Spots.Weights]
        initial_segment.RelativeWeight = 0
        beam.DeleteSegment(SegmentNumber = 0)    
        #Randomize order of layer paintings
        random.shuffle(layerPaintings)
        for layer in layerPaintings:
            beam.AddEnergyLayerWithSpots(Energy = layer.nominalEnergy, SpotPositionsX = layer.spotPositionsX, SpotPositionsY = layer.spotPositionsY, SpotWeights = layer.spotWeights)
            current_layer_idx = len(beam.Segments) - 1 
            current_layer = beam.Segments[current_layer_idx]
            current_layer.RelativeWeight = sum(layer.spotWeights)
            current_layer.Spots.SpotSize = {'x': spot_size_x, 'y': spot_size_y}


# --- Energy layer splitting ---        
def split_energy_layers(beam, splitting_method, splitting_parameter, min_spot_weight):
    paintings_per_spot_and_energy_layer = []
    for layer in beam.Segments:
        if splitting_method == 'Fixed per plan':
            assert(int(splitting_parameter) - splitting_parameter == 0)
            paintings_per_spot_and_energy_layer.append(split_energy_layer_equal(beam, layer, splitting_parameter, min_spot_weight))
        elif splitting_method == 'Max MU/energy layer':
            paintings_per_spot_and_energy_layer.append(split_energy_layer_max_MU_energy_layer(beam, layer, splitting_parameter, min_spot_weight))
        elif splitting_method == 'Max MU/spot':
            paintings_per_spot_and_energy_layer.append(split_energy_layer_max_MU_spot(beam, layer, splitting_parameter, min_spot_weight))
        else:
            print('Not supported repainting method!')
    return paintings_per_spot_and_energy_layer    

def split_energy_layer_equal(beam, layer, splitting_parameter, min_spot_weight):
    number_of_paintings = int(splitting_parameter)
    # Find the maximum number of repaintings for each spot
    paintings_per_spot = [number_of_paintings for i in range(len(layer.Spots.Weights))]
    paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def split_energy_layer_max_MU_energy_layer(beam, layer, splitting_parameter, min_spot_weight):
    # Find the maximum number of repaintings for each spot
    max_MU_per_layer = splitting_parameter
    number_of_paintings = int(math.ceil(layer.RelativeWeight*beam.BeamMU / max_MU_per_layer))
    paintings_per_spot = [number_of_paintings for i in range(len(layer.Spots.Weights))]
    paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def split_energy_layer_max_MU_spot(beam, layer, splitting_parameter, min_spot_weight):
    max_MU_per_spot = splitting_parameter
    assert (max_MU_per_spot >= min_spot_weight)
    paintings_per_spot = [int(math.ceil(w * beam.BeamMU / max_MU_per_spot)) for w in layer.Spots.Weights]
    if  min_spot_weight != 0:
        paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight):
    if min_spot_weight > 0:
        min_rel_weight_per_spot = min_spot_weight / beam.BeamMU
        for idx, weight in enumerate(layer.Spots.Weights):
            max_num_of_paintings = int(math.floor(weight / min_rel_weight_per_spot))
            if max_num_of_paintings < paintings_per_spot[idx]:
                paintings_per_spot[idx] = max(max_num_of_paintings, 1)
    return paintings_per_spot
    
#--- end Energy layer splitting ---

class EnergyLayer:
    def __init__(self, nominalEnergy, spotPositionsX, spotPositionsY, spotWeights):
        self.nominalEnergy = nominalEnergy
        self.spotPositionsX = spotPositionsX
        self.spotPositionsY = spotPositionsY
        self.spotWeights = spotWeights

with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Kristen\field_reg_summary.csv','w') as f:
    f.write("Plan Name_Before,BeamSet Name_Before,Layers_Before,Spots_Before,Total MU_Before,Plan Name_After,BeamSet Name_After,Layers_After,Spots_After,Total MU_After\n")

patient = get_current("Patient")
case = get_current("Case")

all_changes={}
all_plans = [x for x in case.TreatmentPlans]
print(f"Total Number of plans = {len(all_plans)}")
for idx,plan in enumerate(all_plans):
    print(f"Plan {idx} ------ {plan.Name}")
    if plan.Name[:2]=='RS': 
        #Main
        case=get_current('Case').CaseName
        use_repaint = plan.Name.split('_')[-1]!='0'
        
        # Get parameters
        repainting_type = 'Volumetric repainting'
        volumetric_repainting_order = 'Alternating'
        splitting_method = 'Fixed per plan'
        splitting_parameter = int(plan.Name.split('_')[-1])+1
        min_spot_weight = 0
        rep_type_and_order = 'vol alt '
        splitting_method_name = 'fixed '
        
        changes_dict={'Plan Name':{'Before':plan.Name,'After':None},'BeamSet Name':{'Before':None,'After':None},'Total MU':{'Before':None,'After':None},"Layers":{'Before':None,'After':None},"Spots":{'Before':None,'After':None}}
        #Change plan name
        rs,ctv_itv,motion,num_repaint = plan.Name.split('_')
        rep_plan_name = ctv_itv+'_'+motion+'_'+num_repaint+'_'+rs+' Split'
        beam_set_name = ctv_itv+'_'+motion+num_repaint+rs
        #rep_plan_name = plan.Name + ' ' + rep_type_and_order + splitting_method_name + str(splitting_parameter) + ' ' + str(min_spot_weight)
        if len(rep_plan_name) > 62:
             rep_plan_name = rep_plan_name.Substring(0, 61);
        
        



        #Copy plan
        patient.Cases[case].CopyPlan(PlanName = plan.Name, NewPlanName = rep_plan_name)
        patient.Save()
        plan = patient.Cases[case].TreatmentPlans[rep_plan_name]
        plan.SetCurrent()
        changes_dict['Plan Name']['After']=plan.Name
        

        beam_set = get_current('BeamSet')
        changes_dict['BeamSet Name']['Before']=beam_set.DicomPlanLabel
        beam_set.DicomPlanLabel = beam_set_name
        changes_dict['BeamSet Name']['After']=beam_set.DicomPlanLabel

        # Get total MU from beams
        total = 0
        for beam in beam_set.Beams:
            total+=beam.BeamMU
        changes_dict['Total MU']['Before'] = total
        
        #Get num of layers and spots

        num_layers = len(beam_set.Beams[0].Segments)

        num_spots = 0
        for layer in beam_set.Beams[0].Segments:
            num_spots += len(layer.Spots.Weights)

        changes_dict['Layers']['Before'] = num_layers
        changes_dict['Spots']['Before'] = num_spots

        if use_repaint:
            for beam in beam_set.Beams:
                apply_repainting(beam, repainting_type, volumetric_repainting_order, splitting_method, splitting_parameter, min_spot_weight)
            beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False, RunEntryValidation=True)

        #Get num of layers and spots

        num_layers = len(beam_set.Beams[0].Segments)

        num_spots = 0
        for layer in beam_set.Beams[0].Segments:
            num_spots += len(layer.Spots.Weights)

        changes_dict['Layers']['After'] = num_layers
        changes_dict['Spots']['After'] = num_spots


        # Get total MU from beams
        total = 0
        for beam in beam_set.Beams:
            total+=beam.BeamMU
        changes_dict['Total MU']['After'] = total

        with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Kristen\field_reg_summary.csv','a') as f:
            f.write(f"{changes_dict['Plan Name']['Before']},{changes_dict['BeamSet Name']['Before']},{changes_dict['Layers']['Before']},{changes_dict['Spots']['Before']},{changes_dict['Total MU']['Before']},{changes_dict['Plan Name']['After']},{changes_dict['BeamSet Name']['After']},{changes_dict['Layers']['After']},{changes_dict['Spots']['After']},{changes_dict['Total MU']['After']}\n")
        all_changes[idx] = changes_dict

with open(r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Kristen\field_reg_summary.json','w') as f:
    f.write(json.dumps(all_changes, indent=4))