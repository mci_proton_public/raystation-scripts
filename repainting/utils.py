from connect import get_current
def apply_repainting(beam_name,number_deliveries):
    beam_set = get_current("BeamSet")
    beam = beam_set.Beams[beam_name]
    # Get list of energy layers
    initial_energy_layers = beam.Segments
    initial_energy_layers_ordered = list(initial_energy_layers)

    number_repaints_left = number_deliveries - 1
    
    while number_repaints_left > 0:
   
        number_repaints_left -= 1
        # For each repaint, we need to reverse the order of the energy layers since it's alternating
        initial_energy_layers_ordered = list(reversed(list(initial_energy_layers_ordered)))
        
        for elidx, energy_layer in enumerate(initial_energy_layers_ordered):
            # Proceed to calculate weights for initial energy layer and all repainted layers, i.e. when the number of remaining paintings is larger than 1.              
                
            spotPositionsX = [pos.x for pos in energy_layer.Spots.Positions]
            spotPositionsY = [pos.y for pos in energy_layer.Spots.Positions]
            spotWeights = [w / number_deliveries for w in energy_layer.Spots.Weights]
                
            #Add new layer
            beam.AddEnergyLayerWithSpots(Energy = energy_layer.NominalEnergy, SpotPositionsX = spotPositionsX, SpotPositionsY = spotPositionsY, SpotWeights = spotWeights)                
                
            # Grab the new layer so that you can set the Relative Weight
            current_layer_idx = len(beam.Segments) - 1 
            current_layer = beam.Segments[current_layer_idx]
            current_layer.RelativeWeight = sum(spotWeights)
                              
         
            # Once the repaints are all done, then we need to set the weights and relative weight of the original layers  
            if  number_repaints_left == 0:
                print (f"Resetting weight of original layer # {energy_layer.SegmentNumber}")
                energy_layer.Spots.Weights = spotWeights
                energy_layer.RelativeWeight = sum(spotWeights)

def apply_repainting_wrapper(args):
    return apply_repainting(*args)
