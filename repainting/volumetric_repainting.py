from ast import Num
import numbers
from connect import *
import clr
clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Drawing")
from System.Windows.Forms import Application, Form, Label, ComboBox, Button, TextBox, MessageBox, MessageBoxButtons, MessageBoxIcon
from System.Drawing import Point, Size
import time



# Define Forms class that will prompt the user to select an
# ROI for creating isocenter data for the current beam set
class VolumetericRepainting(Form):

    def __init__(self):
        # Set the size of the form
        self.Size = Size(300, 200)
        # Set title of the form
        self.Text = 'Volumetric Repaiting'
        # Add a label
        label = Label()
        label.Text = 'Please input the total number of deliveries'
        label.Location = Point(15, 15)
        label.AutoSize = True
        self.Controls.Add(label)
        
        self.textbox = TextBox()
        self.textbox.Location = Point(15, 60)
        self.textbox.AutoSize = True
        self.Controls.Add(self.textbox)
        
        # Add button to press OK and close the form
        button = Button()
        button.Text = 'OK'
        button.AutoSize = True
        button.Location = Point(15, 100)
        button.Click += self.ok_button_clicked
        self.Controls.Add(button)

    def ok_button_clicked(self, sender, event):
        # Method invoked when the button is clicked
        # Save the selected ROI name
        self.num_deliveries = self.textbox.Text
        # Close the form
        self.Close()


def get_min_mu(beam_set):
    #get min,max of each beam in beamset
    min_mu_dict = {"value":1e8,"beam":None,"energy":None}
    for beam in beam_set.Beams:   #Iterate through each beam
        bm_wt = beam.BeamMU   
        for layer in beam.Segments:   #Iterate through each energy layer
            beam_energy = layer.NominalEnergy
            wts = layer.Spots.Weights * bm_wt   #Get the spot weights in MU/fx
            min_mu = np.min(wts)
            if min_mu<min_mu_dict['value']:
                min_mu_dict['value'] = min_mu
                min_mu_dict['beam'] = beam.Name
                min_mu_dict['energy'] = beam_energy
    return min_mu_dict
                

def apply_repainting(beam,number_deliveries):

    # Get list of energy layers
    initial_energy_layers = beam.Segments
    initial_energy_layers_ordered = list(initial_energy_layers)

    number_repaints_left = number_deliveries - 1
    
    while number_repaints_left > 0:
   
        number_repaints_left -= 1
        # For each repaint, we need to reverse the order of the energy layers since it's alternating
        initial_energy_layers_ordered = list(reversed(list(initial_energy_layers_ordered)))
        
        for elidx, energy_layer in enumerate(initial_energy_layers_ordered):
            # Proceed to calculate weights for initial energy layer and all repainted layers, i.e. when the number of remaining paintings is larger than 1.              
                
            spotPositionsX = [pos.x for pos in energy_layer.Spots.Positions]
            spotPositionsY = [pos.y for pos in energy_layer.Spots.Positions]
            spotWeights = [w / number_deliveries for w in energy_layer.Spots.Weights]
                
            #Add new layer
            beam.AddEnergyLayerWithSpots(Energy = energy_layer.NominalEnergy, SpotPositionsX = spotPositionsX, SpotPositionsY = spotPositionsY, SpotWeights = spotWeights)                
                
            # Grab the new layer so that you can set the Relative Weight
            current_layer_idx = len(beam.Segments) - 1 
            current_layer = beam.Segments[current_layer_idx]
            current_layer.RelativeWeight = sum(spotWeights)
                              
         
            # Once the repaints are all done, then we need to set the weights and relative weight of the original layers  
            if  number_repaints_left == 0:
                print (f"Resetting weight of original layer # {energy_layer.SegmentNumber}")
                energy_layer.Spots.Weights = spotWeights
                energy_layer.RelativeWeight = sum(spotWeights)

 


if __name__ == '__main__':
    try:
        plan = get_current('Plan')
        patient = get_current('Patient')
        case=get_current('Case').CaseName
        beam_set = get_current('BeamSet')

        min_accepted_mu = .015 #.0075
        form = VolumetericRepainting()
        Application.Run(form)
        num_deliveries = form.num_deliveries
    
        assert(int(num_deliveries) - float(num_deliveries) == 0)
        num_deliveries = int(num_deliveries)
    
        # Get min_mu
        min_mu = get_min_mu(beam_set)
    
        # Check that min divided by num_deliveries >= min_accepted_mu
        if min_mu['value']/num_deliveries<min_accepted_mu:
            MessageBox.Show(f"The min_mu after splitting {num_deliveries} will be less than the allowed minimum {min_accepted_mu}. Program will now exit")
        else:
            # In this case first divide the layers and sort them
        
            rep_plan_name = plan.Name +"_Split" # +f"_{num_deliveries -1}RP_Split" 
            if len(rep_plan_name) > 62:
                    rep_plan_name = rep_plan_name[:61];
            #Copy plan
            patient.Cases[case].CopyPlan(PlanName = plan.Name, NewPlanName = rep_plan_name)
            patient.Save()
            plan = patient.Cases[case].TreatmentPlans[rep_plan_name]
            plan.SetCurrent()
        

            beam_set = get_current('BeamSet')
            #Disable scale to prescription
            beam_set.SetAutoScaleToPrimaryPrescription(AutoScale=False)
        
            import time
            # Start the timer
            start_time = time.time()
        
            for beam in beam_set.Beams:
                print('------------------------------')
                print(f"Repaiting for BEAM ----- {beam.Name}")
                apply_repainting(beam,num_deliveries)
        
            # patient.Save()
            beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo",ForceRecompute=False,RunEntryValidation=True)
            patient.Save()
            # Stop the timer
            end_time = time.time()

            # Calculate and print the elapsed time
            elapsed_time = end_time - start_time
            print(f"The code took {elapsed_time} seconds to run.")    
            
            

    except Exception as e:
        MessageBox.Show("An error has occurred\n" + str(e), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


