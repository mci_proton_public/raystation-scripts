from connect import *
import time
import numpy as np
import clr
clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Drawing")
from System.Windows.Forms import Application, Form, Label, ComboBox, Button, TextBox, MessageBox, MessageBoxButtons, MessageBoxIcon
from System.Drawing import Point, Size
import traceback



def get_min_mu(beam_set):
    #get min,max of each beam in beamset
    min_mu_dict = {"value":1e8,"beam":None,"energy":None}
    for beam in beam_set.Beams:   #Iterate through each beam
        bm_wt = beam.BeamMU   
        for layer in beam.Segments:   #Iterate through each energy layer
            beam_energy = layer.NominalEnergy
            wts = layer.Spots.Weights * bm_wt   #Get the spot weights in MU/fx
            min_mu = np.min(wts)
            if min_mu<min_mu_dict['value']:
                min_mu_dict['value'] = min_mu
                min_mu_dict['beam'] = beam.Name
                min_mu_dict['energy'] = beam_energy
    return min_mu_dict
                

def apply_repainting(beam,number_deliveries):

    # Get list of energy layers
    initial_energy_layers = beam.Segments
    initial_energy_layers_ordered = list(initial_energy_layers)

    number_repaints_left = number_deliveries - 1
    
    while number_repaints_left > 0:
   
        number_repaints_left -= 1
        # For each repaint, we need to reverse the order of the energy layers since it's alternating
        initial_energy_layers_ordered = list(reversed(list(initial_energy_layers_ordered)))
        
        for elidx, energy_layer in enumerate(initial_energy_layers_ordered):
            # Proceed to calculate weights for initial energy layer and all repainted layers, i.e. when the number of remaining paintings is larger than 1.              
                
            spotPositionsX = [pos.x for pos in energy_layer.Spots.Positions]
            spotPositionsY = [pos.y for pos in energy_layer.Spots.Positions]
            spotWeights = [w / number_deliveries for w in energy_layer.Spots.Weights]
                
            #Add new layer
            beam.AddEnergyLayerWithSpots(Energy = energy_layer.NominalEnergy, SpotPositionsX = spotPositionsX, SpotPositionsY = spotPositionsY, SpotWeights = spotWeights)                
                
            # Grab the new layer so that you can set the Relative Weight
            current_layer_idx = len(beam.Segments) - 1 
            current_layer = beam.Segments[current_layer_idx]
            current_layer.RelativeWeight = sum(spotWeights)
                              
         
            # Once the repaints are all done, then we need to set the weights and relative weight of the original layers  
            if  number_repaints_left == 0:
                print (f"Resetting weight of original layer # {energy_layer.SegmentNumber}")
                energy_layer.Spots.Weights = spotWeights
                energy_layer.RelativeWeight = sum(spotWeights)

 


if __name__ == '__main__':
    try:
        START = time.time()
        patient = get_current('Patient')
        actual_case = get_current('Case')
        case=get_current('Case').CaseName
        

        #mod_plans = ['03cm_00mm_1RP_RS', '03cm_00mm_5RP_RS', '03cm_05mm_5RP_RS', '03cm_08mm_1RP_RS', '03cm_08mm_5RP_RS', '03cm_10mm_3RP_RS', '02cm_05mm_1RP_RS', '02cm_05mm_5RP_RS', '03cm_15mm_1RP_RS', '03cm_15mm_3RP_RS', '05cm_00mm_5RP_RS', '02cm_08mm_3RP_RS', '02cm_08mm_5RP_RS', '02cm_15mm_3RP_RS', '02cm_00mm_3RP_RS']


        min_accepted_mu = .015 #.0075
        
        all_plans = [x for x in actual_case.TreatmentPlans] # if x.Name in mod_plans]
        num_plans = len(all_plans)
        
        for idx,plan in enumerate(all_plans):
            set_progress(f"Splitting {plan.Name} [{idx+1}/{num_plans}]",int(idx/num_plans*100))
            
            if plan.Name[-3:] != "_RS":
                num_deliveries = int(plan.Name.split('_')[-1].split('RP')[0]) + 1 # form.num_deliveries
            else:
                num_deliveries = int(plan.Name.split('_')[-2].split('RP')[0]) + 1
            beam_set = plan.BeamSets[0]
    
            assert(int(num_deliveries) - float(num_deliveries) == 0)
            num_deliveries = int(num_deliveries)
    
            # Get min_mu
            min_mu = get_min_mu(beam_set)
    
            # Check that min divided by num_deliveries >= min_accepted_mu
            if min_mu['value']/num_deliveries<min_accepted_mu:
                MessageBox.Show(f"The min_mu after splitting {num_deliveries} will be less than the allowed minimum {min_accepted_mu}. Program will now exit")
            else:
                # In this case first divide the layers and sort them
        
                rep_plan_name = plan.Name +"_Split" # +f"_{num_deliveries -1}RP_Split" 
                if len(rep_plan_name) > 62:
                        rep_plan_name = rep_plan_name[:61];
                #Copy plan
                patient.Cases[case].CopyPlan(PlanName = plan.Name, NewPlanName = rep_plan_name)
                patient.Save()
                plan = patient.Cases[case].TreatmentPlans[rep_plan_name]
                plan.SetCurrent()
        

                beam_set = get_current('BeamSet')
                
                # Disable scale to prescription
                beam_set.SetAutoScaleToPrimaryPrescription(AutoScale=False)
        
                
                # Start the timer
                start_time = time.time()
                
                total_beams = len(beam_set.Beams)
                for ib,beam in enumerate(beam_set.Beams):
                    set_progress(f"Splitting {plan.Name} [{idx+1}/{num_plans}] - Beam: {beam.Name} [{ib+1}/{total_beams}] ",int(idx/num_plans*100) + (ib/total_beams))
                    apply_repainting(beam,num_deliveries)
        
                # patient.Save()
                if num_deliveries != 1:    # Should only do a final dose if something changed. For single delivery, this has already been calculated in the OG plan.
                    beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo",ForceRecompute=False,RunEntryValidation=True)
                patient.Save()
                # Stop the timer
                end_time = time.time()

                # Calculate and print the elapsed time
                elapsed_time = end_time - start_time
                print(f"The code took {elapsed_time} seconds to run.")
        
        END = time.time()
        MessageBox.Show(f"Complete. Run Time = {(END - START)/60:.1f} minutes", "Done")
            
            

    except Exception as e:
        tb = traceback.format_exc()
        MessageBox.Show("An error has occurred\n" + tb, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


