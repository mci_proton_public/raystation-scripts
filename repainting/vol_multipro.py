from ast import Num
from connect import *
import clr
clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Drawing")
from System.Windows.Forms import Application, Form, Label, ComboBox, Button, TextBox, MessageBox, MessageBoxButtons, MessageBoxIcon
from System.Drawing import Point, Size
import time
from multiprocessing import Pool
from utils import *


# Define Forms class that will prompt the user to select an
# ROI for creating isocenter data for the current beam set
class VolumetericRepainting(Form):

    def __init__(self):
        # Set the size of the form
        self.Size = Size(300, 200)
        # Set title of the form
        self.Text = 'Volumetric Repaiting'
        # Add a label
        label = Label()
        label.Text = 'Please input the toal number of deliveries'
        label.Location = Point(15, 15)
        label.AutoSize = True
        self.Controls.Add(label)
        
        self.textbox = TextBox()
        self.textbox.Location = Point(15, 60)
        self.textbox.AutoSize = True
        self.Controls.Add(self.textbox)
        
        # Add button to press OK and close the form
        button = Button()
        button.Text = 'OK'
        button.AutoSize = True
        button.Location = Point(15, 100)
        button.Click += self.ok_button_clicked
        self.Controls.Add(button)

    def ok_button_clicked(self, sender, event):
        # Method invoked when the button is clicked
        # Save the selected ROI name
        self.num_deliveries = self.textbox.Text
        # Close the form
        self.Close()


def get_min_mu(beam_set):
    #get min,max of each beam in beamset
    min_mu_dict = {"value":1e8,"beam":None,"energy":None}
    for beam in beam_set.Beams:   #Iterate through each beam
        bm_wt = beam.BeamMU   
        for layer in beam.Segments:   #Iterate through each energy layer
            beam_energy = layer.NominalEnergy
            wts = layer.Spots.Weights * bm_wt   #Get the spot weights in MU/fx
            min_mu = np.min(wts)
            if min_mu<min_mu_dict['value']:
                min_mu_dict['value'] = min_mu
                min_mu_dict['beam'] = beam.Name
                min_mu_dict['energy'] = beam_energy
    return min_mu_dict

if __name__ == '__main__':
    try:
        plan = get_current('Plan')
        patient = get_current('Patient')
        case=get_current('Case').CaseName
        beam_set = get_current('BeamSet')

        min_accepted_mu = .0075 #.015
        form = VolumetericRepainting()
        Application.Run(form)
        num_deliveries = form.num_deliveries
    
        assert(int(num_deliveries) - float(num_deliveries) == 0)
        num_deliveries = int(num_deliveries)
    
        # Get min_mu
        min_mu = get_min_mu(beam_set)
    
        # Check that min divided by num_deliveries >= min_accepted_mu
        if min_mu['value']/num_deliveries<min_accepted_mu:
            MessageBox.Show(f"The min_mu after splitting {num_deliveries} will be less than the allowed minimum {min_accepted_mu}. Program will now exit")
        else:
            # In this case first divide the layers and sort them
        
            rep_plan_name = plan.Name + ' ' +f"RP_{num_deliveries}" 
            if len(rep_plan_name) > 62:
                    rep_plan_name = rep_plan_name[:61];
            #Copy plan
            patient.Cases[case].CopyPlan(PlanName = plan.Name, NewPlanName = rep_plan_name)
            patient.Save()
            plan = patient.Cases[case].TreatmentPlans[rep_plan_name]
            plan.SetCurrent()
        

            beam_set = get_current('BeamSet')
        
            import time
            # Start the timer
            start_time = time.time()
            args = [(beam.Name, num_deliveries) for beam in beam_set.Beams]
            with Pool() as p:
                p.map(apply_repainting_wrapper, args)
            
            # patient.Save()
            # Stop the timer
            end_time = time.time()
            # Calculate and print the elapsed time
            elapsed_time = end_time - start_time
            print(f"The code took {elapsed_time} seconds to run.") 
            
            

    except Exception as e:
        MessageBox.Show("An error has occurred\n" + str(e), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)



