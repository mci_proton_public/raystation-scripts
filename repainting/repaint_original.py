set_repainting_gui = '﻿<Window xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"\n        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"\n        Title="Set repainting"\n        Width="400"\n        SizeToContent="Height">\n    <Grid>\n        <Grid.ColumnDefinitions>\n            <ColumnDefinition />\n            <ColumnDefinition />\n        </Grid.ColumnDefinitions>\n        <Grid.RowDefinitions>\n            <RowDefinition />\n            <RowDefinition />\n            <RowDefinition />\n            <RowDefinition />\n            <RowDefinition />\n            <RowDefinition />\n        </Grid.RowDefinitions>\n        <TextBlock Grid.Row="0"\n                   Grid.Column="0"\n                   Margin="5"\n                   VerticalAlignment="Center"\n                   Text="Repainting type:" />\n        <ComboBox Name="RepaintingTypeComboBox"\n                  Grid.Row="0"\n                  Grid.Column="1"\n                  Margin="5"\n                  SelectionChanged="repainting_type_selection_changed" />\n        <TextBlock Name="VolumetricRepaintingOrderTextBlock"\n                   Grid.Row="1"\n                   Grid.Column="0"\n                   Margin="5"\n                   VerticalAlignment="Center"\n                   Text="Volumetric repainting order:"\n                   Visibility="Collapsed" />\n        <ComboBox Name="VolumetricRepaintingOrderComboBox"\n                  Grid.Row="1"\n                  Grid.Column="1"\n                  Margin="5"\n                  Visibility="Collapsed" />\n        <TextBlock Grid.Row="2"\n                   Grid.Column="0"\n                   Margin="5"\n                   VerticalAlignment="Center"\n                   Text="Splitting method:" />\n        <ComboBox Name="SplittingMethodComboBox"\n                  Grid.Row="2"\n                  Grid.Column="1"\n                  Margin="5"\n                  SelectionChanged="splitting_method_selection_changed" />\n        <TextBlock Grid.Row="3"\n                   Grid.Column="0"\n                   Margin="5"\n                   VerticalAlignment="Center"\n                   Text="Splitting parameter:" />\n        <TextBox Name="SplittingParameterTextBox"\n                 Grid.Row="3"\n                 Grid.Column="1"\n                 Width="50"\n                 Margin="5"\n                 HorizontalAlignment="Left" />\n        <TextBlock Name="SplittingParameterTextBlock"\n                   Grid.Row="3"\n                   Grid.Column="1"\n                   Margin="60,5,5,5" />\n        <RadioButton Name="UseMinSpotWeightRadioButton"\n                     Grid.Row="4"\n                     Grid.Column="0"\n                     Margin="5"\n                     VerticalAlignment="Center"\n                     Content="Use min spot weight" />\n        <TextBox Name="MinSpotWeightTextBox"\n                 Grid.Row="4"\n                 Grid.Column="1"\n                 Width="50"\n                 Margin="5"\n                 HorizontalAlignment="Left" />\n        <Button Grid.Row="5"\n                Grid.ColumnSpan="2"\n                Width="100"\n                Margin="5"\n                Click="ok_button_clicked"\n                Content="OK" />\n    </Grid>\n</Window>'
with open('set_repainting_gui.xaml','w') as f:
    f.write(set_repainting_gui)



from connect import *
import math
import wpf
import random

from System.Windows import Application, Window, Visibility, MessageBox

class MyWindow(Window):
    def __init__(self, min_mu):
        #os.chdir(r'C:\script_files\repainting')
        wpf.LoadComponent(self, 'set_repainting_gui.xaml')
        self.RepaintingTypeComboBox.ItemsSource = ['Layer repainting', 'Volumetric repainting']
        self.RepaintingTypeComboBox.SelectedIndex = 0
        self.VolumetricRepaintingOrderComboBox.ItemsSource = ['Back to front', 'Alternating']
        self.VolumetricRepaintingOrderComboBox.SelectedIndex = 0
        self.SplittingMethodComboBox.ItemsSource = ['Fixed per plan', 'Max MU/energy layer', 'Max MU/spot']
        self.SplittingMethodComboBox.SelectedIndex = 0
        self.MinSpotWeightTextBox.Text = str(min_mu)

    def repainting_type_selection_changed(self, sender, event):
        if sender.SelectedItem == 'Volumetric repainting':
            self.VolumetricRepaintingOrderComboBox.Visibility = Visibility.Visible
            self.VolumetricRepaintingOrderTextBlock.Visibility = Visibility.Visible
        else:
            self.VolumetricRepaintingOrderComboBox.Visibility = Visibility.Collapsed
            self.VolumetricRepaintingOrderTextBlock.Visibility = Visibility.Collapsed
    
    def splitting_method_selection_changed(self, sender, event):
        if sender.SelectedItem == 'Fixed per plan':
            self.SplittingParameterTextBlock.Text = 'fixed no. paintings'
        elif sender.SelectedItem == 'Max MU/energy layer':
            self.SplittingParameterTextBlock.Text = 'max MU/energy layer'
        elif sender.SelectedItem == 'Max MU/spot':
            self.SplittingParameterTextBlock.Text = 'max MU/spot'

    def ok_button_clicked(self, sender, event):
        self.repainting_type = self.RepaintingTypeComboBox.SelectedItem
        self.volumetric_repainting_order = self.VolumetricRepaintingOrderComboBox.SelectedItem
        self.splitting_method = self.SplittingMethodComboBox.SelectedItem
        try:
            self.splitting_parameter = float(self.SplittingParameterTextBox.Text)
        except:
            MessageBox.Show('Invalid input for splitting parameter')
            return
        if self.UseMinSpotWeightRadioButton.IsChecked:
            try:
                self.min_spot_weight = float(self.MinSpotWeightTextBox.Text)
            except:
                MessageBox.Show('Invalid input for min spot weight')
                return
        else:
            self.min_spot_weight = 0
        self.DialogResult = True

def apply_repainting(beam, repainting_type, volumetric_repainting_order, splitting_method, splitting_parameter, min_spot_weight):
    
    if (repainting_type != 'Volumetric repainting' and min_spot_weight == 0 and splitting_method != 'Max MU/spot'):
    # Layer repainting, where SetLayerRepainting-action can be used
        if splitting_method == 'Fixed per plan':
            beam.SetLayerRepainting(FixedNumberOfPaintings = splitting_parameter)
        elif splitting_method == 'Max MU/energy layer':
            maxWeightPerRescan = splitting_parameter
            beam.SetLayerRepainting(MaxMuPerPainting = maxWeightPerRescan * beam.BeamMU)
    else:        
    # In this case first divide the layers and sort them
        paintings_per_spot_and_energy_layer = split_energy_layers(beam, splitting_method, splitting_parameter, min_spot_weight)
        if repainting_type == 'Volumetric repainting':
            add_energy_layers_for_volumetric_repainting(beam, paintings_per_spot_and_energy_layer, volumetric_repainting_order)
        else:
            add_energy_layers_for_layer_repainting_no_dcm_repainting_set(beam, paintings_per_spot_and_energy_layer)
 
	
def add_energy_layers_for_volumetric_repainting(beam, paintings_per_spot_and_energy_layer, volumetric_repainting_order):
    initial_number_of_layers = beam.Segments.Count
    assert(initial_number_of_layers == len(paintings_per_spot_and_energy_layer))
    initial_energy_layers = beam.Segments
    initial_energy_layers_ordered = list(initial_energy_layers)

    maxNumberOfPaintings = max(list([max(paintings_per_spot) for paintings_per_spot in paintings_per_spot_and_energy_layer]))
    remaining_paintings_per_spot_and_energy_layer = list(paintings_per_spot_and_energy_layer)

    for paintingidx in range(1, maxNumberOfPaintings):
        if volumetric_repainting_order == 'Alternating':
                initial_energy_layers_ordered = list(reversed(list(initial_energy_layers_ordered)))
                remaining_paintings_per_spot_and_energy_layer = list(reversed(remaining_paintings_per_spot_and_energy_layer))
                paintings_per_spot_and_energy_layer = list(reversed(paintings_per_spot_and_energy_layer))
            
        for elidx, energy_layer in enumerate(initial_energy_layers_ordered):
            # Proceed to calculate weights for initial energy layer and all repainted layers, i.e. when the number of remaining paintings is larger than 1.
            if max( remaining_paintings_per_spot_and_energy_layer[elidx]) > 1:
                initial_paintings_per_spot = paintings_per_spot_and_energy_layer[elidx]
                weights = [w / initial_paintings_per_spot[i] for i, w in enumerate(energy_layer.Spots.Weights)]
                maxNumberOfPaintingsForLayer = max(initial_paintings_per_spot)
                
                # Find spot indices for repainted layers, i.e. when the number of remaining paintings is larger than 1.                
                spot_indices = [idx for idx, num in enumerate(remaining_paintings_per_spot_and_energy_layer[elidx]) if num > 1]
                spotPositionsX = [pos.x for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
                spotPositionsY = [pos.y for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
                spotWeights = [weights[i] for i in spot_indices]
                beam.AddEnergyLayerWithSpots(Energy = energy_layer.NominalEnergy, SpotPositionsX = spotPositionsX, SpotPositionsY = spotPositionsY, SpotWeights = spotWeights)                
                
                current_layer_idx = beam.Segments.Count - 1 
                current_layer = beam.Segments[current_layer_idx]
                current_layer.RelativeWeight = sum(spotWeights)
                              
                #Remove one painting since it has already been delivered
                remaining_paintings_per_spot_and_energy_layer[elidx] = [num-1 for idx, num in enumerate(remaining_paintings_per_spot_and_energy_layer[elidx])]              
                    
                #Adjust the spot weights of the first energy layer in the last iteration  
                if  paintingidx == maxNumberOfPaintingsForLayer - 1:
                    print "Resetting weight of initial layer for segment:"
                    print  energy_layer.SegmentNumber                 
                    energy_layer.Spots.Weights = weights
                    energy_layer.RelativeWeight = sum(weights)

def add_energy_layers_for_layer_repainting_no_dcm_repainting_set(beam, paintings_per_spot_and_energy_layer):
    initial_number_of_layers = beam.Segments.Count
    assert(initial_number_of_layers == len(paintings_per_spot_and_energy_layer))
    initial_energy_layers = beam.Segments
    #with CompositeAction('Apply repainting for beam {0}'.format(beam.Name)): # This line needs to be removed due to a problem with the composite action in 8B and 9A
    for elidx, energy_layer in enumerate(initial_energy_layers):
        paintings_per_spot = paintings_per_spot_and_energy_layer[elidx]
        spot_size_x = energy_layer.Spots.SpotSize.x
        spot_size_y = energy_layer.Spots.SpotSize.y
        
        layers_to_add = max(paintings_per_spot) 
        remaining_paintings_per_spot = paintings_per_spot
        

        totalweight = 0
        layerPaintings = []
        weights = [w / paintings_per_spot[i] for i, w in enumerate(energy_layer.Spots.Weights)]
        for layer_idx in range(layers_to_add):
            spot_indices = [idx for idx, num in enumerate(remaining_paintings_per_spot) if (num > 0)]
            assert(len(spot_indices)>0)
            spotPositionsX = [pos.x for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
            spotPositionsY = [pos.y for idx, pos in enumerate(energy_layer.Spots.Positions) if idx in spot_indices]
            spotWeights = [weights[i] for i in spot_indices]
            energyLayer = EnergyLayer(energy_layer.NominalEnergy, spotPositionsX, spotPositionsY, spotWeights)
            
            layerPaintings.append(energyLayer)
            remaining_paintings_per_spot = [num-1 for idx, num in enumerate(remaining_paintings_per_spot)]
            
        #Set spot weights to 0 for initial segment and then delete it
        initial_segment = beam.Segments[0]
        initial_segment.Spots.Weights = [0 for i in initial_segment.Spots.Weights]
        initial_segment.RelativeWeight = 0
        beam.DeleteSegment(SegmentNumber = 0)    
        #Randomize order of layer paintings
        random.shuffle(layerPaintings)
        for layer in layerPaintings:
            beam.AddEnergyLayerWithSpots(Energy = layer.nominalEnergy, SpotPositionsX = layer.spotPositionsX, SpotPositionsY = layer.spotPositionsY, SpotWeights = layer.spotWeights)
            current_layer_idx = beam.Segments.Count - 1 
            current_layer = beam.Segments[current_layer_idx]
            current_layer.RelativeWeight = sum(layer.spotWeights)
            current_layer.Spots.SpotSize = {'x': spot_size_x, 'y': spot_size_y}


# --- Energy layer splitting ---        
def split_energy_layers(beam, splitting_method, splitting_parameter, min_spot_weight):
    paintings_per_spot_and_energy_layer = []
    for layer in beam.Segments:
        if splitting_method == 'Fixed per plan':
            assert(int(splitting_parameter) - splitting_parameter == 0)
            paintings_per_spot_and_energy_layer.append(split_energy_layer_equal(beam, layer, splitting_parameter, min_spot_weight))
        elif splitting_method == 'Max MU/energy layer':
            paintings_per_spot_and_energy_layer.append(split_energy_layer_max_MU_energy_layer(beam, layer, splitting_parameter, min_spot_weight))
        elif splitting_method == 'Max MU/spot':
            paintings_per_spot_and_energy_layer.append(split_energy_layer_max_MU_spot(beam, layer, splitting_parameter, min_spot_weight))
        else:
            print('Not supported repainting method!')
    return paintings_per_spot_and_energy_layer    

def split_energy_layer_equal(beam, layer, splitting_parameter, min_spot_weight):
    number_of_paintings = int(splitting_parameter)
    # Find the maximum number of repaintings for each spot
    paintings_per_spot = [number_of_paintings for i in range(layer.Spots.Weights.Count)]
    paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def split_energy_layer_max_MU_energy_layer(beam, layer, splitting_parameter, min_spot_weight):
    # Find the maximum number of repaintings for each spot
    max_MU_per_layer = splitting_parameter
    number_of_paintings = int(math.ceil(layer.RelativeWeight*beam.BeamMU / max_MU_per_layer))
    paintings_per_spot = [number_of_paintings for i in range(layer.Spots.Weights.Count)]
    paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def split_energy_layer_max_MU_spot(beam, layer, splitting_parameter, min_spot_weight):
    max_MU_per_spot = splitting_parameter
    assert (max_MU_per_spot >= min_spot_weight)
    paintings_per_spot = [int(math.ceil(w * beam.BeamMU / max_MU_per_spot)) for w in layer.Spots.Weights]
    if  min_spot_weight != 0:
        paintings_per_spot = include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight)
    return paintings_per_spot
    
def include_min_spot_weight_in_paintings_per_spot(layer, paintings_per_spot, min_spot_weight):
    if min_spot_weight > 0:
        min_rel_weight_per_spot = min_spot_weight / beam.BeamMU
        for idx, weight in enumerate(layer.Spots.Weights):
            max_num_of_paintings = int(math.floor(weight / min_rel_weight_per_spot))
            if max_num_of_paintings < paintings_per_spot[idx]:
                paintings_per_spot[idx] = max(max_num_of_paintings, 1)
    return paintings_per_spot
    
#--- end Energy layer splitting ---

class EnergyLayer:
    def __init__(self, nominalEnergy, spotPositionsX, spotPositionsY, spotWeights):
        self.nominalEnergy = nominalEnergy
        self.spotPositionsX = spotPositionsX
        self.spotPositionsY = spotPositionsY
        self.spotWeights = spotWeights

        

#Main
plan = get_current('Plan')
patient = get_current('Patient')

min_spot_weight = plan.PlanOptimizations[0].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimits.x
if abs(min_spot_weight) < 1e-9:
    print "Min MU per spot must be larger than zero."
else:
    window = MyWindow(min_spot_weight)
    window.ShowDialog()
    if window.DialogResult:
        # Get parameters
        repainting_type = window.repainting_type
        volumetric_repainting_order = window.volumetric_repainting_order
        splitting_method = window.splitting_method
        splitting_parameter = window.splitting_parameter
        min_spot_weight = window.min_spot_weight
        #Repainted plan name
        if repainting_type == 'Volumetric repainting':
            if volumetric_repainting_order == 'Alternating':
                rep_type_and_order = 'vol alt '
            else:
                rep_type_and_order = 'vol bf '        
        else:
            rep_type_and_order = 'layer'
        if splitting_method == 'Fixed per plan':
            splitting_method_name = 'fixed '
        elif splitting_method == 'Max MU/energy layer':
            splitting_method_name = 'MU/layer '
        elif splitting_method == 'Max MU/spot':
            splitting_method_name = 'MU/spot '
            
        rep_plan_name = plan.Name + ' ' + rep_type_and_order + splitting_method_name + str(splitting_parameter) + ' ' + str(min_spot_weight)
        if len(rep_plan_name) > 62:
             rep_plan_name = rep_plan_name.Substring(0, 61);
        #Copy plan
        patient.Cases[0].CopyPlan(PlanName = plan.Name, NewPlanName = rep_plan_name)
        patient.Save()
        plan = patient.Cases[0].TreatmentPlans[rep_plan_name]
        plan.SetCurrent()
        

        beam_set = get_current('BeamSet')
        
        
        for beam in beam_set.Beams:
            apply_repainting(beam, repainting_type, volumetric_repainting_order, splitting_method, splitting_parameter, min_spot_weight)



