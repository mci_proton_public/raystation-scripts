"""
Original from RayStation
Modified by FC

6/22/2022: Modified by Lauren Rigsby for the purpose of making a gantry change script for MCI.


"""

import sys
import os
import clr
import wpf

clr.AddReference('System')
clr.AddReference("System.Xml")
import System

from random import choice
from string import ascii_uppercase
from System.Windows import Window
from System.Windows import *
from System.Collections.ObjectModel import ObservableCollection
from System.Windows.Controls import ComboBox, ComboBoxItem
from System.IO import StringReader
from System.Xml import XmlReader

clr.AddReference('ScriptClient.dll')

from connect import *


class MyWindow(Window):

    def __init__(self):
        xaml = \
            """
<Window
       xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
       xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" x:Name="CopyPlanWindow"
       Title="Gantry Change" Height="300" Width="265" FontSize="14" WindowStartupLocation="CenterScreen">

                <Grid Background="#2c2c2c">

                                      
                    <Label Content="Current Gantry" HorizontalAlignment="Left" Margin="20,24,0,0" VerticalAlignment="Top" Width="120" Foreground="#e2e2e2"/>
                    <TextBox x:Name="currMachineBox" IsReadOnly="True" HorizontalAlignment="Left" Margin="20,55,0,0" Background="#14d999" VerticalAlignment="Top" Width="200" /> 
                    
  
                    <Label Content="New Gantry" HorizontalAlignment="Left" Margin="20,96,0,0" VerticalAlignment="Top" Width="120" Foreground="#e2e2e2"/>
                    <ComboBox x:Name="NewMachineBox" HorizontalAlignment="Left" Margin="20,125,0,0" VerticalAlignment="Top" Width="200" 
                        Background="#14d999"  SelectionChanged="NewMachine_Selected"/>                  

         
                    <Button x:Name="CopyPlanButton" Content="Copy"  Click="CopyPlanButton_Click" FontSize="16" Margin="0,0,100,30" Padding="0,0,0,2" Width="75" Height="30" Background="#7f8080" Foreground="#FFFFFF" VerticalAlignment="Bottom" IsEnabled="False">
                        <Button.Resources>
                            <Style TargetType="Border">
                                <Setter Property="CornerRadius" Value="15"/>
                            </Style>
                        </Button.Resources>
                    </Button>                   
         
                    <Button x:Name="CancelButton" Content="Cancel"  Click="CancelButton_Click" FontSize="16" Margin="90,0,0,30" Padding="0,0,0,2" Width="75" Height="30" Background="#7f8080" Foreground="#FFFFFF" VerticalAlignment="Bottom">
                        <Button.Resources>
                            <Style TargetType="Border">
                                <Setter Property="CornerRadius" Value="15"/>
                            </Style>
                        </Button.Resources>
                    </Button>              
         
                 
                </Grid>

</Window>
"""
        xr = XmlReader.Create(StringReader(xaml))
        wpf.LoadComponent(self, xr)


        #load machine names
        machine_DB = get_current("MachineDB")
        #machine_list = machine_DB.QueryCommissionedMachineInfo(Filter = {'IsLinac': True})

        machine_list=[]
        machine_list.append("GTR1")
        machine_list.append("GTR2")
        machine_list.append("GTR3")



        for machine in machine_list:
            #print(machine)
            self.NewMachineBox.Items.Add(machine)


        self.mode = ""
        
        originalPlan = get_current("Plan")
        curr_machineName = originalPlan.BeamSets[0].MachineReference.MachineName
        self.currMachineBox.Text = curr_machineName
    #====End of Init===========

    def CopyPlanButton_Click(self, sender, e):
        self.mode = 'copy'
        self.DialogResult = True

    def CancelButton_Click(self, sender, e):
        self.DialogResult = False

    
    def NewMachine_Selected(self, sender, e):
        self.enable_CopyPlanButton()


    def NewPlan_Selected(self, sender, e):
        self.enable_CopyPlanButton()




    def bsLB_SelectionChanged(self, sender, e):
        self.enable_Copy_BS_Button()

    def CopyBSButton_Click(self, sender, e):
        self.mode = 'merge'
        self.DialogResult = True

    def enable_CopyPlanButton(self): #check all three conditions selected
        #if (self.plan_to_copy_box.Text != ''):
        self.CopyPlanButton.IsEnabled = True

class ComboBoxItemClass(ComboBoxItem):
    def __init__(self,name):
        self.Content = name
        self.FontSize = 10

#=== Convert the naming convention between inside and user interface
def TreatmentTechnique(bs):
    TT = '?'
    if bs.Modality == 'Protons':
        if bs.PlanGenerationTechnique == 'Imrt':
            if bs.DeliveryTechnique == 'PencilBeamScanning':
                TT = 'ProtonPencilBeamScanning'
    return TT

    
def get_energy_layers(segs):
    """
    Given the segments of a beam, create a list of energy layer objects
    :param segs: all segments (energy layer) for a beam
    :return: [list] of energy layer objects
    """
    layers = []
    for s in segs:
        el = EnergyLayer()
        el.Energy = s.NominalEnergy
        el.SpotPositions = [(p.x, p.y) for p in s.Spots.Positions]
        el.SpotWeights = [w for w in s.Spots.Weights]
        el.RelativeWeight = s.RelativeWeight
        el.SpotSize = {'x': s.Spots.SpotSize.x,'y': s.Spots.SpotSize.y}
        layers.append(el)
    return layers


def getPlanName(pn):
    """
    Function to return unique plan name
    :param pn: current plan name
    :return: unique plan name
    """
    i = 0
    npn = pn[:]
    while len([p.Name for p in root.TreatmentPlans if p.Name == npn]):
        suffix = '_{}'.format(i)
        trim_length = 16 - len(suffix)
        npn = pn[:trim_length] + suffix
        i += 1
    return npn


def getBeamSetName(bsn):
    """
    Function to return unique plan name
    :param pn: current plan name
    :return: unique plan name
    """
    i = 0
    nbsn = bsn[:]
    while len([bs.DicomPlanLabel for bs in new_plan.BeamSets if bs.DicomPlanLabel == nbsn]):
        suffix = '_{}'.format(i)
        trim_length = 16 - len(suffix)
        nbsn = bsn[:trim_length] + suffix
        i += 1
    return nbsn


class Beamset(object):

    def __init__(self):
        self.Name = None
        self.Number = None
        self.ExaminationName = None
        self.MachineName = None
        self.TreatmentTechnique = None
        self.PatientPosition = None
        self.NumberOfFractions = None
        self.Rx = None
        self.CreateSetupBeams = True
        self.UseLocalizationPointAsSetupIsocenter = False
        self.UseUserSelectedIsocenterSetupIsocenter = False # this should be added for 11B (12.0)
        self.Beams = []
        self.Objectives = []
        self.IsSFO = None
        self.IsRobust = True
        self.Robustness = None
        self.OptimizationParameters = None


class Rx(object):

    def __init__(self):
        self.RoiName = None
        self.PrescriptionType = None
        self.DoseVolume = None
        self.DoseValue = None
        self.RelativePrescriptionLevel = None
        self.AutoScaleDose = False

class OptimizationParameters(object):

    def __init__(self):
        self.NumberOfIterationsBeforeSpotWeightBounding = 0
        self.SpotWeightLimits = {}
        self.SpotWeightLimitsMargin = None


class Beam(object):

    def __init__(self):
        self.Name = None
        self.Description = None
        self.Position = {}
        self.GantryAngle = None
        self.CollimatorAngle = None
        self.CouchAngle = None
        self.CouchPitchAngle = None
        self.CouchRollAngle = None
        self.SnoutId = None
        self.SpotTuneId = None
        self.RangeShifter = None
        self.SnoutPosition = None
        self.Block = None
        self.EnergyLayers = None
        self.Mu = None


class EnergyLayer(object):

    def __init__(self):
        self.Energy = None
        self.SpotPositions = []
        self.SpotWeights = []
        self.RelativeWeight = None
        self.SpotSize = None

        
class Objective(object):

    def __init__(self):
        self.FunctionType = None
        self.RoiName = None
        self.IsConstraint = False
        self.RestrictAllBeamsIndividually = None
        self.IsRobust = None
        self.RestrictToBeamSet = False
        self.UseRbeDose = False
        self.DoseLevel = None
        self.HighDoseLevel = None
        self.LowDoseLevel = None
        self.AdaptToTargetDoseLevels = False
        self.LowDoseDistance = None
        self.PercentVolume = None
        self.Weight = None
        self.EudParameterA = None
        self.PercentStdDeviation = None
        
class Robustness(object):

    def __init__(self):
        self.PositionUncertaintyAnterior = None
        self.PositionUncertaintyPosterior = None
        self.PositionUncertaintySuperior = None
        self.PositionUncertaintyInferior = None
        self.PositionUncertaintyLeft = None
        self.PositionUncertaintyRight = None
        self.DensityUncertainty = None
        self.PositionUncertaintySetting = None
        self.IndependentLeftRight = None
        self.IndependentAnteriorPosterior = None
        self.IndependentSuperiorInferior = None
        self.NumberOfDiscretizationPoints = None
        self.IndependentBeams = None
        self.ComputeExactScenarioDoses = None
        self.NamesOfNonPlanningExaminations = []        
 
#End of definitions
 
    
# OBSERVE: The script would probably generate the wrong isocenter i patient is not HFS.
# OBSERVE: The script doesn't work for VMAT beams. No beams are copied.
# -----------------------
if __name__ == '__main__':
    # Check if a patient is loaded
    try:
        patient = get_current('Patient')
        ui = get_current('ui')
        version = int(ui.GetApplicationVersion()[0])
    except:
        #raise IOError('Please load a patient first!')
        MessageBox.Show('Please load a patient first !')
        sys.exit(-1)

    poIndex = 0
    opoIndex = 0


    root = get_current('Case') # no need to define global? if root is not in main()?


    dialog = MyWindow()
    dialog.ShowDialog()
    if dialog.DialogResult == False:
        sys.exit(0)


    # user click submit


    originalPlan = get_current("Plan")

    newMachineName = dialog.NewMachineBox.SelectedItem #get new machine name
    oldMachineName = dialog.currMachineBox.Text

    newPlanName = str(originalPlan.Name)+'_'+str(newMachineName)[0]+str(newMachineName)[-1]

    machine_db = get_current('MachineDB')
    old_machine = machine_db.GetTreatmentMachine(machineName = oldMachineName, lockMode = None)
    new_machine = machine_db.GetTreatmentMachine(machineName = newMachineName, lockMode = None)

    originalPlan.SetCurrent()
    originalPlanCTName = (get_current("Examination")).Name # #New code by FC 10/10

    # if a plan with the same name exists, rename the new plan to include 5 random characters
    for plancheck in root.TreatmentPlans:
        if newPlanName == plancheck.Name:
            newPlanName += ''.join(choice(ascii_uppercase) for i in range(5))
            break


    # Add a new plan using new CT
    new_plan = root.AddNewPlan(PlanName=newPlanName, PlannedBy='Script', Comment='Generated by Script',
                             ExaminationName=originalPlanCTName, #started a new plan with New CT
                             AllowDuplicateNames=False)
    dg = originalPlan.GetDoseGrid()

    new_plan.UpdateDoseGrid(Corner = {'x': dg.Corner.x, 'y': dg.Corner.y, 'z': dg.Corner.z},
                            VoxelSize={'x': dg.VoxelSize.x, 'y': dg.VoxelSize.y, 'z': dg.VoxelSize.z},
                            NumberOfVoxels = { 'x': dg.NrVoxels.x, 'y': dg.NrVoxels.y, 'z': dg.NrVoxels.z}) # set dose grid

    old_beamsets = originalPlan.BeamSets #Make a copy of all beamsets


    #=====Start copying BeamSets
    
    for bs in old_beamsets:
        if version >= 11:
            rx = bs.Prescription.PrimaryPrescriptionDoseReference
        else:
            rx = bs.FractionDose.ForBeamSet.Prescription.PrimaryDosePrescription
        name = bs.DicomPlanLabel
        machineName = bs.MachineReference.MachineName
        try:
            machine = machine_db.GetTreatmentMachine(machineName = machineName)
        except:
            machine = machine_db.GetTreatmentMachine(machineName=machineName,lockMode='Read')
        modality = bs.Modality
        patientPosition = bs.PatientPosition
        fractions = bs.FractionationPattern.NumberOfFractions
        
        if bs.AccurateDoseAlgorithm.DoseAlgorithm  == 'IonMonteCarlo':
            originalAlgo = 'IonMonteCarlo'
            ionmeanhistories = bs.AccurateDoseAlgorithm.IonMCMeanHistoriesPerSpotForFinalDose
            MCUncertainty = bs.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose
        
        elif bs.AccurateDoseAlgorithm.DoseAlgorithm == 'SpotWeightPencilBeam':
            originalAlgo = 'SpotWeightPencilBeam'
         
        
    
        TT = TreatmentTechnique(bs)
        if 'conformal' in TT.lower():
            for beam in bs.Beams:
                if beam.Segments.Count > 1:
                    print ('********************')
                    print ('Please unmerge any merged beams before running this script')
                    print ('********************')
                    MessageBox.Show('Please unmerge any merged beams before running this script')
                    exit(0)




        # if the beamset name exists we will add 5 random characters to maintain uniquesness
        # for bscheck in replan.BeamSets:
        #     if bs.DicomPlanLabel == bscheck.DicomPlanLabel:
        #         name += ''.join(choice(ascii_uppercase) for i in range(5))
        #         break
        name = getBeamSetName(bs.DicomPlanLabel)+'_'+str(newMachineName)[0]+str(newMachineName)[-1]
        
        
        # lets create the beamset
        NBSargs = {'Name': name,
                                'ExaminationName': originalPlanCTName,
                                'MachineName': newMachineName,
                                'Modality': modality,
                                'TreatmentTechnique': TT,
                                'PatientPosition': patientPosition,
                                'NumberOfFractions': fractions,
                                'CreateSetupBeams': True,
                                'Comment': ''}
# =============================================================================
#         if version >= 10 and subversion == 1 or version >= 11:
#             NBSargs['RbeModelName'] = None
#             NBSargs['EnableDynamicTrackingForVero'] = False
#             NBSargs['NewDoseSpecificationPointNames'] = []
#             NBSargs['NewDoseSpecificationPoints'] = []
#             NBSargs['MotionSynchronizationTechniqueSettings'] = {'DisplayName': None,
#                                                       'MotionSynchronizationSettings': None,
#                                                       'RespiratoryIntervalTime': None,
#                                                       'RespiratoryPhaseGatingDutyCycleTimePercentage': None,
#                                                       'MotionSynchronizationTechniqueType': 'Undefined'},
#             NBSargs['Custom'] = ''
#         if version >= 10 and subversion == 1: # 3.0.9
#             NBSargs['MotionSynchronizationTechniqueSettings'] = { 'DisplayName': None,
#                                                                    'MotionSynchronizationSettings': None,
#                                                                    'RespiratoryIntervalTime': None,
#                                                                    'RespiratoryPhaseGatingDutyCycleTimePercentage': None
#                                                                   }
#             NBSargs['Custom'] = None
#         if version >= 11:
#             NBSargs['MotionSynchronizationTechniqueSettings'] = \
#                                                      {'DisplayName': None, 'MotionSynchronizationSettings': None,
#                                                       'RespiratoryIntervalTime': None,
#                                                       'RespiratoryPhaseGatingDutyCycleTimePercentage': None,
#                                                       'MotionSynchronizationTechniqueType': "Undefined"}
#             NBSargs['ToleranceTableLabel'] = None
# =============================================================================


        rpbs = new_plan.AddNewBeamSet(**NBSargs)
    

                
        if TT == 'ProtonPencilBeamScanning':
            beamSet = Beamset()
            beams = []
            #beamIsos = []
            objectives = []
            is_robust = False
            rx = Rx()
            optimization_parameters = OptimizationParameters()

            beamSet.Name = rpbs.DicomPlanLabel
            beamSet.Number = rpbs.Number
            beamSet.ExaminationName = rpbs.GetPlanningExamination().Name
            beamSet.MachineName = rpbs.MachineReference.MachineName
            beamSet.TreatmentTechnique = 'ProtonPencilBeamScanning'
            beamSet.PatientPosition = rpbs.PatientPosition
            beamSet.NumberOfFractions = rpbs.FractionationPattern.NumberOfFractions
            beamSet.CreateSetupBeams = rpbs.PatientSetup.UseSetupBeams
            
            if version >= 12: # boolean flags for setup change in 11B
                if patient.Cases[0].TreatmentPlans[2].BeamSets[0].PatientSetup.IsocenterType == 'LocalizationPoint':
                    beamSet.UseLocalizationPointAsSetupIsocenter = True
                    beamSet.UseUserSelectedIsocenterSetupIsocenter = False
                elif patient.Cases[0].TreatmentPlans[2].BeamSets[0].PatientSetup.IsocenterType == 'TreatmentIsocenter':
                    beamSet.UseLocalizationPointAsSetupIsocenter = False
                    beamSet.UseUserSelectedIsocenterSetupIsocenter = True
                else: # by default it is false false?
                    beamSet.UseLocalizationPointAsSetupIsocenter = False
                    beamSet.UseUserSelectedIsocenterSetupIsocenter = False
            else:
                beamSet.UseLocalizationPointAsSetupIsocenter = bs.PatientSetup.UseLocalizationPointAsSetupIsocenter
            beamSet.IsRobust = True

            # let's build our beam objects and make logical checks on the beams
            for b in bs.Beams:
                if b.Segments.Count == 0:
                    raise IOError("Not all beams have energy layers.")

                beam = Beam()

                beam.Name = b.Name
                beam.Description = b.Description
                beam.Position = {
                    'x': b.Isocenter.Position.x,
                    'y': b.Isocenter.Position.y,
                    'z': b.Isocenter.Position.z
                }
                beam.GantryAngle = b.GantryAngle
                beam.CollimatorAngle = b.InitialCollimatorAngle
                beam.CouchAngle = b.CouchAngle
                beam.CouchPitchAngle = b.CouchPitchAngle
                beam.CouchRollAngle = b.CouchRollAngle
                beam.SnoutId = b.SnoutId
                beam.SnoutPosition = b.SnoutPosition
                beam.SpotTuneId = b.Segments[0].Spots.SpotTuneId
                beam.EnergyLayers = get_energy_layers(b.Segments)
                beam.Mu = b.BeamMU
                if b.RangeShifterId is not None:
                    beam.RangeShifter = [
                        rs.Name for rs in machine.RangeShifters
                        if rs.RangeShifterId == b.RangeShifterId
                        ][0]

                # if the beam has a block, let's make a block object to add to the beam
                if b.Blocks.Count != 0:
                    block = Block()
                    block.BlockName = "{0}_Block".format(b.Name)
                    block.MaterialName = b.Blocks[0].Material.Name
                    block.MillingToolDiameter = b.Blocks[0].MillingToolDiameter
                    block.Thickness = b.Blocks[0].Thickness
                    block.Contour = [
                        {
                            'x': c.x,
                            'y': c.y
                        }
                        for c in b.Blocks[0].Contour
                        ]

                    beam.Block = block

                # let's add this beam to the list of beams for this beamset
                beams.append(beam)

            beamSet.Beams = beams
            
           

            # let's get the index of the PlanOptimization
            for pbsPoIndex, po in enumerate(originalPlan.PlanOptimizations):
                if po.OptimizedBeamSets[0].DicomPlanLabel == bs.DicomPlanLabel:
                    bsNumber = pbsPoIndex

            swl = originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimits
            optimization_parameters.SpotWeightLimits = {'x': swl.x, 'y': swl.y}
            optimization_parameters.SpotWeightLimitsMargin = \
                originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimitsMargin
                
            optimization_parameters.NumberOfIterationsBeforeSpotWeightBounding = \
                originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.PencilBeamScanningProperties.NumberOfIterationsBeforeSpotWeightBounding      

            beamSet.OptimizationParameters = optimization_parameters
            
            new_plan.PlanOptimizations[bsNumber].OptimizationParameters.Algorithm.MaxNumberOfIterations = originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.Algorithm.MaxNumberOfIterations
            
            new_plan.PlanOptimizations[bsNumber].OptimizationParameters.Algorithm.OptimalityTolerance = originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.Algorithm.OptimalityTolerance
            
            new_plan.PlanOptimizations[bsNumber].OptimizationParameters.PencilBeamScanningProperties.MinSpotWeightPerEnergyLayer  = originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.PencilBeamScanningProperties.MinSpotWeightPerEnergyLayer 


            # if there are non-constraint objectives, let's build our objective
            if originalPlan.PlanOptimizations[bsNumber].Objective.ConstituentFunctions.Count > 0:
                for i, ob in enumerate(originalPlan.PlanOptimizations[bsNumber].Objective.ConstituentFunctions):
                    objective = Objective()
                    try:
                        objective.FunctionType = ob.DoseFunctionParameters.FunctionType
                        objective.DoseLevel = ob.DoseFunctionParameters.DoseLevel
                        objective.PercentVolume = ob.DoseFunctionParameters.PercentVolume
                    except:
                        if hasattr(ob.DoseFunctionParameters, 'AdaptToTargetDoseLevels'):
                            objective.FunctionType = "DoseFallOff"
                            objective.AdaptToTargetDoseLevels = ob.DoseFunctionParameters.AdaptToTargetDoseLevels
                            objective.HighDoseLevel = ob.DoseFunctionParameters.HighDoseLevel
                            objective.LowDoseLevel = ob.DoseFunctionParameters.LowDoseLevel
                            objective.LowDoseDistance = ob.DoseFunctionParameters.LowDoseDistance
                        elif hasattr(ob.DoseFunctionParameters, 'PercentStdDeviation'):
                            objective.FunctionType = "UniformityConstraint"
                            objective.PercentStdDeviation = ob.DoseFunctionParameters.PercentStdDeviation
                        else:
                            objective.FunctionType = ob.DoseFunctionParameters.FunctionType
                            objective.DoseLevel = ob.DoseFunctionParameters.DoseLevel
                            objective.EudParameterA = ob.DoseFunctionParameters.EudParameterA
                    objective.RoiName = ob.ForRegionOfInterest.Name
                    objective.RestrictAllBeamsIndividually = False
                    objective.IsConstraint = False
                    objective.IsRobust = ob.UseRobustness
                    objective.Weight = ob.DoseFunctionParameters.Weight
                    if objective.IsRobust:
                        is_robust = True


                    objectives.append(objective)

            else:
                i = 0

            # if there are constraint objectives, let's build our objective objects and make logical checks
            if originalPlan.PlanOptimizations[bsNumber].Constraints.Count > 0:
                for j, ob in enumerate(originalPlan.PlanOptimizations[bsNumber].Constraints):
                    objective = Objective()
                    try:
                        objective.FunctionType = ob.DoseFunctionParameters.FunctionType
                        objective.DoseLevel = ob.DoseFunctionParameters.DoseLevel
                        objective.PercentVolume = ob.DoseFunctionParameters.PercentVolume
                    except:
                        if hasattr(ob.DoseFunctionParameters, 'AdaptToTargetDoseLevels'):
                            objective.FunctionType = "DoseFallOff"
                            objective.AdaptToTargetDoseLevels = ob.DoseFunctionParameters.AdaptToTargetDoseLevels
                            objective.HighDoseLevel = ob.DoseFunctionParameters.HighDoseLevel
                            objective.LowDoseLevel = ob.DoseFunctionParameters.LowDoseLevel
                            objective.LowDoseDistance = ob.DoseFunctionParameters.LowDoseDistance
                        elif hasattr(ob.DoseFunctionParameters, 'PercentStdDeviation'):
                            objective.FunctionType = "UniformityConstraint"
                            objective.PercentStdDeviation = ob.DoseFunctionParameters.PercentStdDeviation
                        else:
                            objective.FunctionType = ob.DoseFunctionParameters.FunctionType
                            objective.DoseLevel = ob.DoseFunctionParameters.DoseLevel
                            objective.EudParameterA = ob.DoseFunctionParameters.EudParameterA
                    objective.RoiName = ob.ForRegionOfInterest.Name
                    objective.IsConstraint = True
                    objective.IsRobust = ob.UseRobustness
                    if objective.IsRobust:
                        is_robust = True
                    objective.Weight = ob.DoseFunctionParameters.Weight
                    objective.RestrictAllBeamsIndividually = False

                objectives.append(objective)

            # let's add the objective objects to our beamset object
            beamSet.Objectives = objectives

            # if the plan is robust, we need to copy the robust optimization
            # parameters
            if is_robust:
                rp = originalPlan.PlanOptimizations[
                    bsNumber].OptimizationParameters.RobustnessParameters
                robustness = Robustness()
                robustness.PositionUncertaintyAnterior = rp.PositionUncertaintyParameters.PositionUncertaintyAnterior
                robustness.PositionUncertaintyInferior = rp.PositionUncertaintyParameters.PositionUncertaintyInferior
                robustness.PositionUncertaintyPosterior = rp.PositionUncertaintyParameters.PositionUncertaintyPosterior
                robustness.PositionUncertaintySuperior = rp.PositionUncertaintyParameters.PositionUncertaintySuperior
                robustness.PositionUncertaintyLeft = rp.PositionUncertaintyParameters.PositionUncertaintyLeft
                robustness.PositionUncertaintyRight = rp.PositionUncertaintyParameters.PositionUncertaintyRight
                robustness.PositionUncertaintySetting = rp.PositionUncertaintyParameters.PositionUncertaintySetting
                robustness.IndependentLeftRight = rp.PositionUncertaintyParameters.IndependentLeftRight
                robustness.IndependentAnteriorPosterior = rp.PositionUncertaintyParameters.IndependentAnteriorPosterior
                robustness.IndependentSuperiorInferior = rp.PositionUncertaintyParameters.IndependentSuperiorInferior
                robustness.DensityUncertainty = rp.DensityUncertaintyParameters.DensityUncertainty
                robustness.NumberOfDiscretizationPoints = rp.DensityUncertaintyParameters.NumberOfDiscretizationPoints
                robustness.ComputeExactScenarioDoses = rp.RobustComputationSettings.ComputeExactScenarioDoses
                robustness.NamesOfNonPlanningExaminations = [e.Name for e in rp.PatientGeometryUncertaintyParameters.Examinations]

                # let's add the robustness object to the beamset object
                beamSet.Robustness = robustness
                beamSet.IsRobust = True


            newBeamSet = rpbs

            # let's add the beams
            isoNames = []
            for b, oldb in zip(beamSet.Beams, bs.Beams):

                #if b.Name not in beamIsos:
                    #beamIsos.append('_'+str(newMachineName)[0]+str(newMachineName)[-1])
                isocenter = b.Position
                isoData = bs.CreateDefaultIsocenterData(
                        Position=isocenter
                    )

                

                newBeam = newBeamSet.CreatePBSIonBeam(
                    SnoutId=b.SnoutId,
                    SpotTuneId=b.SpotTuneId,
                    RangeShifter=b.RangeShifter,
                    IsocenterData=isoData,
                    Name=b.Name,
                    Description=b.Description,
                    GantryAngle=b.GantryAngle,
                    CouchAngle=b.CouchAngle,
                    CouchPitchAngle=b.CouchPitchAngle,
                    CouchRollAngle=b.CouchRollAngle,
                    CollimatorAngle=b.CollimatorAngle
                )




                if oldb.Isocenter.Annotation.Name not in isoNames:
                    isocolor = oldb.Isocenter.Annotation.DisplayColor
                    newBeam.Isocenter.EditIsocenter(Name = oldb.Isocenter.Annotation.Name +'_'+str(newMachineName)[0]+str(newMachineName)[-1], Color=isocolor)
                    isoNames.append(oldb.Isocenter.Annotation.Name)
                    
                else:
                    newBeam.SetIsocenter(Name = oldb.Isocenter.Annotation.Name +'_'+str(newMachineName)[0]+str(newMachineName)[-1])
                    
                

                newBeam.EditSnoutPosition(
                    SnoutPosition=b.SnoutPosition
                )

                # if there is a block, let's create it
                if b.Block is not None:
                    blockName = "{0}_Block".format(b.Name)
                    newBeam.AddOrEditIonBlock(
                        BlockName=blockName,
                        MaterialName=b.Block.MaterialName,
                        Thickness=b.Block.Thickness,
                        MillingToolDiameter=b.Block.MillingToolDiameter
                    )

                    newBeam.Blocks[blockName].Contour = b.Block.Contour

                # lets add the spots
                for el in b.EnergyLayers:
                    xPos = [sp[0] for sp in el.SpotPositions]
                    yPos = [sp[1] for sp in el.SpotPositions]
                    # weights = [sw / sum(el.SpotWeights) for sw in el.SpotWeights]

                    newBeam.AddEnergyLayerWithSpots(
                        Energy=el.Energy,
                        SpotPositionsX=xPos,
                        SpotPositionsY=yPos,
                        SpotWeights=el.SpotWeights
                    )

                    newBeam.Segments[newBeam.Segments.Count-1].RelativeWeight = el.RelativeWeight
                    
                    #MessageBox.Show(str(el.SpotSizeX))
                    
                    newBeam.Segments[newBeam.Segments.Count-1].Spots.SpotSize = el.SpotSize
                    
                    #MessageBox.Show(str(newBeam.Segments[newBeam.Segments.Count-1].Spots.SpotSize.x))

                newBeam.BeamMU = b.Mu

            # let's add the objectives
            for poIndex, po in enumerate(new_plan.PlanOptimizations):
                if po.OptimizedBeamSets[0].DicomPlanLabel == beamSet.Name:
                    print ("poIndex = " + str(poIndex))
                    bsNumber = poIndex
            iO = 0
            iC = 0
            for ob in beamSet.Objectives:
                ft = "TargetEud" if ob.FunctionType == "UniformEud" else ob.FunctionType
                new_plan.PlanOptimizations[bsNumber].AddOptimizationFunction(
                    FunctionType=ft,
                    RoiName=ob.RoiName,
                    IsConstraint=ob.IsConstraint,
                    RestrictAllBeamsIndividually=ob.RestrictAllBeamsIndividually,
                    IsRobust=ob.IsRobust
                )

                if ob.IsConstraint:
                    dFP = new_plan.PlanOptimizations[bsNumber].Constraints[iC].DoseFunctionParameters
                    iC += 1
                else:
                    dFP = new_plan.PlanOptimizations[bsNumber].Objective.ConstituentFunctions[iO].DoseFunctionParameters
                    iO += 1

                if ob.FunctionType == "DoseFallOff":
                    dFP.HighDoseLevel = ob.HighDoseLevel
                    dFP.LowDoseLevel = ob.LowDoseLevel
                    dFP.LowDoseDistance = ob.LowDoseDistance
                    dFP.AdaptToTargetDoseLevels = ob.AdaptToTargetDoseLevels
                    dFP.Weight = ob.Weight
                elif ob.FunctionType in ["UniformEud", "MaxEud", "MinEud"]:
                    dFP.DoseLevel = ob.DoseLevel
                    dFP.EudParameterA = ob.EudParameterA
                    dFP.Weight = ob.Weight
                elif ob.FunctionType == "UniformityConstraint":
                    dFP.PercentStdDeviation = ob.PercentStdDeviation
                    dFP.Weight = ob.Weight
                else:
                    dFP.DoseLevel = ob.DoseLevel
                    dFP.PercentVolume = ob.PercentVolume
                    dFP.Weight = ob.Weight

            new_plan.PlanOptimizations[
                bsNumber
            ].OptimizationParameters.PencilBeamScanningProperties.NumberOfIterationsBeforeSpotWeightBounding = \
                beamSet.OptimizationParameters.NumberOfIterationsBeforeSpotWeightBounding

            new_plan.PlanOptimizations[
                bsNumber
            ].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimits = \
                beamSet.OptimizationParameters.SpotWeightLimits

            new_plan.PlanOptimizations[
                bsNumber
            ].OptimizationParameters.PencilBeamScanningProperties.SpotWeightLimitsMargin = \
                beamSet.OptimizationParameters.SpotWeightLimitsMargin
                
                
            for b_set, beam_setting in enumerate(originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings):
                
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSelectionLateralMarginMode = beam_setting.ScannedBeamProperties.SpotSelectionLateralMarginMode
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSelectionLateralMarginScaleFactor = beam_setting.ScannedBeamProperties.SpotSelectionLateralMarginScaleFactor
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSelectionLateralMargin = beam_setting.ScannedBeamProperties.SpotSelectionLateralMargin
                
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SelectSpotSpacingAutomatically = beam_setting.ScannedBeamProperties.SelectSpotSpacingAutomatically
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSpacingSeparationFactor = beam_setting.ScannedBeamProperties.SpotSpacingSeparationFactor
            
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.EnergySelectionMode = beam_setting.ScannedBeamProperties.EnergySelectionMode
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.EnergyLayerSeparationFactor = beam_setting.ScannedBeamProperties.EnergyLayerSeparationFactor
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.EnergyLayerSeparation = beam_setting.ScannedBeamProperties.EnergyLayerSeparation
                
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSelectionDistalTargetLayerMargin = beam_setting.ScannedBeamProperties.SpotSelectionDistalTargetLayerMargin
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.SpotSelectionProximalTargetLayerMargin = beam_setting.ScannedBeamProperties.SpotSelectionProximalTargetLayerMargin
            
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ScannedBeamProperties.ScanDirectionAngle = beam_setting.ScannedBeamProperties.ScanDirectionAngle
                
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].DistalEdgeWidth = beam_setting.DistalEdgeWidth
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].DistalTargetMargin = beam_setting.DistalTargetMargin
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].ImptSpotSelectionType = beam_setting.ImptSpotSelectionType
                
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].MinimumRadiologicDepthMargin = beam_setting.MinimumRadiologicDepthMargin
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].MaximumRadiologicalDepth = beam_setting.MaximumRadiologicalDepth
                
                for ar, avoid_rois in enumerate(originalPlan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].AvoidanceRois):
                
                    new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].OrganAtRiskRangeMargin = beam_setting.OrganAtRiskRangeMargin    
                    #new_plan.PlanOptimizations[bsNumber].OptimizationParameters.TreatmentSetupSettings[0].BeamSettings[b_set].AvoidanceRois[ar] = beam_setting.AvoidanceRois[ar]
            
     

            # let's check and add as appropriate the robustness parameters
            if beamSet.Robustness is not None:
                print ("Adding Robust Parameters")
                new_plan.PlanOptimizations[bsNumber].OptimizationParameters.SaveRobustnessParameters(
                    PositionUncertaintyAnterior=beamSet.Robustness.PositionUncertaintyAnterior,
                    PositionUncertaintyPosterior=beamSet.Robustness.PositionUncertaintyPosterior,
                    PositionUncertaintySuperior=beamSet.Robustness.PositionUncertaintySuperior,
                    PositionUncertaintyInferior=beamSet.Robustness.PositionUncertaintyInferior,
                    PositionUncertaintyLeft=beamSet.Robustness.PositionUncertaintyLeft,
                    PositionUncertaintyRight=beamSet.Robustness.PositionUncertaintyRight,
                    IndependentLeftRight=beamSet.Robustness.IndependentLeftRight,
                    IndependentAnteriorPosterior=beamSet.Robustness.IndependentAnteriorPosterior,
                    IndependentSuperiorInferior=beamSet.Robustness.IndependentSuperiorInferior,
                    DensityUncertainty=beamSet.Robustness.DensityUncertainty,
                    ComputeExactScenarioDoses=beamSet.Robustness.ComputeExactScenarioDoses,
                    NamesOfNonPlanningExaminations=beamSet.Robustness.NamesOfNonPlanningExaminations
                )
                


    #copy and add clinical goals
    for i, cg in enumerate(originalPlan.TreatmentCourse.EvaluationSetup.EvaluationFunctions):
        roi = cg.ForRegionOfInterest.Name
        criteria = cg.PlanningGoal.GoalCriteria
        goal_type = cg.PlanningGoal.Type
        acceptancelevel = cg.PlanningGoal.AcceptanceLevel
        parametervalue = cg.PlanningGoal.ParameterValue
        iscomparativegoal = cg.PlanningGoal.IsComparativeGoal
        priority = cg.PlanningGoal.Priority
                
        new_plan.TreatmentCourse.EvaluationSetup.AddClinicalGoal(RoiName = roi, GoalCriteria = criteria, GoalType = goal_type, AcceptanceLevel = acceptancelevel, ParameterValue = parametervalue, IsComparativeGoal = iscomparativegoal, Priority = priority)
                
        

    for bs, beam_set in enumerate(originalPlan.BeamSets):

        #copy RX definition to new plan
        roiname = beam_set.Prescription.PrimaryDosePrescription.OnStructure.Name
        dosevolume = beam_set.Prescription.PrimaryDosePrescription.DoseVolume
        prescriptiontype = beam_set.Prescription.PrimaryDosePrescription.PrescriptionType
        dosevalue = beam_set.Prescription.PrimaryDosePrescription.DoseValue
        relativeprescriptionlevel = beam_set.Prescription.PrimaryDosePrescription.RelativePrescriptionLevel
        #autoscaledose = 
        new_plan.BeamSets[bs].AddDosePrescriptionToRoi(RoiName=roiname, DoseVolume=dosevolume, PrescriptionType=prescriptiontype, DoseValue=dosevalue, RelativePrescriptionLevel=relativeprescriptionlevel, AutoScaleDose=False) 
        
        #collect gantry angles for setup beams
        setup_g_angles=[]
        for sb, setup_beam in enumerate(beam_set.PatientSetup.SetupBeams):
            setup_g_angles.append(setup_beam.GantryAngle)

        new_plan.BeamSets[bs].UpdateSetupBeams(ResetSetupBeams = True, SetupBeamsGantryAngles = setup_g_angles)
        num_beams = beam_set.Beams.Count
        new_plan.BeamSets[bs].PatientSetup.SetupBeams[0].Name = beam_set.PatientSetup.SetupBeams[0].Name
        new_plan.BeamSets[bs].PatientSetup.SetupBeams[1].Name = beam_set.PatientSetup.SetupBeams[1].Name
        
        first_iso_name = beam_set.Beams[0].Isocenter.Annotation.Name
        iso_names = []
        iso_name = beam_set.Beams[0].Isocenter.Annotation.Name
        iso_name_next = iso_name
        iso_names.append(iso_name)
        for i in range(num_beams):
    
            if beam_set.Beams[i].Isocenter.Annotation.Name == iso_name:
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2].Name = "keep" + str(i+1)
       
            elif beam_set.Beams[i].Isocenter.Annotation.Name == iso_name_next:
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2+(len(iso_names)-1)*(num_beams+2)].Name = "keep" + str(i+1)
            
            else:
                iso_name_next=beam_set.Beams[i].Isocenter.Annotation.Name   
                iso_names.append(iso_name_next)  
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2+(len(iso_names)-1)*(num_beams+2)].Name = "keep" + str(i+1)
        
        sb_delete=[]
        for n, sb in enumerate(new_plan.BeamSets[bs].PatientSetup.SetupBeams):
            if 'SB' in sb.Name:
                sb_delete.append(n)
            else:
                pass

        for m in sb_delete[::-1]:
            sb_name = new_plan.BeamSets[bs].PatientSetup.SetupBeams[m].Name
            new_plan.BeamSets[bs].DeleteSetupBeam(BeamName=sb_name)
            
        for l, sb_new in enumerate(new_plan.BeamSets[bs].PatientSetup.SetupBeams):  
            sb_new.Number=l+1
        
        
        
        for sb, setup_beam in enumerate(beam_set.PatientSetup.SetupBeams):
            new_plan.BeamSets[bs].PatientSetup.SetupBeams[sb].CouchAngle = setup_beam.CouchAngle
            new_plan.BeamSets[bs].PatientSetup.SetupBeams[sb].Description = setup_beam.Description
            new_plan.BeamSets[bs].PatientSetup.SetupBeams[sb].Name = setup_beam.Name
          

        for i in range(num_beams):  
            if newMachineName =="GTR1":
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[0].SetupImagingDeviceName = r"GTR1-rad-A"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[1].SetupImagingDeviceName = r"GTR1-rad-B"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2].SetupImagingDeviceName = r"GTR1-rad-B"
            elif newMachineName == "GTR2":
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[0].SetupImagingDeviceName = r"GTR2-rad-A"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[1].SetupImagingDeviceName = r"GTR2-rad-B"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2].SetupImagingDeviceName = r"GTR2-rad-B"
            elif newMachineName == "GTR3":
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[0].SetupImagingDeviceName = r"GTR3-rad-A"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[1].SetupImagingDeviceName = r"GTR3-rad-B"
                new_plan.BeamSets[bs].PatientSetup.SetupBeams[i+2].SetupImagingDeviceName = r"GTR3-rad-B"






     #let's compute the beams

    for bs in new_plan.BeamSets:
 
        if bs.Beams.Count != 0:
              if originalAlgo  == 'IonMonteCarlo':
                  bs.AccurateDoseAlgorithm.IonMCMeanHistoriesPerSpotForFinalDose = ionmeanhistories
                  bs.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = MCUncertainty 
                  bs.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm='IonMonteCarlo' , ForceRecompute=True)
                
              elif originalAlgo == 'SpotWeightPencilBeam': 
                  bs.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm='SpotWeightPencilBeam', ForceRecompute=True)
 

 


patient.Save()
new_plan.SetCurrent()

