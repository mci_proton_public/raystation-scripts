# -*- coding: utf-8 -*-

import sys
from connect import *
import clr
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox

__version__ = '0.0.6'

beam_set = get_current("BeamSet")
plan = get_current("Plan")

setup_g_angles = []

plan_name = str(plan.Name)

patient_position = beam_set.PatientPosition

#determine which orthogonal setup fields are needed
first_g_angle = beam_set.Beams[0].GantryAngle

if patient_position == "HeadFirstSupine":

    if 180 >= first_g_angle > 90: #start with a PA/LL setup
        setup_g_angles = [180,90]
        name_0 = "PA_Setup"
        name_1 = "LL_Setup"
    if 90 >= first_g_angle > 0: #start with an LL/AP setup
        setup_g_angles = [90,0]
        name_0 = "LL_Setup"
        name_1 = "AP_Setup"
    if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/RL setup
        setup_g_angles = [0,270]
        name_0 = "AP_Setup"
        name_1 = "RL_Setup"
    if 270 >= first_g_angle > 180: #start with RL/PA setup
        setup_g_angles = [270,180]
        name_0 = "RL_Setup"
        name_1 = "PA_Setup"

elif patient_position == "FeetFirstSupine":

    if 180 >= first_g_angle > 90: #start with a PA/RL setup
        setup_g_angles = [180,90]
        name_0 = "PA_Setup"
        name_1 = "RL_Setup"
    if 90 >= first_g_angle > 0: #start with an RL/AP setup
        setup_g_angles = [90,0]
        name_0 = "RL_Setup"
        name_1 = "AP_Setup"
    if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/LL setup
        setup_g_angles = [0,270]
        name_0 = "AP_Setup"
        name_1 = "LL_Setup"
    if 270 >= first_g_angle > 180: #start with LL/PA setup
        setup_g_angles = [270,180]
        name_0 = "LL_Setup"
        name_1 = "PA_Setup"
   
elif patient_position == "HeadFirstProne":

    if 180 >= first_g_angle > 90: #start with a PA/RL setup
        setup_g_angles = [180,90]
        name_0 = "AP_Setup"
        name_1 = "RL_Setup"
    if 90 >= first_g_angle > 0: #start with an RL/AP setup
        setup_g_angles = [90,0]
        name_0 = "RL_Setup"
        name_1 = "PA_Setup"
    if 360 >= first_g_angle > 270 or first_g_angle == 0: #start with AP/LL setup
        setup_g_angles = [0,270]
        name_0 = "PA_Setup"
        name_1 = "LL_Setup"
    if 270 >= first_g_angle > 180: #start with LL/PA setup
        setup_g_angles = [270,180]
        name_0 = "LL_Setup"
        name_1 = "AP_Setup"    
   
    
   
name_list = ['CW','Br_L','Br_R','Br+N_L','Br+N_R']    #['breast', 'Breast', 'brst', 'Brst', 'CW','Br+N']   
if any(n in plan_name for n in name_list): #if plan is breast or chestwall, use these setup orthogonals  
    setup_g_angles = [0,270]        
    name_0 = "AP_Setup"
    name_1 = "RL_Setup"

if 'HN_R' in plan_name:  # if plan is HN_R, use these setup orthogonals
    setup_g_angles = [270, 180]
    name_0 = "RL_Setup"
    name_1 = "PA_Setup"
elif 'HN_L' in plan_name or 'HN' in plan_name:  # if plan is HN_L or HN, use these setup orthogonals
    setup_g_angles = [90, 0]
    name_0 = "LL_Setup"
    name_1 = "AP_Setup"

        
        
save_tx_angles = []
num_beams = beam_set.Beams.Count #get the number of tx beams in the beamset
for b, beams in enumerate(beam_set.Beams):
    tx_g_angle = beam_set.Beams[b].GantryAngle #record the gantry angle of each beam
    save_tx_angles.append(int(tx_g_angle)) #save tx gantry angles to a list
    
    if tx_g_angle <=89:
        setup_g_angle = tx_g_angle+360-90
    else:    
        setup_g_angle = tx_g_angle-90
    setup_g_angles.append(setup_g_angle) #save a list of the setup gantry angles needed

    
beam_names = []
beam_descriptions = []
for b, beams in enumerate(beam_set.Beams):
    beam_names.append(beam_set.Beams[b].Name) #copy tx beam names
    beam_descriptions.append(beam_set.Beams[b].Description) #copy tx beam descriptions


beam_set.UpdateSetupBeams(ResetSetupBeams = True, SetupBeamsGantryAngles = setup_g_angles)

beam_set.PatientSetup.SetupBeams[0].Name = name_0
beam_set.PatientSetup.SetupBeams[0].Description = name_0
beam_set.PatientSetup.SetupBeams[1].Name = name_1
beam_set.PatientSetup.SetupBeams[1].Description = name_1



first_iso_name = beam_set.Beams[0].Isocenter.Annotation.Name
iso_names = []
iso_name = beam_set.Beams[0].Isocenter.Annotation.Name
iso_name_next = iso_name
iso_names.append(iso_name)
for i in range(num_beams):
    
    if beam_set.Beams[i].Isocenter.Annotation.Name == iso_name:
       beam_set.PatientSetup.SetupBeams[i+2].Name = "keep" + str(i+1)
       
    elif beam_set.Beams[i].Isocenter.Annotation.Name == iso_name_next:
        beam_set.PatientSetup.SetupBeams[i+2+(len(iso_names)-1)*(num_beams+2)].Name = "keep" + str(i+1)
            
    else:
        iso_name_next=beam_set.Beams[i].Isocenter.Annotation.Name   
        iso_names.append(iso_name_next)  
        beam_set.PatientSetup.SetupBeams[i+2+(len(iso_names)-1)*(num_beams+2)].Name = "keep" + str(i+1)

sb_delete=[]
for n, sb in enumerate(beam_set.PatientSetup.SetupBeams):
    if 'SB' in sb.Name:
        sb_delete.append(n)
    else:
        pass

for m in sb_delete[::-1]:
    sb_name = beam_set.PatientSetup.SetupBeams[m].Name
    beam_set.DeleteSetupBeam(BeamName=sb_name)
    
for l, sb_new in enumerate(beam_set.PatientSetup.SetupBeams):  
    sb_new.Number=l+1
    

plan_name = str(plan.Name)
if '^' in plan_name:
    plan_name_after_caret=plan_name.split('^')[1]
else:
    plan_name_after_caret = plan_name
name_char_count=[]
if 'G1' in plan_name_after_caret:
    machine='GTR1'
elif 'G2' in plan_name_after_caret:
    machine='GTR2'
elif 'G3' in plan_name_after_caret:
    machine='GTR3'
else:
    machine='GTR1'

for i in range(num_beams):    

    
    #machine = beam_set.MachineReference.MachineName
    
    if machine =="GTR1":
        beam_set.PatientSetup.SetupBeams[0].SetupImagingSystemName = r"GTR1-rad-A"
        beam_set.PatientSetup.SetupBeams[1].SetupImagingSystemName = r"GTR1-rad-B"
        beam_set.PatientSetup.SetupBeams[i+2].SetupImagingSystemName = r"GTR1-rad-B"
    elif machine == "GTR2":
        beam_set.PatientSetup.SetupBeams[0].SetupImagingSystemName = r"GTR2-rad-A"
        beam_set.PatientSetup.SetupBeams[1].SetupImagingSystemName = r"GTR2-rad-B"
        beam_set.PatientSetup.SetupBeams[i+2].SetupImagingSystemName = r"GTR2-rad-B"
    elif machine == "GTR3":
        beam_set.PatientSetup.SetupBeams[0].SetupImagingSystemName = r"GTR3-rad-A"
        beam_set.PatientSetup.SetupBeams[1].SetupImagingSystemName = r"GTR3-rad-B"
        beam_set.PatientSetup.SetupBeams[i+2].SetupImagingSystemName = r"GTR3-rad-B"
    
    couch = int(beam_set.Beams[i].CouchRotationAngle)
    
  
    
    if couch != 0:
        string = beam_names[i] + "_B" #save_tx_angles[s]
        beam_set.PatientSetup.SetupBeams[i+2].CouchRotationAngle = couch
    else:
        string = beam_names[i] + "_B" #save_tx_angles[s]

    beam_set.PatientSetup.SetupBeams[i+2].Name = string
    beam_set.PatientSetup.SetupBeams[i+2].Description = string
    
    name_char_count.append(string)


#make a warning pop up if name is over 16 characters

long_names = [x for x in filter(lambda x: len(x) > 16, name_char_count)]
format_names = ('\n'.join(long_names))
if len(long_names) > 0:
    MessageBox.Show("Warning: The following setup beam names are over 16 characters long and will be truncated by ARIA:\n\n" + str(format_names))
    sys.exit()
else:
    pass    
