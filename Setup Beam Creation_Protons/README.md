# Setup Beam Creation
## Purpose
This Python script automates the creation of setup beams for proton therapy treatment planning. It is designed to streamline the process of configuring beams based on patient positioning and specific treatment plan requirements, enhancing efficiency and accuracy in the initial stages of treatment planning.
## Workflow
* **Automatic Setup Beam Generation**: The script generates orthogonal setup fields necessary for various patient positions (HeadFirstSupine, FeetFirstSupine, HeadFirstProne) and aligns them according to the initial gantry angle of the treatment beams.
* **Plan-Specific Customization**: It provides tailored setup configurations for plans targeting the breast or chest wall, as well as head and neck treatments from either the right (HN_R) or left (HN_L) side, ensuring precise patient alignment.
* **Dynamic Naming and Description**: Setup beams are automatically named and described based on their orientation and the specific needs of the plan, facilitating easy identification and management within the treatment planning system.
* **Isocenter Management**: For plans with multiple isocenters, the script assigns unique names to setup beams associated with each isocenter, preventing confusion, and ensuring accurate beam placement.
* **Setup Beam Cleanup**: Unnecessary setup beams are identified and removed from the patient setup, maintaining a clean and organized treatment plan.
* **Imaging System Assignment**: The script assigns the appropriate imaging system to setup beams based on the treatment machine (GTR1, GTR2, GTR3) and couch rotation angle, ensuring compatibility and optimal imaging quality.
* **Name Length Check**: A built-in check warns the user if any setup beam names exceed 16 characters, preventing potential issues with name truncation in the treatment planning system.
## How to use
* Press play ;)
