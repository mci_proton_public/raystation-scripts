# -*- coding: utf-8 -*-
"""
Modified on 5/18/2022

@author: LAU12597
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 07:19:40 2022

@author: edg11397
"""

from connect import *

case = get_current("Case")
beam_set = get_current("BeamSet")
patient =get_current("Patient")
pl=get_current("Plan")

orname=pl.Name
Nname=orname+"+0.5R-L"

Nom_iso=beam_set.Beams[2].Isocenter.Position
xval=Nom_iso.x
yval=Nom_iso.y
zval=Nom_iso.z

case.CopyPlan(PlanName=orname, NewPlanName=Nname)

beam_set=case.TreatmentPlans[Nname].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval+0.5, 'y': yval, 'z': zval })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.01

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)

###########################3

Nname2=orname+"-0.5R-L"
case.CopyPlan(PlanName=orname, NewPlanName=Nname2)

beam_set=case.TreatmentPlans[Nname2].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval-0.5, 'y': yval, 'z': zval })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.01

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)

###########################3

Nname3=orname+"+0.5P-A"
case.CopyPlan(PlanName=orname, NewPlanName=Nname3)

beam_set=case.TreatmentPlans[Nname3].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval, 'y': yval+0.5, 'z': zval })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.01

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)


###########################3

Nname4=orname+"-0.5P-A"
case.CopyPlan(PlanName=orname, NewPlanName=Nname4)

beam_set=case.TreatmentPlans[Nname4].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval, 'y': yval-0.5, 'z': zval })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.01

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)

###########################3

Nname5=orname+"+0.5I-S"
case.CopyPlan(PlanName=orname, NewPlanName=Nname5)

beam_set=case.TreatmentPlans[Nname5].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval, 'y': yval, 'z': zval+0.5 })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.005

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)

###########################

Nname6=orname+"-0.5I-S"
case.CopyPlan(PlanName=orname, NewPlanName=Nname6)

beam_set=case.TreatmentPlans[Nname6].BeamSets[0]
beam_set.Beams[2].Isocenter.EditIsocenter(Position={ 'x': xval, 'y': yval, 'z': zval-0.5 })

beam_set.AccurateDoseAlgorithm.MCStatisticalUncertaintyForFinalDose = 0.005

beam_set.ComputeDose(ComputeBeamDoses=True, DoseAlgorithm="IonMonteCarlo", ForceRecompute=False)









