from connect import *
import numpy as np
import os
from datetime import datetime

# This script will export a pld for each beam in the current beamset
# The pld will be exported to MCI Proton - Mauricio - Ray_to_PLD folder, where each pld will have the name of the beam followed by datetime

fol = r'\\ad\dfs\Shared Data\MCI Proton\Mauricio\Ray_to_PLD'
beamset = get_current('BeamSet')
patient = get_current('Patient')
beams = beamset.Beams
for id_beam,beam in enumerate(beams):
    content = ''
    filename = beam.Name + ' ' + datetime.now().strftime('%Y%m%d_%H%M%S') + '.pld'
    totalMU = beam.BeamMU
    totalWt = np.sum([np.sum(np.round(layer.Spots.Weights,10)) for layer in beam.Segments])

    content +=f"Beam,{patient.PatientID},{patient.Name},Patient Initial,{patient.Name.split('^')[1]},{beamset.BeamSetIdentifier()},{beam.Name},{totalMU},{totalWt},{len(beam.Segments)}\n"
    wts_ = 0
    for id_layer,layer in enumerate(beam.Segments):
        wts_ += np.sum(layer.Spots.Weights)
        energy = layer.NominalEnergy
        content += f"Layer,3.0,{energy},{wts_},{2*len(layer.Spots.Weights)},1\n"
        for pos,wt in zip(layer.Spots.Positions,layer.Spots.Weights):
            content += f"Element,{pos.x},{pos.y},{0},0\n"
            content += f"Element,{pos.x},{pos.y},{wt},0\n"
    with open(os.path.join(fol,filename),'w') as f:
        f.write(content[:-1])
